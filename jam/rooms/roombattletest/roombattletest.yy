
{
    "name": "roombattletest",
    "id": "d2152e52-83a1-4bc0-b960-d2ab0307fdcd",
    "creationCodeFile": "",
    "inheritCode": false,
    "inheritCreationOrder": false,
    "inheritLayers": false,
    "instanceCreationOrderIDs": [
        "10f924fc-269e-48f5-97e5-903e6222a86b",
        "5c713b9e-c2df-4c07-a660-4e948ad904dd",
        "085e5ca1-52f6-4d96-bc29-c54511afea33",
        "8a9c10a9-5ffd-4391-9598-02c0f13bb5dc",
        "e84e34d1-6244-4090-8cd6-6ad07b92a339",
        "fa775191-0005-4e6b-8e80-01429e303aeb",
        "f9d86ea2-3ab2-4104-bcbc-042264a97aca",
        "592a3015-7b6d-4c29-93e7-25d6d3837c35",
        "de0f7dbc-0978-4ac7-b182-538a07dcc094",
        "15a36022-ed07-4ec0-8e7f-2398867c97f4",
        "c9385c43-8d66-4f9b-be1e-28d6fb63fce2",
        "f05188d3-7990-4ec9-bdf2-58900648eab7",
        "7e2f1d74-5acd-4dc1-8739-df5f5c479075",
        "bd87b8ed-d9f5-4d3a-8efa-91509f268a11",
        "701eb962-0049-4774-9b29-b35c0f9cd46c",
        "816b4b83-0287-4e13-8653-75e2f8d28008",
        "88f99bff-bd6b-4a8e-8d32-2b2edb9f53c1",
        "6d80c62d-8568-4ffb-ab81-73268b45140c",
        "a92a55a6-4c5d-4a1d-bb6c-c565850e1ed8",
        "475d7ba2-ec13-452f-9009-e099a5cd26cb"
    ],
    "IsDnD": false,
    "layers": [
        {
            "__type": "GMRInstanceLayer_Model:#YoYoStudio.MVCFormat",
            "name": "a",
            "id": "af3f50db-cd51-473a-ace8-0a8f2ce4c693",
            "depth": 0,
            "grid_x": 32,
            "grid_y": 32,
            "hierarchyFrozen": false,
            "hierarchyVisible": true,
            "inheritLayerDepth": false,
            "inheritLayerSettings": false,
            "inheritSubLayers": false,
            "inheritVisibility": false,
            "instances": [
{"name": "inst_38BB0741","id": "475d7ba2-ec13-452f-9009-e099a5cd26cb","colour": { "Value": 4294967295 },"creationCodeFile": "","creationCodeType": "","ignore": false,"imageIndex": 0,"imageSpeed": 1,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_38BB0741","objId": "09bcc222-cf07-440e-9aac-1acaea96fc2c","properties": null,"rotation": 0,"scaleX": 1,"scaleY": 1,"mvc": "1.1","x": 384,"y": 32}
            ],
            "layers": [

            ],
            "m_parentID": "00000000-0000-0000-0000-000000000000",
            "m_serialiseFrozen": false,
            "modelName": "GMRInstanceLayer",
            "mvc": "1.0",
            "userdefined_depth": false,
            "visible": true
        },
        {
            "__type": "GMRInstanceLayer_Model:#YoYoStudio.MVCFormat",
            "name": "Battle",
            "id": "3bd429d1-e7f7-48e5-b50e-31f54a035ce7",
            "depth": 100,
            "grid_x": 4,
            "grid_y": 4,
            "hierarchyFrozen": false,
            "hierarchyVisible": true,
            "inheritLayerDepth": false,
            "inheritLayerSettings": false,
            "inheritSubLayers": false,
            "inheritVisibility": false,
            "instances": [
{"name": "inst_3C497466","id": "e84e34d1-6244-4090-8cd6-6ad07b92a339","colour": { "Value": 4294967295 },"creationCodeFile": "","creationCodeType": "","ignore": false,"imageIndex": 0,"imageSpeed": 1,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_3C497466","objId": "ac9cf621-adad-4053-8214-c4ef8255e13a","properties": null,"rotation": 0,"scaleX": 1,"scaleY": 1,"mvc": "1.1","x": 7,"y": 5},
{"name": "inst_7FF13B46","id": "de0f7dbc-0978-4ac7-b182-538a07dcc094","colour": { "Value": 4294967295 },"creationCodeFile": "","creationCodeType": "","ignore": false,"imageIndex": 0,"imageSpeed": 1,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_7FF13B46","objId": "c5fcff23-7ebb-4c68-b31d-88a1c3a95126","properties": null,"rotation": 0,"scaleX": 1,"scaleY": 1,"mvc": "1.1","x": -33,"y": 35},
{"name": "inst_455A43D2","id": "15a36022-ed07-4ec0-8e7f-2398867c97f4","colour": { "Value": 4294967295 },"creationCodeFile": "","creationCodeType": "","ignore": false,"imageIndex": 0,"imageSpeed": 1,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_455A43D2","objId": "8d5c752f-8978-4c34-8070-3336c69968b3","properties": null,"rotation": 0,"scaleX": 1,"scaleY": 1,"mvc": "1.1","x": 244,"y": 108},
{"name": "inst_52FB4E99","id": "7e2f1d74-5acd-4dc1-8739-df5f5c479075","colour": { "Value": 4294967295 },"creationCodeFile": "","creationCodeType": "","ignore": false,"imageIndex": 0,"imageSpeed": 1,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_52FB4E99","objId": "05ca3930-518f-48b4-afca-a882c2eac93c","properties": [{"id": "1fab5222-bfc4-4354-8f0a-066382a55224","modelName": "GMOverriddenProperty","objectId": "68800126-136c-4215-b97d-0894e7875698","propertyId": "719c11d6-2eb8-4936-aeb7-53eb197fd065","mvc": "1.0","value": "Assemble a Robot!"},{"id": "ec6784c0-4a95-4597-ada1-dfd61ea6e101","modelName": "GMOverriddenProperty","objectId": "05ca3930-518f-48b4-afca-a882c2eac93c","propertyId": "454befd9-0061-48ed-af99-e04c8c7a8cb0","mvc": "1.0","value": "Repair Robots!"}],"rotation": 0,"scaleX": 1,"scaleY": 1,"mvc": "1.1","x": 584,"y": 96},
{"name": "inst_211FA83B","id": "bd87b8ed-d9f5-4d3a-8efa-91509f268a11","colour": { "Value": 4294967295 },"creationCodeFile": "","creationCodeType": "","ignore": false,"imageIndex": 0,"imageSpeed": 1,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_211FA83B","objId": "05ca3930-518f-48b4-afca-a882c2eac93c","properties": [{"id": "101267e0-f257-4a71-ab01-377cdbc3a2fa","modelName": "GMOverriddenProperty","objectId": "05ca3930-518f-48b4-afca-a882c2eac93c","propertyId": "454befd9-0061-48ed-af99-e04c8c7a8cb0","mvc": "1.0","value": "Bolt pieces in place!"}],"rotation": 0,"scaleX": 1,"scaleY": 1,"mvc": "1.1","x": 12,"y": 132},
{"name": "inst_EEC9BF4","id": "701eb962-0049-4774-9b29-b35c0f9cd46c","colour": { "Value": 4294967295 },"creationCodeFile": "","creationCodeType": "","ignore": false,"imageIndex": 0,"imageSpeed": 1,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_EEC9BF4","objId": "b66ea329-57bc-461a-9ea2-6ce526f6ab8f","properties": null,"rotation": 0,"scaleX": 1,"scaleY": 1,"mvc": "1.1","x": 152,"y": 116},
{"name": "inst_63F78404","id": "816b4b83-0287-4e13-8653-75e2f8d28008","colour": { "Value": 4294967295 },"creationCodeFile": "","creationCodeType": "","ignore": false,"imageIndex": 0,"imageSpeed": 1,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_63F78404","objId": "96445661-0513-48dc-9950-842945c8fa22","properties": null,"rotation": 0,"scaleX": 1,"scaleY": 1,"mvc": "1.1","x": 572,"y": -24},
{"name": "inst_30E2FB54","id": "88f99bff-bd6b-4a8e-8d32-2b2edb9f53c1","colour": { "Value": 4294967295 },"creationCodeFile": "","creationCodeType": "","ignore": false,"imageIndex": 0,"imageSpeed": 1,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_30E2FB54","objId": "05ca3930-518f-48b4-afca-a882c2eac93c","properties": [{"id": "bcf327e4-679a-4f81-ab8e-dfa3450fba25","modelName": "GMOverriddenProperty","objectId": "05ca3930-518f-48b4-afca-a882c2eac93c","propertyId": "454befd9-0061-48ed-af99-e04c8c7a8cb0","mvc": "1.0","value": "Right click to toggle!"}],"rotation": 0,"scaleX": 1,"scaleY": 1,"mvc": "1.1","x": 12,"y": 152},
{"name": "inst_24C61044","id": "6d80c62d-8568-4ffb-ab81-73268b45140c","colour": { "Value": 4294967295 },"creationCodeFile": "","creationCodeType": "","ignore": false,"imageIndex": 0,"imageSpeed": 1,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_24C61044","objId": "05ca3930-518f-48b4-afca-a882c2eac93c","properties": [{"id": "19b4fbc4-419b-463d-9da8-8ba1dd6128fa","modelName": "GMOverriddenProperty","objectId": "05ca3930-518f-48b4-afca-a882c2eac93c","propertyId": "454befd9-0061-48ed-af99-e04c8c7a8cb0","mvc": "1.0","value": "This button to reset!"}],"rotation": 0,"scaleX": 1,"scaleY": 1,"mvc": "1.1","x": 584,"y": 8}
            ],
            "layers": [

            ],
            "m_parentID": "00000000-0000-0000-0000-000000000000",
            "m_serialiseFrozen": false,
            "modelName": "GMRInstanceLayer",
            "mvc": "1.0",
            "userdefined_depth": false,
            "visible": true
        },
        {
            "__type": "GMRInstanceLayer_Model:#YoYoStudio.MVCFormat",
            "name": "Bolts",
            "id": "8d1d1bcb-e8e9-4d1b-9db1-3f291d09bf36",
            "depth": 200,
            "grid_x": 32,
            "grid_y": 32,
            "hierarchyFrozen": false,
            "hierarchyVisible": true,
            "inheritLayerDepth": false,
            "inheritLayerSettings": false,
            "inheritSubLayers": false,
            "inheritVisibility": false,
            "instances": [
{"name": "inst_D5A8279","id": "10f924fc-269e-48f5-97e5-903e6222a86b","colour": { "Value": 4294967295 },"creationCodeFile": "","creationCodeType": "","ignore": false,"imageIndex": 0,"imageSpeed": 1,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_D5A8279","objId": "d6a111c9-0363-4601-a802-7c86a49d638f","properties": null,"rotation": 0,"scaleX": 1,"scaleY": 1,"mvc": "1.1","x": 660,"y": 136},
{"name": "inst_5CDB3B60","id": "fa775191-0005-4e6b-8e80-01429e303aeb","colour": { "Value": 4294967295 },"creationCodeFile": "","creationCodeType": "","ignore": false,"imageIndex": 0,"imageSpeed": 1,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_5CDB3B60","objId": "bb260b43-4a5b-434a-934c-c28a62d3409d","properties": null,"rotation": 0,"scaleX": 1,"scaleY": 1,"mvc": "1.1","x": 368,"y": 57},
{"name": "inst_50496C8A","id": "f9d86ea2-3ab2-4104-bcbc-042264a97aca","colour": { "Value": 4294967295 },"creationCodeFile": "","creationCodeType": "","ignore": false,"imageIndex": 0,"imageSpeed": 1,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_50496C8A","objId": "80230881-4a26-4584-952c-057485500f1e","properties": null,"rotation": 0,"scaleX": 1,"scaleY": 1,"mvc": "1.1","x": 377,"y": 92},
{"name": "inst_731EABBE","id": "592a3015-7b6d-4c29-93e7-25d6d3837c35","colour": { "Value": 4294967295 },"creationCodeFile": "","creationCodeType": "","ignore": false,"imageIndex": 0,"imageSpeed": 1,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_731EABBE","objId": "1a9137e9-dad3-4821-b35a-67ec6488d49f","properties": null,"rotation": 0,"scaleX": 1,"scaleY": 3.5625,"mvc": "1.1","x": 768,"y": -30}
            ],
            "layers": [

            ],
            "m_parentID": "00000000-0000-0000-0000-000000000000",
            "m_serialiseFrozen": false,
            "modelName": "GMRInstanceLayer",
            "mvc": "1.0",
            "userdefined_depth": false,
            "visible": true
        },
        {
            "__type": "GMRInstanceLayer_Model:#YoYoStudio.MVCFormat",
            "name": "Instances",
            "id": "192e018b-94ce-41b4-811c-134bbe1afb5f",
            "depth": 300,
            "grid_x": 32,
            "grid_y": 32,
            "hierarchyFrozen": false,
            "hierarchyVisible": true,
            "inheritLayerDepth": false,
            "inheritLayerSettings": false,
            "inheritSubLayers": false,
            "inheritVisibility": false,
            "instances": [
{"name": "inst_10730190","id": "c9385c43-8d66-4f9b-be1e-28d6fb63fce2","colour": { "Value": 4294967295 },"creationCodeFile": "","creationCodeType": "","ignore": false,"imageIndex": 0,"imageSpeed": 1,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_10730190","objId": "1a9137e9-dad3-4821-b35a-67ec6488d49f","properties": null,"rotation": 0,"scaleX": 1,"scaleY": 3,"mvc": "1.1","x": -95,"y": -3},
{"name": "inst_1EFA40D1","id": "f05188d3-7990-4ec9-bdf2-58900648eab7","colour": { "Value": 4294967295 },"creationCodeFile": "","creationCodeType": "","ignore": false,"imageIndex": 0,"imageSpeed": 1,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_1EFA40D1","objId": "8ad8ca55-1686-4d30-8065-aac1ea8e3ff3","properties": null,"rotation": 0,"scaleX": 1,"scaleY": 1,"mvc": "1.1","x": 658,"y": 166}
            ],
            "layers": [

            ],
            "m_parentID": "00000000-0000-0000-0000-000000000000",
            "m_serialiseFrozen": false,
            "modelName": "GMRInstanceLayer",
            "mvc": "1.0",
            "userdefined_depth": false,
            "visible": true
        },
        {
            "__type": "GMRInstanceLayer_Model:#YoYoStudio.MVCFormat",
            "name": "Chasis",
            "id": "a2b765ac-2de0-4ad5-8a97-d90b9c7e9db5",
            "depth": 400,
            "grid_x": 32,
            "grid_y": 32,
            "hierarchyFrozen": false,
            "hierarchyVisible": true,
            "inheritLayerDepth": false,
            "inheritLayerSettings": false,
            "inheritSubLayers": false,
            "inheritVisibility": false,
            "instances": [

            ],
            "layers": [

            ],
            "m_parentID": "00000000-0000-0000-0000-000000000000",
            "m_serialiseFrozen": false,
            "modelName": "GMRInstanceLayer",
            "mvc": "1.0",
            "userdefined_depth": false,
            "visible": true
        },
        {
            "__type": "GMRAssetLayer_Model:#YoYoStudio.MVCFormat",
            "name": "Assets_1",
            "id": "a9b10d1f-37de-4622-8783-a1e1fe9257d3",
            "assets": [
{"__type": "GMRSpriteGraphic_Model:#YoYoStudio.MVCFormat","name": "graphic_46DAE1DC","id": "dbf7452a-119d-4313-98d6-05072f22b049","animationFPS": 2,"animationSpeedType": "0","colour": { "Value": 4294967295 },"frameIndex": 0,"ignore": false,"inheritItemSettings": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRSpriteGraphic","rotation": 0,"scaleX": 1,"scaleY": 1,"mvc": "1.0","spriteId": "3557e8bb-a441-48a3-9a88-53325b6ceb6a","userdefined_animFPS": false,"x": 637,"y": 115}
            ],
            "depth": 500,
            "grid_x": 32,
            "grid_y": 32,
            "hierarchyFrozen": false,
            "hierarchyVisible": true,
            "inheritLayerDepth": false,
            "inheritLayerSettings": false,
            "inheritSubLayers": false,
            "inheritVisibility": false,
            "layers": [

            ],
            "m_parentID": "00000000-0000-0000-0000-000000000000",
            "m_serialiseFrozen": false,
            "modelName": "GMRAssetLayer",
            "mvc": "1.0",
            "userdefined_depth": false,
            "visible": true
        },
        {
            "__type": "GMRInstanceLayer_Model:#YoYoStudio.MVCFormat",
            "name": "UI",
            "id": "3a2a0e66-91ae-410d-9e69-3962281d057d",
            "depth": 600,
            "grid_x": 32,
            "grid_y": 32,
            "hierarchyFrozen": false,
            "hierarchyVisible": true,
            "inheritLayerDepth": false,
            "inheritLayerSettings": false,
            "inheritSubLayers": false,
            "inheritVisibility": false,
            "instances": [
{"name": "inst_78B5A1A","id": "5c713b9e-c2df-4c07-a660-4e948ad904dd","colour": { "Value": 4294967295 },"creationCodeFile": "","creationCodeType": "","ignore": false,"imageIndex": 0,"imageSpeed": 1,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_78B5A1A","objId": "96c9a786-a055-407f-9871-c7880eb77e99","properties": null,"rotation": 0,"scaleX": 1,"scaleY": 1,"mvc": "1.1","x": 572,"y": 0},
{"name": "inst_68B9D4F2","id": "a92a55a6-4c5d-4a1d-bb6c-c565850e1ed8","colour": { "Value": 4294967295 },"creationCodeFile": "","creationCodeType": "","ignore": false,"imageIndex": 0,"imageSpeed": 1,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_68B9D4F2","objId": "95f81a1a-5e13-4111-a33b-d3ce95d235b6","properties": null,"rotation": 0,"scaleX": 1,"scaleY": 1,"mvc": "1.1","x": 704,"y": 96}
            ],
            "layers": [

            ],
            "m_parentID": "00000000-0000-0000-0000-000000000000",
            "m_serialiseFrozen": false,
            "modelName": "GMRInstanceLayer",
            "mvc": "1.0",
            "userdefined_depth": false,
            "visible": true
        },
        {
            "__type": "GMRInstanceLayer_Model:#YoYoStudio.MVCFormat",
            "name": "Under_Instance",
            "id": "ce1cd026-7b3c-43f1-a9d8-517edd7d5800",
            "depth": 700,
            "grid_x": 32,
            "grid_y": 32,
            "hierarchyFrozen": false,
            "hierarchyVisible": true,
            "inheritLayerDepth": false,
            "inheritLayerSettings": false,
            "inheritSubLayers": false,
            "inheritVisibility": false,
            "instances": [

            ],
            "layers": [

            ],
            "m_parentID": "00000000-0000-0000-0000-000000000000",
            "m_serialiseFrozen": false,
            "modelName": "GMRInstanceLayer",
            "mvc": "1.0",
            "userdefined_depth": false,
            "visible": true
        },
        {
            "__type": "GMRInstanceLayer_Model:#YoYoStudio.MVCFormat",
            "name": "Conveyer",
            "id": "bd93a693-3dbb-4db5-b00a-468e927154f0",
            "depth": 800,
            "grid_x": 32,
            "grid_y": 32,
            "hierarchyFrozen": false,
            "hierarchyVisible": true,
            "inheritLayerDepth": false,
            "inheritLayerSettings": false,
            "inheritSubLayers": false,
            "inheritVisibility": false,
            "instances": [
{"name": "inst_4AF3C641","id": "085e5ca1-52f6-4d96-bc29-c54511afea33","colour": { "Value": 4294967295 },"creationCodeFile": "","creationCodeType": "","ignore": false,"imageIndex": 0,"imageSpeed": 1,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_4AF3C641","objId": "0e2a8be5-93a9-4438-b849-477ebb3b2636","properties": null,"rotation": 0,"scaleX": 1,"scaleY": 1,"mvc": "1.1","x": 585,"y": 133},
{"name": "inst_56CB9612","id": "8a9c10a9-5ffd-4391-9598-02c0f13bb5dc","colour": { "Value": 4294967295 },"creationCodeFile": "","creationCodeType": "","ignore": false,"imageIndex": 0,"imageSpeed": 1,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_56CB9612","objId": "78ef8968-66e9-4645-ac3d-4fea37c5e80e","properties": null,"rotation": 0,"scaleX": 1,"scaleY": 1,"mvc": "1.1","x": 585,"y": 133}
            ],
            "layers": [

            ],
            "m_parentID": "00000000-0000-0000-0000-000000000000",
            "m_serialiseFrozen": false,
            "modelName": "GMRInstanceLayer",
            "mvc": "1.0",
            "userdefined_depth": false,
            "visible": true
        },
        {
            "__type": "GMRBackgroundLayer_Model:#YoYoStudio.MVCFormat",
            "name": "Backgrounds_1",
            "id": "e16b597d-0b5b-4ad9-8728-101225acd3c5",
            "animationFPS": 15,
            "animationSpeedType": "0",
            "colour": { "Value": 4294967295 },
            "depth": 900,
            "grid_x": 32,
            "grid_y": 32,
            "hierarchyFrozen": false,
            "hierarchyVisible": true,
            "hspeed": 0,
            "htiled": false,
            "inheritLayerDepth": false,
            "inheritLayerSettings": false,
            "inheritSubLayers": false,
            "inheritVisibility": false,
            "layers": [

            ],
            "m_parentID": "00000000-0000-0000-0000-000000000000",
            "m_serialiseFrozen": false,
            "modelName": "GMRBackgroundLayer",
            "mvc": "1.0",
            "spriteId": "82c33977-7f37-441b-ae6d-bd8fbef96c4d",
            "stretch": false,
            "userdefined_animFPS": false,
            "userdefined_depth": false,
            "visible": true,
            "vspeed": 0,
            "vtiled": false,
            "x": -4,
            "y": -4
        }
    ],
    "modelName": "GMRoom",
    "parentId": "00000000-0000-0000-0000-000000000000",
    "physicsSettings":     {
        "id": "e214b1da-4794-41c1-9af1-c9bfa6532c0e",
        "inheritPhysicsSettings": false,
        "modelName": "GMRoomPhysicsSettings",
        "PhysicsWorld": false,
        "PhysicsWorldGravityX": 0,
        "PhysicsWorldGravityY": 10,
        "PhysicsWorldPixToMeters": 0.1,
        "mvc": "1.0"
    },
    "roomSettings":     {
        "id": "fc4639d2-ad71-4232-b224-a4a025553c9b",
        "Height": 230,
        "inheritRoomSettings": false,
        "modelName": "GMRoomSettings",
        "persistent": false,
        "mvc": "1.0",
        "Width": 750
    },
    "mvc": "1.0",
    "views": [
{"id": "a661f4cf-7c51-4a3b-8856-7c7318272fbd","hborder": 64,"hport": 1080,"hspeed": -1,"hview": 180,"inherit": false,"modelName": "GMRView","objId": "95f81a1a-5e13-4111-a33b-d3ce95d235b6","mvc": "1.0","vborder": 90,"visible": true,"vspeed": -1,"wport": 768,"wview": 128,"xport": 0,"xview": 572,"yport": 0,"yview": 0},
{"id": "32c4bf23-b102-4e8c-9504-b21ce4fd5af5","hborder": 32,"hport": 1080,"hspeed": -1,"hview": 180,"inherit": false,"modelName": "GMRView","objId": "00000000-0000-0000-0000-000000000000","mvc": "1.0","vborder": 32,"visible": true,"vspeed": -1,"wport": 1152,"wview": 192,"xport": 768,"xview": 0,"yport": 0,"yview": 0},
{"id": "4c6055ca-ec86-4cca-b5cb-16111e6b70a5","hborder": 32,"hport": 768,"hspeed": -1,"hview": 768,"inherit": false,"modelName": "GMRView","objId": "00000000-0000-0000-0000-000000000000","mvc": "1.0","vborder": 32,"visible": false,"vspeed": -1,"wport": 1024,"wview": 1024,"xport": 0,"xview": 0,"yport": 0,"yview": 0},
{"id": "821f84f4-99e9-4147-aaf4-25677d318dda","hborder": 32,"hport": 768,"hspeed": -1,"hview": 768,"inherit": false,"modelName": "GMRView","objId": "00000000-0000-0000-0000-000000000000","mvc": "1.0","vborder": 32,"visible": false,"vspeed": -1,"wport": 1024,"wview": 1024,"xport": 0,"xview": 0,"yport": 0,"yview": 0},
{"id": "9f7eaf8b-46b3-42d0-a028-a5ad6f9efa6b","hborder": 32,"hport": 768,"hspeed": -1,"hview": 768,"inherit": false,"modelName": "GMRView","objId": "00000000-0000-0000-0000-000000000000","mvc": "1.0","vborder": 32,"visible": false,"vspeed": -1,"wport": 1024,"wview": 1024,"xport": 0,"xview": 0,"yport": 0,"yview": 0},
{"id": "f5dcdb13-9a7a-44a9-8ebf-2b4a28838bb8","hborder": 32,"hport": 768,"hspeed": -1,"hview": 768,"inherit": false,"modelName": "GMRView","objId": "00000000-0000-0000-0000-000000000000","mvc": "1.0","vborder": 32,"visible": false,"vspeed": -1,"wport": 1024,"wview": 1024,"xport": 0,"xview": 0,"yport": 0,"yview": 0},
{"id": "85aeec43-87c6-45ac-b05d-3687efef7c9a","hborder": 32,"hport": 768,"hspeed": -1,"hview": 768,"inherit": false,"modelName": "GMRView","objId": "00000000-0000-0000-0000-000000000000","mvc": "1.0","vborder": 32,"visible": false,"vspeed": -1,"wport": 1024,"wview": 1024,"xport": 0,"xview": 0,"yport": 0,"yview": 0},
{"id": "2fadd91f-8217-4421-945e-0d43edfd96b5","hborder": 32,"hport": 768,"hspeed": -1,"hview": 768,"inherit": false,"modelName": "GMRView","objId": "00000000-0000-0000-0000-000000000000","mvc": "1.0","vborder": 32,"visible": false,"vspeed": -1,"wport": 1024,"wview": 1024,"xport": 0,"xview": 0,"yport": 0,"yview": 0}
    ],
    "viewSettings":     {
        "id": "b7912b5d-0aa6-4d42-a201-5b439d8a892a",
        "clearDisplayBuffer": true,
        "clearViewBackground": false,
        "enableViews": true,
        "inheritViewSettings": false,
        "modelName": "GMRoomViewSettings",
        "mvc": "1.0"
    }
}