{
    "id": "ccff5e16-e234-4833-af8c-8f91b5d4ee24",
    "modelName": "GMFont",
    "mvc": "1.1",
    "name": "fnt",
    "AntiAlias": 0,
    "TTFName": "",
    "ascenderOffset": 0,
    "bold": false,
    "charset": 0,
    "first": 0,
    "fontName": "Bahnschrift Light SemiCondensed",
    "glyphOperations": 0,
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "82c5262d-1f7e-448d-93ae-5c8cf2e46f5d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 13,
                "offset": 0,
                "shift": 2,
                "w": 2,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "33bd1ad0-7196-4210-95e1-d30a6736b1a8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 13,
                "offset": 1,
                "shift": 3,
                "w": 1,
                "x": 58,
                "y": 47
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "e8bbdc8d-6600-48e1-bef8-b15267074fd4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 13,
                "offset": 1,
                "shift": 4,
                "w": 2,
                "x": 54,
                "y": 47
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "2030f37c-622d-46ef-b767-ffd0f0a9ef5f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 13,
                "offset": 0,
                "shift": 7,
                "w": 6,
                "x": 46,
                "y": 47
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "0ca75447-b54e-42bd-875f-899ef401ccf5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 13,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 38,
                "y": 47
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "9c77d4b9-c714-4a8c-a83d-2795068b8f19",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 13,
                "offset": 1,
                "shift": 8,
                "w": 6,
                "x": 30,
                "y": 47
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "68d60c71-8715-49f4-aef1-7d74d8eaaf68",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 13,
                "offset": 1,
                "shift": 7,
                "w": 6,
                "x": 22,
                "y": 47
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "8c418315-aff4-4163-be3d-85422be09919",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 13,
                "offset": 0,
                "shift": 2,
                "w": 2,
                "x": 18,
                "y": 47
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "de68b2c8-90a9-4e94-8184-1b724e403239",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 13,
                "offset": 1,
                "shift": 4,
                "w": 3,
                "x": 13,
                "y": 47
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "f5f395d5-ffae-4da9-a8b2-795164ad02a4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 13,
                "offset": 0,
                "shift": 4,
                "w": 3,
                "x": 8,
                "y": 47
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "2bb6aec9-a4da-498e-9bf0-43328b746190",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 13,
                "offset": 0,
                "shift": 5,
                "w": 4,
                "x": 61,
                "y": 47
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "591816d0-0e29-46a9-9ffa-ca02c80b8e02",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 13,
                "offset": 1,
                "shift": 6,
                "w": 4,
                "x": 2,
                "y": 47
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "1b6cbdf7-2c2e-4b44-93df-161f3c5cff7e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 13,
                "offset": 1,
                "shift": 3,
                "w": 1,
                "x": 114,
                "y": 32
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "046b951d-68fc-4b7a-b028-38454bf3086e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 13,
                "offset": 1,
                "shift": 5,
                "w": 3,
                "x": 109,
                "y": 32
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "adaa2156-c4c8-4c0d-ba36-eae2afa8766c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 13,
                "offset": 1,
                "shift": 3,
                "w": 1,
                "x": 106,
                "y": 32
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "45b53a70-e429-4226-b5f0-c78c16195ca2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 13,
                "offset": 0,
                "shift": 4,
                "w": 5,
                "x": 99,
                "y": 32
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "15c7a436-836e-4e9c-a048-3c63a6281149",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 13,
                "offset": 1,
                "shift": 6,
                "w": 4,
                "x": 93,
                "y": 32
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "f6e6e920-0f93-4be1-a8f8-be78c10153a0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 13,
                "offset": 0,
                "shift": 4,
                "w": 3,
                "x": 88,
                "y": 32
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "00928c90-378d-4dfb-bb30-4347e0a79ef1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 13,
                "offset": 1,
                "shift": 6,
                "w": 4,
                "x": 82,
                "y": 32
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "2eafd192-4f01-4100-b6c3-e71c6e055530",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 13,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 75,
                "y": 32
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "0f8d128a-1a2b-4f27-859e-38be26f88625",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 13,
                "offset": 1,
                "shift": 6,
                "w": 5,
                "x": 68,
                "y": 32
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "28ab3d41-7e76-45fb-8738-21c734a22f0b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 13,
                "offset": 1,
                "shift": 6,
                "w": 4,
                "x": 117,
                "y": 32
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "3ebe19fd-4967-4dee-afb8-7fe2cfef6ea6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 13,
                "offset": 1,
                "shift": 6,
                "w": 4,
                "x": 67,
                "y": 47
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "e442bb90-3be5-44a6-b23f-f5f7aa394a68",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 13,
                "offset": 1,
                "shift": 6,
                "w": 4,
                "x": 73,
                "y": 47
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "6afff75e-953c-4164-b67a-f8ff8bf64558",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 13,
                "offset": 1,
                "shift": 6,
                "w": 4,
                "x": 79,
                "y": 47
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "12ef6629-8238-4a55-917d-bd126e6d386b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 13,
                "offset": 1,
                "shift": 6,
                "w": 4,
                "x": 91,
                "y": 62
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "cb3796a8-7091-4838-a13f-3225d49addb4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 13,
                "offset": 1,
                "shift": 3,
                "w": 1,
                "x": 88,
                "y": 62
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "be362b7a-a939-48a1-930f-48a694355bb8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 13,
                "offset": 1,
                "shift": 3,
                "w": 1,
                "x": 85,
                "y": 62
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "3f5a5c53-3a46-47db-845f-32d36790fec0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 13,
                "offset": 0,
                "shift": 5,
                "w": 4,
                "x": 79,
                "y": 62
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "394465de-bb56-4932-a705-f01f7030362a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 13,
                "offset": 1,
                "shift": 5,
                "w": 3,
                "x": 74,
                "y": 62
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "d8cfeab6-5441-40f9-9f94-c5335afb622f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 13,
                "offset": 1,
                "shift": 5,
                "w": 3,
                "x": 69,
                "y": 62
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "1846cc09-9d3c-4b33-8eb5-fd5e472adc50",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 13,
                "offset": 0,
                "shift": 5,
                "w": 4,
                "x": 63,
                "y": 62
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "5bf0ee7d-6c77-454b-a976-60d305f38803",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 13,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 53,
                "y": 62
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "ff834fa2-9a81-42b6-a648-281cfcecf459",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 13,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 44,
                "y": 62
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "4f74ddf2-111c-4d0a-988c-86c10757e7fa",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 13,
                "offset": 1,
                "shift": 7,
                "w": 5,
                "x": 37,
                "y": 62
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "e3beb98b-419f-453c-9147-ec8f89e8e4e3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 13,
                "offset": 1,
                "shift": 7,
                "w": 5,
                "x": 30,
                "y": 62
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "52e594ba-701d-4bcc-999c-b8628f691b9b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 13,
                "offset": 1,
                "shift": 7,
                "w": 5,
                "x": 23,
                "y": 62
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "2b07fe7e-3d70-4eb1-bc7d-7049297be980",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 13,
                "offset": 1,
                "shift": 7,
                "w": 5,
                "x": 16,
                "y": 62
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "bc1d03a1-836e-4818-80f2-fc496a06d0a0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 13,
                "offset": 1,
                "shift": 6,
                "w": 5,
                "x": 9,
                "y": 62
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "4870bb87-e106-4fb2-aa82-d3901286d0fd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 13,
                "offset": 1,
                "shift": 7,
                "w": 5,
                "x": 2,
                "y": 62
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "1b3be843-3404-41e9-9de8-5d9f5b748fa7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 13,
                "offset": 1,
                "shift": 7,
                "w": 6,
                "x": 119,
                "y": 47
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "668f57e5-cdb9-4eb9-923d-d37f11888082",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 13,
                "offset": 1,
                "shift": 3,
                "w": 1,
                "x": 116,
                "y": 47
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "473904a8-b0be-407c-8815-3ca439457a3a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 13,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 109,
                "y": 47
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "25a9fcdc-e825-4829-a454-557af1bc73a8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 13,
                "offset": 1,
                "shift": 7,
                "w": 6,
                "x": 101,
                "y": 47
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "8124e63e-7cf8-4ec2-b42d-2332cee95e45",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 13,
                "offset": 1,
                "shift": 6,
                "w": 5,
                "x": 94,
                "y": 47
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "01b30703-bb29-4700-8032-ef771b49205f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 13,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 85,
                "y": 47
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "0cd1c1fd-47fd-4f03-8352-7e1f48af484b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 13,
                "offset": 1,
                "shift": 8,
                "w": 6,
                "x": 60,
                "y": 32
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "11b94d17-4f62-4f5d-9c7f-536a738c6007",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 13,
                "offset": 1,
                "shift": 7,
                "w": 5,
                "x": 53,
                "y": 32
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "8430ed55-6750-4541-bee8-45617051e2ac",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 13,
                "offset": 1,
                "shift": 7,
                "w": 5,
                "x": 46,
                "y": 32
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "41b97d30-4d3e-4ea0-ba4e-eafd028ee088",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 13,
                "offset": 1,
                "shift": 7,
                "w": 6,
                "x": 22,
                "y": 17
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "385cc4cb-f625-4fcb-8364-fc7191de14b0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 13,
                "offset": 1,
                "shift": 7,
                "w": 6,
                "x": 10,
                "y": 17
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "acd3f2f0-66c2-4560-be7a-f776ae2732f4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 13,
                "offset": 0,
                "shift": 7,
                "w": 6,
                "x": 2,
                "y": 17
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "dbeb5e42-a69d-401a-a2d9-9ad34fee4ed4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 13,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 118,
                "y": 2
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "d9fb1b18-77d8-4c2d-8fe7-e57487369254",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 13,
                "offset": 1,
                "shift": 7,
                "w": 5,
                "x": 111,
                "y": 2
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "c16b05ec-5033-4d43-8ead-e545c36c6a19",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 13,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 103,
                "y": 2
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "476fc41c-5040-468f-891f-6d16a8f888f3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 13,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 92,
                "y": 2
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "5abfdac1-7321-4508-92cc-1354072b6210",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 13,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 84,
                "y": 2
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "0258dbab-5284-4b2e-b0d0-239d60432968",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 13,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 76,
                "y": 2
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "725f3da7-2111-45dc-9fa9-3b4a76b544ac",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 13,
                "offset": 1,
                "shift": 6,
                "w": 5,
                "x": 69,
                "y": 2
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "41fdd735-8f30-4c1e-855d-229c693fdf01",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 13,
                "offset": 1,
                "shift": 3,
                "w": 2,
                "x": 18,
                "y": 17
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "ef170a44-b943-44ef-bc2b-b7e967af1d9c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 13,
                "offset": 0,
                "shift": 4,
                "w": 4,
                "x": 63,
                "y": 2
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "f29bb394-e04e-49dc-bda9-24a6fff85863",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 13,
                "offset": 0,
                "shift": 3,
                "w": 2,
                "x": 54,
                "y": 2
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "185752ef-e26d-44bb-aa0b-49d24373facf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 13,
                "offset": 1,
                "shift": 6,
                "w": 4,
                "x": 48,
                "y": 2
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "a705671f-7403-4b1c-8e3d-cccbbbc9681f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 13,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 41,
                "y": 2
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "9f450859-afb1-4c72-9207-ddf1a4eb47de",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 13,
                "offset": 0,
                "shift": 3,
                "w": 3,
                "x": 36,
                "y": 2
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "28f5f5e7-8bb3-4c13-b626-966473156915",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 13,
                "offset": 1,
                "shift": 6,
                "w": 4,
                "x": 30,
                "y": 2
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "205b486e-63cd-4110-87ca-df8db15b4a57",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 13,
                "offset": 1,
                "shift": 6,
                "w": 4,
                "x": 24,
                "y": 2
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "7b6c20ba-0b6e-400a-8005-a6d47cc507f5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 13,
                "offset": 1,
                "shift": 5,
                "w": 4,
                "x": 18,
                "y": 2
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "ee654534-20ca-47cf-bfaf-5320e1bbd6a3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 13,
                "offset": 1,
                "shift": 6,
                "w": 4,
                "x": 12,
                "y": 2
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "01d83540-ce96-4ae8-90a9-e185750d063d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 13,
                "offset": 1,
                "shift": 6,
                "w": 4,
                "x": 6,
                "y": 2
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "7e4df1e5-8930-40d5-af2d-967c6717f7c8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 13,
                "offset": 0,
                "shift": 3,
                "w": 3,
                "x": 58,
                "y": 2
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "200365c6-042a-4625-a7f8-6ad70936d95c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 13,
                "offset": 1,
                "shift": 6,
                "w": 4,
                "x": 30,
                "y": 17
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "41facbcb-ffb3-4d88-bd53-208c06cf296f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 13,
                "offset": 1,
                "shift": 6,
                "w": 4,
                "x": 95,
                "y": 17
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "47551350-6bb4-47c0-ad17-da0d4992e4fb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 13,
                "offset": 1,
                "shift": 3,
                "w": 1,
                "x": 36,
                "y": 17
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "4b0c8f73-6980-4e16-880f-ebfada2da21a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 13,
                "offset": 0,
                "shift": 3,
                "w": 2,
                "x": 35,
                "y": 32
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "ea2e24be-da41-45cf-9fca-249199ee63c9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 13,
                "offset": 1,
                "shift": 6,
                "w": 5,
                "x": 28,
                "y": 32
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "4f95b43e-0058-4aa2-bc99-7b84c71544c9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 13,
                "offset": 1,
                "shift": 3,
                "w": 2,
                "x": 24,
                "y": 32
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "748fc4e3-ae06-4dd0-8ad0-8c3baaff6c49",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 13,
                "offset": 1,
                "shift": 9,
                "w": 8,
                "x": 14,
                "y": 32
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "4dfabaa4-27a7-41ac-8858-49d069422f80",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 13,
                "offset": 1,
                "shift": 6,
                "w": 4,
                "x": 8,
                "y": 32
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "db30a2e2-7162-419e-8038-b723c0d04fb9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 13,
                "offset": 1,
                "shift": 6,
                "w": 4,
                "x": 2,
                "y": 32
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "ed8a5413-9120-43e9-81a3-55f11e49ce1b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 13,
                "offset": 1,
                "shift": 6,
                "w": 4,
                "x": 118,
                "y": 17
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "85580a7f-c6c4-43a1-9c64-76a5ab84a06c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 13,
                "offset": 1,
                "shift": 6,
                "w": 4,
                "x": 112,
                "y": 17
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "77d8571d-b5ba-481e-9b71-c6dc2752bc8c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 13,
                "offset": 1,
                "shift": 5,
                "w": 4,
                "x": 106,
                "y": 17
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "34cf0a4f-1fdf-47a6-ba49-cb2469d6af84",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 13,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 39,
                "y": 32
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "44a2812b-a500-4597-b09c-debfa47cce16",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 13,
                "offset": 0,
                "shift": 4,
                "w": 3,
                "x": 101,
                "y": 17
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "273039a7-5aa8-46d9-9a15-e6824cb6af36",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 13,
                "offset": 1,
                "shift": 6,
                "w": 4,
                "x": 89,
                "y": 17
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "bed18c5c-7f0d-420f-96d4-a8157a425291",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 13,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 82,
                "y": 17
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "a5e898e9-37b5-4c97-b509-f0ff933b80cc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 13,
                "offset": 0,
                "shift": 9,
                "w": 8,
                "x": 72,
                "y": 17
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "79788398-2d0c-494a-a4a1-f4d1ab81d391",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 13,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 65,
                "y": 17
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "a9e41672-9216-4e68-87f6-4664e7bcee62",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 13,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 58,
                "y": 17
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "1f40385e-03d5-4013-aa5f-9e4276962cc6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 13,
                "offset": 1,
                "shift": 5,
                "w": 4,
                "x": 52,
                "y": 17
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "6a3cd303-ecc9-4f45-86ef-2fe37794a44a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 13,
                "offset": 1,
                "shift": 4,
                "w": 3,
                "x": 47,
                "y": 17
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "46923aa2-c01f-4562-bb92-67f42db6255e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 13,
                "offset": 1,
                "shift": 3,
                "w": 1,
                "x": 44,
                "y": 17
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "bc781d7e-3272-4f9f-8664-b902d017ab8e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 13,
                "offset": 0,
                "shift": 4,
                "w": 3,
                "x": 39,
                "y": 17
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "775a5720-4a43-4e47-b5a1-ee357871c3bc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 13,
                "offset": 1,
                "shift": 6,
                "w": 4,
                "x": 97,
                "y": 62
            }
        },
        {
            "Key": 9647,
            "Value": {
                "id": "eb96de73-a7c0-441f-983a-f688124ed9c9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 9647,
                "h": 13,
                "offset": 2,
                "shift": 11,
                "w": 6,
                "x": 103,
                "y": 62
            }
        }
    ],
    "hinting": 0,
    "includeTTF": false,
    "interpreter": 0,
    "italic": false,
    "kerningPairs": [
        
    ],
    "last": 0,
    "maintainGms1Font": false,
    "pointRounding": 0,
    "ranges": [
        {
            "x": 32,
            "y": 127
        },
        {
            "x": 9647,
            "y": 9647
        }
    ],
    "sampleText": "abcdef ABCDEF\\u000a0123456789 .,<>\"'&!?\\u000athe quick brown fox jumps over the lazy dog\\u000aTHE QUICK BROWN FOX JUMPS OVER THE LAZY DOG\\u000aDefault character: ▯ (9647)",
    "size": 8,
    "styleName": "Regular",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}