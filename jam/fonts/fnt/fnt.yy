{
    "id": "ccff5e16-e234-4833-af8c-8f91b5d4ee24",
    "modelName": "GMFont",
    "mvc": "1.1",
    "name": "fnt",
    "AntiAlias": 0,
    "TTFName": "",
    "ascenderOffset": 0,
    "bold": false,
    "charset": 0,
    "first": 0,
    "fontName": "Bahnschrift Light SemiCondensed",
    "glyphOperations": 0,
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "9d2e9edd-d816-4683-b4c9-821f60c9dfcb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 13,
                "offset": 0,
                "shift": 2,
                "w": 2,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "43a3bba3-9940-46d0-bca8-9f23b3d63471",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 13,
                "offset": 1,
                "shift": 3,
                "w": 1,
                "x": 58,
                "y": 47
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "0c1557a6-03ed-469d-a0f5-df9be95feec2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 13,
                "offset": 1,
                "shift": 4,
                "w": 2,
                "x": 54,
                "y": 47
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "f72c918b-3aa1-495d-82c8-a90c6fc314dd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 13,
                "offset": 0,
                "shift": 7,
                "w": 6,
                "x": 46,
                "y": 47
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "63f06363-9253-4dd7-811c-78021cc57e02",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 13,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 38,
                "y": 47
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "0eec5fc9-cb02-4691-9f88-7be37a49b257",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 13,
                "offset": 1,
                "shift": 8,
                "w": 6,
                "x": 30,
                "y": 47
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "86e070c0-2b71-47ce-a3b9-1996d535dd6c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 13,
                "offset": 1,
                "shift": 7,
                "w": 6,
                "x": 22,
                "y": 47
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "e80b4c7d-1022-4fa5-a12c-4cb1830eb927",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 13,
                "offset": 0,
                "shift": 2,
                "w": 2,
                "x": 18,
                "y": 47
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "4b77412f-0c3e-4cb1-ba33-65fd986e8f6b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 13,
                "offset": 1,
                "shift": 4,
                "w": 3,
                "x": 13,
                "y": 47
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "a04f38ba-3541-40dc-9691-075199250367",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 13,
                "offset": 0,
                "shift": 4,
                "w": 3,
                "x": 8,
                "y": 47
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "556fcaff-7851-4bfe-8459-6f840fc6e47d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 13,
                "offset": 0,
                "shift": 5,
                "w": 4,
                "x": 61,
                "y": 47
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "24b5365a-c0a2-429f-b9ff-4992bf15ae8a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 13,
                "offset": 1,
                "shift": 6,
                "w": 4,
                "x": 2,
                "y": 47
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "f9bc0a69-aed7-41de-8780-b76fcdb210d3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 13,
                "offset": 1,
                "shift": 3,
                "w": 1,
                "x": 114,
                "y": 32
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "2e822919-fc5a-433a-aebf-059ffe4611fc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 13,
                "offset": 1,
                "shift": 5,
                "w": 3,
                "x": 109,
                "y": 32
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "7d6b9a46-dd78-484c-a5f7-e98d4440957a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 13,
                "offset": 1,
                "shift": 3,
                "w": 1,
                "x": 106,
                "y": 32
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "0dbfe893-d98a-4fa3-b0fd-7e1feb517a49",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 13,
                "offset": 0,
                "shift": 4,
                "w": 5,
                "x": 99,
                "y": 32
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "3589c7ef-5b5f-40b6-a343-9113244b4b49",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 13,
                "offset": 1,
                "shift": 6,
                "w": 4,
                "x": 93,
                "y": 32
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "4b6d65f2-da3e-4c55-a53e-144c305e9962",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 13,
                "offset": 0,
                "shift": 4,
                "w": 3,
                "x": 88,
                "y": 32
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "08a4df90-e315-4ae3-a632-191089d06064",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 13,
                "offset": 1,
                "shift": 6,
                "w": 4,
                "x": 82,
                "y": 32
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "0b6ef7b7-ef9b-42c2-8173-87d0711ba763",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 13,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 75,
                "y": 32
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "7613f12f-48d4-4869-bd4f-8a4025827803",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 13,
                "offset": 1,
                "shift": 6,
                "w": 5,
                "x": 68,
                "y": 32
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "934f797a-4ac2-4da5-b15c-890fef9196c4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 13,
                "offset": 1,
                "shift": 6,
                "w": 4,
                "x": 117,
                "y": 32
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "92fcbcee-4c18-495b-bf4e-579db1550ee0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 13,
                "offset": 1,
                "shift": 6,
                "w": 4,
                "x": 67,
                "y": 47
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "ad4e4281-868a-430b-b74a-6375c047e935",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 13,
                "offset": 1,
                "shift": 6,
                "w": 4,
                "x": 73,
                "y": 47
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "fb59cfff-3fa4-43ff-997e-cec5a0ba9197",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 13,
                "offset": 1,
                "shift": 6,
                "w": 4,
                "x": 79,
                "y": 47
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "33583d11-3da8-4a80-9634-efb519b3a351",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 13,
                "offset": 1,
                "shift": 6,
                "w": 4,
                "x": 91,
                "y": 62
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "03b3c9be-bc9a-44a9-9fb0-f13a380cfe67",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 13,
                "offset": 1,
                "shift": 3,
                "w": 1,
                "x": 88,
                "y": 62
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "0d580c3c-9cac-40b1-b229-d91be83f7133",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 13,
                "offset": 1,
                "shift": 3,
                "w": 1,
                "x": 85,
                "y": 62
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "5e00cc2e-34d0-49ba-a090-a91d9ac0e0cd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 13,
                "offset": 0,
                "shift": 5,
                "w": 4,
                "x": 79,
                "y": 62
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "85291dbe-e4cc-4d3f-93df-ea7992f6ec6b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 13,
                "offset": 1,
                "shift": 5,
                "w": 3,
                "x": 74,
                "y": 62
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "bcdcaccd-bbdf-45b2-96e9-cac10fbd6c2e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 13,
                "offset": 1,
                "shift": 5,
                "w": 3,
                "x": 69,
                "y": 62
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "26ee2bc2-e1ab-4ea2-8cc3-926fbfd0ae27",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 13,
                "offset": 0,
                "shift": 5,
                "w": 4,
                "x": 63,
                "y": 62
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "5adbd337-7d4d-4188-85e7-20212c8dca3f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 13,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 53,
                "y": 62
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "785cee22-b1fc-4509-a654-7d4778c32634",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 13,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 44,
                "y": 62
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "f07a6f23-831c-4806-8b2e-6d6654fcc243",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 13,
                "offset": 1,
                "shift": 7,
                "w": 5,
                "x": 37,
                "y": 62
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "61a5ec55-9cf8-4c22-8569-283d36730ae4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 13,
                "offset": 1,
                "shift": 7,
                "w": 5,
                "x": 30,
                "y": 62
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "fcc45308-9061-4343-a1a1-696d91795e9e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 13,
                "offset": 1,
                "shift": 7,
                "w": 5,
                "x": 23,
                "y": 62
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "c6ad388c-1924-4e2e-b23d-fe09be3749a0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 13,
                "offset": 1,
                "shift": 7,
                "w": 5,
                "x": 16,
                "y": 62
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "c33db34a-08b6-409f-9cf5-999e7e3535e8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 13,
                "offset": 1,
                "shift": 6,
                "w": 5,
                "x": 9,
                "y": 62
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "bf1e03ba-1ec7-44f4-9e4b-258e16170e1f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 13,
                "offset": 1,
                "shift": 7,
                "w": 5,
                "x": 2,
                "y": 62
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "47502682-d734-4458-ba32-1964f88f3131",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 13,
                "offset": 1,
                "shift": 7,
                "w": 6,
                "x": 119,
                "y": 47
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "042b13f3-50ca-4fc2-8e76-8c8797e7eccc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 13,
                "offset": 1,
                "shift": 3,
                "w": 1,
                "x": 116,
                "y": 47
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "cd1af813-4daf-4a14-8cb1-f258b787f9d8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 13,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 109,
                "y": 47
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "7bac8a1e-1172-4bf6-9a18-cd1171c23c7b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 13,
                "offset": 1,
                "shift": 7,
                "w": 6,
                "x": 101,
                "y": 47
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "267b0273-a965-4bd9-b437-40103e9b7ac1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 13,
                "offset": 1,
                "shift": 6,
                "w": 5,
                "x": 94,
                "y": 47
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "0cbdaeac-2e17-45aa-ad2c-4a00e1d9c972",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 13,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 85,
                "y": 47
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "eb7e3580-cb69-4cf7-b25c-d597e4effa17",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 13,
                "offset": 1,
                "shift": 8,
                "w": 6,
                "x": 60,
                "y": 32
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "e9518a81-d830-42e2-a319-be04f5916d81",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 13,
                "offset": 1,
                "shift": 7,
                "w": 5,
                "x": 53,
                "y": 32
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "65c29e35-c0b9-4876-8a8b-5169c91d4fae",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 13,
                "offset": 1,
                "shift": 7,
                "w": 5,
                "x": 46,
                "y": 32
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "9f316886-e09e-4357-a06e-fabe6779271f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 13,
                "offset": 1,
                "shift": 7,
                "w": 6,
                "x": 22,
                "y": 17
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "7e8f448a-943f-4eb3-98fc-2224c7669c84",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 13,
                "offset": 1,
                "shift": 7,
                "w": 6,
                "x": 10,
                "y": 17
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "00e30389-12f1-4ddd-a8eb-f911eb909c8c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 13,
                "offset": 0,
                "shift": 7,
                "w": 6,
                "x": 2,
                "y": 17
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "fedce50c-abe8-4aa0-aec7-2e7c058bae8a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 13,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 118,
                "y": 2
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "2a73f837-5f95-44c1-bf90-cd4926072ce7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 13,
                "offset": 1,
                "shift": 7,
                "w": 5,
                "x": 111,
                "y": 2
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "fc391b1d-024a-4ba8-8ff7-0d511f883a75",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 13,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 103,
                "y": 2
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "740af7f8-fb6f-48a7-93a1-255b10ccf476",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 13,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 92,
                "y": 2
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "94c3d101-5889-4d76-8553-ebcb4a20c842",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 13,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 84,
                "y": 2
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "c06d2da6-f87d-4f19-b1f0-73dc9910c1ef",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 13,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 76,
                "y": 2
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "04589ccb-d156-4840-9b9d-72595ce4f0fc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 13,
                "offset": 1,
                "shift": 6,
                "w": 5,
                "x": 69,
                "y": 2
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "7c5e9381-464f-4421-8cb3-49adad7cc3e9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 13,
                "offset": 1,
                "shift": 3,
                "w": 2,
                "x": 18,
                "y": 17
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "0ab0f8e1-303d-43a7-8e5a-3f64d57088a2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 13,
                "offset": 0,
                "shift": 4,
                "w": 4,
                "x": 63,
                "y": 2
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "5a5d28a1-dfb8-44d0-b821-c35b97f9c6ca",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 13,
                "offset": 0,
                "shift": 3,
                "w": 2,
                "x": 54,
                "y": 2
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "4daf45b8-e2c6-40e9-ada8-397cda94c5ef",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 13,
                "offset": 1,
                "shift": 6,
                "w": 4,
                "x": 48,
                "y": 2
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "60173f58-73e5-422e-a144-91036daa8ee1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 13,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 41,
                "y": 2
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "add83e21-4795-40ba-81f5-fe9bedab2f58",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 13,
                "offset": 0,
                "shift": 3,
                "w": 3,
                "x": 36,
                "y": 2
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "755eb005-dde2-4d36-aef1-12b6013311ba",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 13,
                "offset": 1,
                "shift": 6,
                "w": 4,
                "x": 30,
                "y": 2
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "3960426e-dc82-491d-8ded-bd983f90d423",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 13,
                "offset": 1,
                "shift": 6,
                "w": 4,
                "x": 24,
                "y": 2
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "64d45a38-1b3e-4bef-ad45-92ce6cdc322b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 13,
                "offset": 1,
                "shift": 5,
                "w": 4,
                "x": 18,
                "y": 2
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "28b3cd8b-c518-460b-ae13-ce87b6f6e380",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 13,
                "offset": 1,
                "shift": 6,
                "w": 4,
                "x": 12,
                "y": 2
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "8c856ba5-e8f1-484d-b2e5-00abdfd325df",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 13,
                "offset": 1,
                "shift": 6,
                "w": 4,
                "x": 6,
                "y": 2
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "7c1364e3-c17d-46e1-be9a-ad5b226073bf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 13,
                "offset": 0,
                "shift": 3,
                "w": 3,
                "x": 58,
                "y": 2
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "2a78fec4-a23a-4787-850e-24886d1092e1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 13,
                "offset": 1,
                "shift": 6,
                "w": 4,
                "x": 30,
                "y": 17
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "3090e0bb-cccf-426f-a84e-58dfa72e3b3a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 13,
                "offset": 1,
                "shift": 6,
                "w": 4,
                "x": 95,
                "y": 17
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "0f63b8c5-8537-436f-9182-cb74c721bc19",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 13,
                "offset": 1,
                "shift": 3,
                "w": 1,
                "x": 36,
                "y": 17
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "44ea15ad-21eb-451e-bdcb-76afab340d4c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 13,
                "offset": 0,
                "shift": 3,
                "w": 2,
                "x": 35,
                "y": 32
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "6c017748-785b-4d26-945e-c30c357a9030",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 13,
                "offset": 1,
                "shift": 6,
                "w": 5,
                "x": 28,
                "y": 32
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "c19b40eb-ba34-4b4c-8cdc-5d250c445ddc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 13,
                "offset": 1,
                "shift": 3,
                "w": 2,
                "x": 24,
                "y": 32
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "015001cc-32c6-41ed-8052-5b34a8747a10",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 13,
                "offset": 1,
                "shift": 9,
                "w": 8,
                "x": 14,
                "y": 32
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "b49995ca-3697-40cb-9120-d6e111196bb1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 13,
                "offset": 1,
                "shift": 6,
                "w": 4,
                "x": 8,
                "y": 32
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "d9404769-ecfb-4b9a-b971-037d3fcc3abe",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 13,
                "offset": 1,
                "shift": 6,
                "w": 4,
                "x": 2,
                "y": 32
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "cf6083bb-65ed-4c70-a47a-c8b6aa1109e9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 13,
                "offset": 1,
                "shift": 6,
                "w": 4,
                "x": 118,
                "y": 17
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "1f13ce76-d739-447b-b21c-449f2822dfb5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 13,
                "offset": 1,
                "shift": 6,
                "w": 4,
                "x": 112,
                "y": 17
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "b744455e-3031-43c4-a567-4d5e3fe5b7b8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 13,
                "offset": 1,
                "shift": 5,
                "w": 4,
                "x": 106,
                "y": 17
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "0be9e7f7-ab8a-4261-8982-62b0ea268928",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 13,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 39,
                "y": 32
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "348c35ea-1e6a-4baf-96a3-3cbb65f96b32",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 13,
                "offset": 0,
                "shift": 4,
                "w": 3,
                "x": 101,
                "y": 17
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "b3774c92-a893-4a38-97cf-8d76d8a6a476",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 13,
                "offset": 1,
                "shift": 6,
                "w": 4,
                "x": 89,
                "y": 17
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "a895446e-a57f-4463-9c2f-e960a54e0910",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 13,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 82,
                "y": 17
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "1d87f764-fc37-4045-95d3-d097fc33d4f7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 13,
                "offset": 0,
                "shift": 9,
                "w": 8,
                "x": 72,
                "y": 17
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "e630f5a4-bab8-4869-929f-c70b2713844a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 13,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 65,
                "y": 17
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "162cdc63-c3c3-4c41-9cee-96a8a08d86b0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 13,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 58,
                "y": 17
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "cefa36a7-afb9-4625-a934-5b8df5b2c19e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 13,
                "offset": 1,
                "shift": 5,
                "w": 4,
                "x": 52,
                "y": 17
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "73ee9c59-e2c9-4e36-946f-1293951d33a6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 13,
                "offset": 1,
                "shift": 4,
                "w": 3,
                "x": 47,
                "y": 17
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "0338169c-1249-4510-9249-0b344a9a9ce8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 13,
                "offset": 1,
                "shift": 3,
                "w": 1,
                "x": 44,
                "y": 17
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "f72cd042-6cf1-4b3d-9d30-5f04557f0833",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 13,
                "offset": 0,
                "shift": 4,
                "w": 3,
                "x": 39,
                "y": 17
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "b5091240-fe17-44cb-be89-a3024eb848ad",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 13,
                "offset": 1,
                "shift": 6,
                "w": 4,
                "x": 97,
                "y": 62
            }
        },
        {
            "Key": 9647,
            "Value": {
                "id": "8ed01edd-ad65-4708-9c8e-7bc3ee3d12bd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 9647,
                "h": 13,
                "offset": 2,
                "shift": 11,
                "w": 6,
                "x": 103,
                "y": 62
            }
        }
    ],
    "hinting": 0,
    "includeTTF": false,
    "interpreter": 0,
    "italic": false,
    "kerningPairs": [
        
    ],
    "last": 0,
    "maintainGms1Font": false,
    "pointRounding": 0,
    "ranges": [
        {
            "x": 32,
            "y": 127
        },
        {
            "x": 9647,
            "y": 9647
        }
    ],
    "sampleText": "abcdef ABCDEF\\u000a0123456789 .,<>\"'&!?\\u000athe quick brown fox jumps over the lazy dog\\u000aTHE QUICK BROWN FOX JUMPS OVER THE LAZY DOG\\u000aDefault character: ▯ (9647)",
    "size": 8,
    "styleName": "Regular",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}