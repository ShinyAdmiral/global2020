{
    "id": "5a8ecca6-e8ea-4992-83d6-909a9cf745e5",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_enemyJab",
    "eventList": [
        
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": [
        {
            "id": "bc40590f-f00a-4ba0-acdc-a4c07f89fc78",
            "modelName": "GMOverriddenProperty",
            "mvc": "1.0",
            "objectId": "933839b8-24f5-4e0d-a2d1-a01d1293106c",
            "propertyId": "22f69857-8df0-44ef-8349-9f99c3f7622e",
            "value": "True"
        },
        {
            "id": "e0daf32b-4d71-4ae8-8ef8-f40478d4839a",
            "modelName": "GMOverriddenProperty",
            "mvc": "1.0",
            "objectId": "933839b8-24f5-4e0d-a2d1-a01d1293106c",
            "propertyId": "c10c8a12-58c0-4e31-aceb-6f08dc8be41f",
            "value": "3"
        }
    ],
    "parentObjectId": "933839b8-24f5-4e0d-a2d1-a01d1293106c",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "54afa862-f8af-4f16-bab3-7719d59fd1ad",
    "visible": true
}