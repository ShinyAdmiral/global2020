{
    "id": "a4a3fc7b-7975-433e-bc33-a861cb10ba57",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_enemySweep",
    "eventList": [
        
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": [
        
    ],
    "parentObjectId": "933839b8-24f5-4e0d-a2d1-a01d1293106c",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "d39cc17f-4bc9-47c4-a86c-aa4ff08aa985",
    "visible": true
}