{
    "id": "05ca3930-518f-48b4-afca-a882c2eac93c",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_hintToRight",
    "eventList": [
        {
            "id": "c7e01c0d-87c4-45ad-bdf6-b694aef65970",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "05ca3930-518f-48b4-afca-a882c2eac93c"
        },
        {
            "id": "1af87064-40d8-492e-b714-144a9be7c20c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 10,
            "eventtype": 6,
            "m_owner": "05ca3930-518f-48b4-afca-a882c2eac93c"
        },
        {
            "id": "f997fbfa-ebef-42bb-8d7e-75baa9b5956f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 11,
            "eventtype": 6,
            "m_owner": "05ca3930-518f-48b4-afca-a882c2eac93c"
        },
        {
            "id": "ac6499d5-f3cd-49ff-9216-93b6f42e5cd1",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "05ca3930-518f-48b4-afca-a882c2eac93c"
        },
        {
            "id": "8505ee47-9038-4996-a69f-fe3f08eb1dfc",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 6,
            "m_owner": "05ca3930-518f-48b4-afca-a882c2eac93c"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": [
        {
            "id": "454befd9-0061-48ed-af99-e04c8c7a8cb0",
            "modelName": "GMObjectProperty",
            "mvc": "1.0",
            "listItems": null,
            "multiselect": false,
            "rangeEnabled": false,
            "rangeMax": 10,
            "rangeMin": 0,
            "resourceFilter": 1023,
            "value": "obama",
            "varName": "hintText",
            "varType": 2
        }
    ],
    "solid": false,
    "spriteId": "8c586a24-9055-4011-9827-4cea34ac9260",
    "visible": true
}