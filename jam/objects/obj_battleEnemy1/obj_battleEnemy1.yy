{
    "id": "a283155c-af76-4923-8c65-6b8add35ca77",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_battleEnemy1",
    "eventList": [
        
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": [
        {
            "id": "12d6063c-c0ef-46ba-9981-e71fe83f34ce",
            "modelName": "GMOverriddenProperty",
            "mvc": "1.0",
            "objectId": "ab0ddc07-19b6-4910-aecf-70b4be6d004e",
            "propertyId": "53f89a47-09fa-4ea1-8532-3107149acb2b",
            "value": "obj_enemyJab"
        },
        {
            "id": "d7e6718e-651e-423a-8b51-685f30beb490",
            "modelName": "GMOverriddenProperty",
            "mvc": "1.0",
            "objectId": "ab0ddc07-19b6-4910-aecf-70b4be6d004e",
            "propertyId": "79b81f97-cf32-4203-ae09-a7fe063fe016",
            "value": "4"
        },
        {
            "id": "e505bf8a-3586-4e36-9106-10ef19621c6e",
            "modelName": "GMOverriddenProperty",
            "mvc": "1.0",
            "objectId": "ab0ddc07-19b6-4910-aecf-70b4be6d004e",
            "propertyId": "30da5f80-42af-42fa-8ef5-29063f517884",
            "value": "0.75"
        }
    ],
    "parentObjectId": "ab0ddc07-19b6-4910-aecf-70b4be6d004e",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "e9405204-8ae5-473b-b13c-7d47e2e1f15d",
    "visible": true
}