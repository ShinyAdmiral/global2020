{
    "id": "4e97a29b-183d-49f6-8509-03735c2d1929",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_RightLegDefault",
    "eventList": [
        
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "8b1258c7-0f93-47cc-b0c4-3c5b85b89e57",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "48c98ed0-a296-431a-beab-e2873dca9f15",
    "visible": true
}