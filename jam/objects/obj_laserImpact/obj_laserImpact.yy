{
    "id": "a9b995f9-2e73-4983-b0b1-5c59fa7bbf5c",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_laserImpact",
    "eventList": [
        
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "2bf3fbb7-0b28-4f01-aa34-5810ce2e0d1e",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "a1f1c320-0449-4437-8916-1c49da08edaa",
    "visible": true
}