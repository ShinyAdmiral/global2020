{
    "id": "b186f4e3-b690-44b4-9a33-39127c2e088f",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_battlebot_ali",
    "eventList": [
        
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": [
        {
            "id": "724e2a90-730f-447e-99b0-c5c496c757cf",
            "modelName": "GMOverriddenProperty",
            "mvc": "1.0",
            "objectId": "5e45bd04-119a-4fa1-a822-c0ff2342673a",
            "propertyId": "64ae687f-d01c-4d9a-b0f5-893a7a9726cc",
            "value": "obj_attackPunch"
        },
        {
            "id": "0d0e591b-da94-40b2-b1f6-b1af8b1530ce",
            "modelName": "GMOverriddenProperty",
            "mvc": "1.0",
            "objectId": "5e45bd04-119a-4fa1-a822-c0ff2342673a",
            "propertyId": "bb048f5b-4d73-4dc6-9397-9b155e26fcdf",
            "value": "20"
        },
        {
            "id": "fd49cd48-049c-4d55-af94-31b0030b204b",
            "modelName": "GMOverriddenProperty",
            "mvc": "1.0",
            "objectId": "5e45bd04-119a-4fa1-a822-c0ff2342673a",
            "propertyId": "632b9632-a0ab-4a56-b0c2-f2d26b852e1c",
            "value": "4"
        }
    ],
    "parentObjectId": "5e45bd04-119a-4fa1-a822-c0ff2342673a",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "681d5c67-13b2-4788-aa91-ab360fe6da87",
    "visible": true
}