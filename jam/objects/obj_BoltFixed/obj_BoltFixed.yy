{
    "id": "63a6f17f-ffea-4ed3-8a9d-e8b81ff64db8",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_BoltFixed",
    "eventList": [
        {
            "id": "4f452f21-94bd-41d4-a034-2a77e5db2e35",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "63a6f17f-ffea-4ed3-8a9d-e8b81ff64db8"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "7b465159-394b-407e-84c4-0410e0852075",
    "visible": true
}