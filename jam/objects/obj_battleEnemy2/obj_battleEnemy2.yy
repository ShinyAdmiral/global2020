{
    "id": "46125afd-37a4-4ff0-9afc-d5df790f5ace",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_battleEnemy2",
    "eventList": [
        
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": [
        {
            "id": "1f0f844c-04f1-4f6a-a490-c18b6f5d9712",
            "modelName": "GMOverriddenProperty",
            "mvc": "1.0",
            "objectId": "ab0ddc07-19b6-4910-aecf-70b4be6d004e",
            "propertyId": "8321e759-916a-4133-963a-c007eb4a82ff",
            "value": "2"
        },
        {
            "id": "20d0a94c-8fee-4a36-b573-b36b5367a8cd",
            "modelName": "GMOverriddenProperty",
            "mvc": "1.0",
            "objectId": "ab0ddc07-19b6-4910-aecf-70b4be6d004e",
            "propertyId": "79b81f97-cf32-4203-ae09-a7fe063fe016",
            "value": "8"
        }
    ],
    "parentObjectId": "ab0ddc07-19b6-4910-aecf-70b4be6d004e",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "2437365e-6c32-4079-a474-4319cf354fda",
    "visible": true
}