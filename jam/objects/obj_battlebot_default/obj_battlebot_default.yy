{
    "id": "c7a36923-acea-4a9a-a97e-58286e3a6c79",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_battlebot_default",
    "eventList": [
        
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": [
        {
            "id": "5bded801-45aa-4af7-b98b-c42f1ec501a0",
            "modelName": "GMOverriddenProperty",
            "mvc": "1.0",
            "objectId": "5e45bd04-119a-4fa1-a822-c0ff2342673a",
            "propertyId": "bb048f5b-4d73-4dc6-9397-9b155e26fcdf",
            "value": "16"
        },
        {
            "id": "1bec7fe5-8dfc-4a0f-b8fb-e0fd11497e12",
            "modelName": "GMOverriddenProperty",
            "mvc": "1.0",
            "objectId": "5e45bd04-119a-4fa1-a822-c0ff2342673a",
            "propertyId": "b2a569b3-3828-44bb-a728-11de0b6d8628",
            "value": "0.6"
        }
    ],
    "parentObjectId": "5e45bd04-119a-4fa1-a822-c0ff2342673a",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "6d3993e1-e70a-4c12-939b-de81cb02dc5a",
    "visible": true
}