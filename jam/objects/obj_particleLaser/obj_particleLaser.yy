{
    "id": "9e610f2c-2ae0-4ba5-b860-a1d308df68b1",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_particleLaser",
    "eventList": [
        {
            "id": "6fce6ea5-2339-4f30-8184-2a4827eeccfd",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 7,
            "eventtype": 7,
            "m_owner": "9e610f2c-2ae0-4ba5-b860-a1d308df68b1"
        },
        {
            "id": "42df97f1-45a6-415b-80ca-8ca0e5a7ecad",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "9e610f2c-2ae0-4ba5-b860-a1d308df68b1"
        },
        {
            "id": "be500937-5da7-4c03-9b7c-7d16b2bee805",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "9e610f2c-2ae0-4ba5-b860-a1d308df68b1"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "a1f1c320-0449-4437-8916-1c49da08edaa",
    "visible": true
}