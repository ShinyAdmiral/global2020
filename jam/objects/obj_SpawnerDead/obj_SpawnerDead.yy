{
    "id": "b2c4b18d-e895-4bb0-aa68-99d1d0b1f2ed",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_SpawnerDead",
    "eventList": [
        {
            "id": "d8a45b06-4da1-472c-aeeb-23f678ddf1d6",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "b2c4b18d-e895-4bb0-aa68-99d1d0b1f2ed"
        },
        {
            "id": "f32d601c-2468-4c72-a0eb-b7bbd9da033d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "b2c4b18d-e895-4bb0-aa68-99d1d0b1f2ed"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}