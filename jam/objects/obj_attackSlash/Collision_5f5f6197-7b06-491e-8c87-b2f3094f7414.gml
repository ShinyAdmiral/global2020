var hitCount
hitCount = 0;

if(activate) {
	
	var _list = ds_list_create();
	var _num = instance_place_list(x, y, obj_battlefieldTestEnemy, _list, false);
	if (_num > 0) {
	
	    for (var i = 0; i < _num; ++i;) 
		{	
			if(instance_exists(_list[| i])){
				
				_list[| i].smacked = true;
				_list[| i].hp -= damage
				if(_list[| i].hp <= 0) {
					instance_destroy(_list[| i]);
				}
				hitCount++
			}
		
		}
	}
	activate = false;
	show_debug_message("SLASH! Hit " + string(hitCount));
	ds_list_destroy(_list);
}