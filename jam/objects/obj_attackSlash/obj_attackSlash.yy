{
    "id": "0c86d942-18ad-4baa-861e-30e9378e995f",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_attackSlash",
    "eventList": [
        {
            "id": "21fbeadf-7c98-44e2-934b-6435782af528",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "0c86d942-18ad-4baa-861e-30e9378e995f"
        },
        {
            "id": "5f5f6197-7b06-491e-8c87-b2f3094f7414",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "ab0ddc07-19b6-4910-aecf-70b4be6d004e",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "0c86d942-18ad-4baa-861e-30e9378e995f"
        },
        {
            "id": "945c6562-dac8-44c3-8087-e672446ec96e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "0c86d942-18ad-4baa-861e-30e9378e995f"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "2bf3fbb7-0b28-4f01-aa34-5810ce2e0d1e",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "d39cc17f-4bc9-47c4-a86c-aa4ff08aa985",
    "visible": true
}