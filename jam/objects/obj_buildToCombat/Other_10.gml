show_debug_message("PERCI ZONE---------------------------------");

//qualityScore = 1/qualityScore;
show_debug_message("Quality " + string(qualityScore))

robotName = string_delete(robotName,1,12)
robotName = string_lower(robotName);

show_debug_message(robotName)

actualRobot = noone;

switch (robotName) {
	case "default":
		actualRobot = obj_battlebot_default
		break;
	case "butler":
		actualRobot = obj_battlebot_butler
		break;
	case "deathro":
		actualRobot = obj_battlebot_deathro
		break;
	case "grievous":
		actualRobot = obj_battlebot_grievous
		break;
	case "ali":
		actualRobot = obj_battlebot_ali
		break;
	case "bronze":
		actualRobot = obj_battlebot_bronze
		break;
	case "sephiroth":
		actualRobot = obj_battlebot_sephiroth
		break;
	case "spaceman":
		actualRobot = obj_battlebot_spaceman
		break;
	case "tim":
		actualRobot = obj_battlebot_tim
		break;
	default:
	actualRobot = noone
	break;
	
}
if(actualRobot != noone) {
	var robot;
	robot = instance_create_layer(-30,118,"Battle",actualRobot);
	robot.hp        = choose(robot.hp,robot.hp*qualityScore);
	robot.moveSpeed = choose(robot.moveSpeed,robot.moveSpeed*qualityScore);
	robot.damage    = choose(robot.damage,robot.damage*qualityScore);
	robot.atkspd    = choose(robot.atkspd,robot.atkspd*qualityScore);
}