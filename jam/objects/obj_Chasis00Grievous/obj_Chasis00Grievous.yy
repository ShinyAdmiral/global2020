{
    "id": "6b62b552-7390-42a7-9e53-0da1e9c16f3c",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_Chasis00Grievous",
    "eventList": [
        
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": [
        {
            "id": "88ad0e79-fc4c-4d58-ab8e-b488c926f17e",
            "modelName": "GMOverriddenProperty",
            "mvc": "1.0",
            "objectId": "3dcad30f-7ed7-4876-bdf8-709b7bcf0b49",
            "propertyId": "93b2db5f-d8ae-46d4-beca-3c2009144d3d",
            "value": "24"
        },
        {
            "id": "55697f28-6c83-47cc-bd86-d2073607e464",
            "modelName": "GMOverriddenProperty",
            "mvc": "1.0",
            "objectId": "3dcad30f-7ed7-4876-bdf8-709b7bcf0b49",
            "propertyId": "015f005a-09dd-4368-9268-d6d49ddd114c",
            "value": "48"
        },
        {
            "id": "6fa8217a-e60e-4889-a003-3f9be7041390",
            "modelName": "GMOverriddenProperty",
            "mvc": "1.0",
            "objectId": "3dcad30f-7ed7-4876-bdf8-709b7bcf0b49",
            "propertyId": "9b297a31-3d2d-405d-97f8-6ec819e2edb6",
            "value": "24"
        },
        {
            "id": "0556ea64-0edc-439a-9416-745d0118a125",
            "modelName": "GMOverriddenProperty",
            "mvc": "1.0",
            "objectId": "3dcad30f-7ed7-4876-bdf8-709b7bcf0b49",
            "propertyId": "babbcd50-3845-406c-ad71-876cf30993ac",
            "value": "4"
        },
        {
            "id": "f485fba4-9fff-420d-9cf8-e26eb31029ef",
            "modelName": "GMOverriddenProperty",
            "mvc": "1.0",
            "objectId": "3dcad30f-7ed7-4876-bdf8-709b7bcf0b49",
            "propertyId": "b058e2a0-3ecd-46d7-a5a4-b5eed3665631",
            "value": "48"
        },
        {
            "id": "a1cec242-d80f-4481-8ff2-844c81e7b09d",
            "modelName": "GMOverriddenProperty",
            "mvc": "1.0",
            "objectId": "3dcad30f-7ed7-4876-bdf8-709b7bcf0b49",
            "propertyId": "f80650f0-a2a6-4d63-8019-866686fbdaf5",
            "value": "26"
        },
        {
            "id": "6cf351bb-31b3-483b-b97a-9c9b7884bf4c",
            "modelName": "GMOverriddenProperty",
            "mvc": "1.0",
            "objectId": "3dcad30f-7ed7-4876-bdf8-709b7bcf0b49",
            "propertyId": "e8ec1153-9a24-410d-ad43-c8e2c75a090c",
            "value": "0"
        },
        {
            "id": "5c19fa94-7756-4ef7-8e0c-f6f26b1c981d",
            "modelName": "GMOverriddenProperty",
            "mvc": "1.0",
            "objectId": "3dcad30f-7ed7-4876-bdf8-709b7bcf0b49",
            "propertyId": "5a671739-5d5a-489c-8f4b-bf8e4d0762d0",
            "value": "0"
        },
        {
            "id": "63eeddff-003e-441a-b69f-ae1749dea3ac",
            "modelName": "GMOverriddenProperty",
            "mvc": "1.0",
            "objectId": "3dcad30f-7ed7-4876-bdf8-709b7bcf0b49",
            "propertyId": "f1e3d7b5-b812-4770-8972-336937f8d5a4",
            "value": "9"
        },
        {
            "id": "741129bd-511b-4ff7-b666-f3dd219260dd",
            "modelName": "GMOverriddenProperty",
            "mvc": "1.0",
            "objectId": "3dcad30f-7ed7-4876-bdf8-709b7bcf0b49",
            "propertyId": "5bf989de-3bc9-48a3-ad93-30379707fdde",
            "value": "52"
        },
        {
            "id": "abdc5c33-929d-478c-b60d-4700bfa2f574",
            "modelName": "GMOverriddenProperty",
            "mvc": "1.0",
            "objectId": "3dcad30f-7ed7-4876-bdf8-709b7bcf0b49",
            "propertyId": "b97fbe8d-2226-4454-b330-0db58b8bdb4e",
            "value": "9"
        },
        {
            "id": "d99a8531-e0fe-4a18-8b87-a8accfb38c7e",
            "modelName": "GMOverriddenProperty",
            "mvc": "1.0",
            "objectId": "3dcad30f-7ed7-4876-bdf8-709b7bcf0b49",
            "propertyId": "b22e7616-619d-4573-918a-3d8302b6088d",
            "value": "22"
        },
        {
            "id": "fcaed2d5-6c9d-4168-b55f-60534f4226b3",
            "modelName": "GMOverriddenProperty",
            "mvc": "1.0",
            "objectId": "3dcad30f-7ed7-4876-bdf8-709b7bcf0b49",
            "propertyId": "7a363fbd-eff1-4533-931f-52453782cb4d",
            "value": "48"
        },
        {
            "id": "63f58c46-14b2-4fa7-84e3-0921f0dde833",
            "modelName": "GMOverriddenProperty",
            "mvc": "1.0",
            "objectId": "3dcad30f-7ed7-4876-bdf8-709b7bcf0b49",
            "propertyId": "cbc008da-7b6c-4413-ae23-faa24a3ceed3",
            "value": "30"
        }
    ],
    "parentObjectId": "3dcad30f-7ed7-4876-bdf8-709b7bcf0b49",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "59ea9b1b-df96-4d25-baf3-3b28162290f3",
    "visible": true
}