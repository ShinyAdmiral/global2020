draw_self();

if(!instance_exists(obj_loss))
{
	if (flash > 0)
	{
		flash -=0.05;
		shader_set(shader0);
		shadeAlpha = shader_get_uniform(shader0, "alpha");
		shader_set_uniform_f(shadeAlpha, flash);
		draw_self();
		shader_reset();
	}
}