{
    "id": "96c9a786-a055-407f-9871-c7880eb77e99",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_UILeft",
    "eventList": [
        {
            "id": "2fae2ad4-f6f1-437f-906b-191a4b9e8daf",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "96c9a786-a055-407f-9871-c7880eb77e99"
        },
        {
            "id": "60d9d2f1-cc34-4e5a-a7ee-ecd556d9cae3",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "96c9a786-a055-407f-9871-c7880eb77e99"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "9d9a869f-bbb1-443b-960c-f3e1f79bbca4",
    "visible": true
}