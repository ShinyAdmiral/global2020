{
    "id": "dc04e90a-fad8-44bc-ad15-4e51eebdb444",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_battleEnemyInvincible",
    "eventList": [
        {
            "id": "910c2501-9e4d-4a8b-855a-1d4b78e069e8",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "dc04e90a-fad8-44bc-ad15-4e51eebdb444"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "ab0ddc07-19b6-4910-aecf-70b4be6d004e",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "252ae9d0-db8f-4552-9df6-9e02d15d9cd9",
    "visible": true
}