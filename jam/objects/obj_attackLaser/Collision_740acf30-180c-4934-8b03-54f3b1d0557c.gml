if(other!=blacklist){
	repeat(irandom(6)+4) instance_create_depth(other.x-8,other.y-16,-room_height,obj_particleLaser)
	if(instance_exists(target)) {
		target.hp -= damage;
		if(target.hp<=0) instance_destroy(target);
		instance_destroy();
		with(obj_battlefieldTest) event_user(0);
	}
}