{
    "id": "13b28ffe-b51e-4c6b-82c3-2034fa3164ac",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_attackLaser",
    "eventList": [
        {
            "id": "4e55f98b-3b96-4d0a-9f16-61964f46f47a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "13b28ffe-b51e-4c6b-82c3-2034fa3164ac"
        },
        {
            "id": "4810e36e-b7ff-437c-9b70-cb9a39e4129f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "13b28ffe-b51e-4c6b-82c3-2034fa3164ac"
        },
        {
            "id": "740acf30-180c-4934-8b03-54f3b1d0557c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "ab0ddc07-19b6-4910-aecf-70b4be6d004e",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "13b28ffe-b51e-4c6b-82c3-2034fa3164ac"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": [
        {
            "id": "d29562a8-f80e-4f41-a5dc-9a2a4fafc4d4",
            "modelName": "GMOverriddenProperty",
            "mvc": "1.0",
            "objectId": "933839b8-24f5-4e0d-a2d1-a01d1293106c",
            "propertyId": "c10c8a12-58c0-4e31-aceb-6f08dc8be41f",
            "value": "2"
        },
        {
            "id": "469f3a94-a74f-4ede-b667-45852144dca8",
            "modelName": "GMOverriddenProperty",
            "mvc": "1.0",
            "objectId": "933839b8-24f5-4e0d-a2d1-a01d1293106c",
            "propertyId": "22f69857-8df0-44ef-8349-9f99c3f7622e",
            "value": "True"
        },
        {
            "id": "b29032ad-868f-41ca-b97b-77c36120d865",
            "modelName": "GMOverriddenProperty",
            "mvc": "1.0",
            "objectId": "2bf3fbb7-0b28-4f01-aa34-5810ce2e0d1e",
            "propertyId": "45378f07-4a1b-4ba8-b851-f4b1e87fa8e6",
            "value": "0"
        }
    ],
    "parentObjectId": "2bf3fbb7-0b28-4f01-aa34-5810ce2e0d1e",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "f3e725e3-302b-4e0b-9b61-df306852b205",
    "visible": true
}