if(instance_exists(target)){
	target.x += lengthdir_x(knockback,direction)
	target.y += lengthdir_y(knockback,direction)
	
	knockback = knockback/2;

	if(image_index > 5) {
		target.hp -= damage;
		if(target.hp<=0) instance_destroy(target);
		with(obj_battlefieldTest) event_user(0);
		instance_destroy();
	}
} 