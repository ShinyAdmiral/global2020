{
    "id": "3c2d9ca2-e9e7-44bd-92f5-e3d6a2ba2ee3",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_attackLightning",
    "eventList": [
        {
            "id": "51409ff6-cd14-43a5-8887-698b42ef3076",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "3c2d9ca2-e9e7-44bd-92f5-e3d6a2ba2ee3"
        },
        {
            "id": "84c5ef17-e8bb-4f34-8e08-99df9aa8fc60",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "3c2d9ca2-e9e7-44bd-92f5-e3d6a2ba2ee3"
        },
        {
            "id": "26a3c425-c0f1-4fd5-bfb5-595d4673f042",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "3c2d9ca2-e9e7-44bd-92f5-e3d6a2ba2ee3"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "2bf3fbb7-0b28-4f01-aa34-5810ce2e0d1e",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "33ef0d51-96ef-445c-86fc-6a1eee7a6ecb",
    "visible": true
}