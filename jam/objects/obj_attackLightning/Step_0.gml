if(instance_exists(target)){
	
	direction = point_direction(x,y,target.x,target.y-16);

	if(image_index > 5) {
		target.hp -= damage;
		if(target.hp<=0) instance_destroy(target);
		with(obj_battlefieldTest) event_user(0);
		instance_destroy();
	}
} else {
	if(image_index > 5) instance_destroy();
}