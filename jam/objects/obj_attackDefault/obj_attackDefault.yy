{
    "id": "2bf3fbb7-0b28-4f01-aa34-5810ce2e0d1e",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_attackDefault",
    "eventList": [
        {
            "id": "c9223524-358e-40da-906a-f0c70591deb5",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "2bf3fbb7-0b28-4f01-aa34-5810ce2e0d1e"
        },
        {
            "id": "2581df1d-9329-41c4-a6e5-46dcf6f5fc54",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "2bf3fbb7-0b28-4f01-aa34-5810ce2e0d1e"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": [
        {
            "id": "9d644146-4875-4308-8bb6-c322de17dc2c",
            "modelName": "GMOverriddenProperty",
            "mvc": "1.0",
            "objectId": "933839b8-24f5-4e0d-a2d1-a01d1293106c",
            "propertyId": "c10c8a12-58c0-4e31-aceb-6f08dc8be41f",
            "value": "5"
        }
    ],
    "parentObjectId": "933839b8-24f5-4e0d-a2d1-a01d1293106c",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": [
        {
            "id": "b1fdf3d3-b56b-465d-995e-c1adbd4c8200",
            "modelName": "GMObjectProperty",
            "mvc": "1.0",
            "listItems": null,
            "multiselect": false,
            "rangeEnabled": false,
            "rangeMax": 10,
            "rangeMin": 0,
            "resourceFilter": 1023,
            "value": "2",
            "varName": "damage",
            "varType": 0
        },
        {
            "id": "45378f07-4a1b-4ba8-b851-f4b1e87fa8e6",
            "modelName": "GMObjectProperty",
            "mvc": "1.0",
            "listItems": null,
            "multiselect": false,
            "rangeEnabled": false,
            "rangeMax": 10,
            "rangeMin": 0,
            "resourceFilter": 1023,
            "value": "15",
            "varName": "spawnOffset",
            "varType": 0
        }
    ],
    "solid": false,
    "spriteId": "7bee3777-cc65-4055-bba4-86957d4f2ecf",
    "visible": true
}