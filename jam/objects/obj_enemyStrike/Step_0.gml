if(instance_exists(target)){
	target.x += lengthdir_x(knockback,direction)
	target.y += lengthdir_y(knockback,direction)
	if(kbDecay) knockback = knockback/2;
}
if(image_index > 5) { 
	if(instance_exists(target)){
		target.hp -= damage
		if(target.hp<=0){
			instance_destroy(target);
		}
	}
	instance_destroy();
}