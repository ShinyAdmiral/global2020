{
    "id": "933839b8-24f5-4e0d-a2d1-a01d1293106c",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_enemyStrike",
    "eventList": [
        {
            "id": "f9fd3348-ef6f-4b54-9715-10f712092e1b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "933839b8-24f5-4e0d-a2d1-a01d1293106c"
        },
        {
            "id": "1f0e6061-3dce-4186-b909-46787b404753",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "933839b8-24f5-4e0d-a2d1-a01d1293106c"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": [
        {
            "id": "22f69857-8df0-44ef-8349-9f99c3f7622e",
            "modelName": "GMObjectProperty",
            "mvc": "1.0",
            "listItems": null,
            "multiselect": false,
            "rangeEnabled": false,
            "rangeMax": 10,
            "rangeMin": 0,
            "resourceFilter": 1023,
            "value": "True",
            "varName": "kbDecay",
            "varType": 3
        },
        {
            "id": "c10c8a12-58c0-4e31-aceb-6f08dc8be41f",
            "modelName": "GMObjectProperty",
            "mvc": "1.0",
            "listItems": null,
            "multiselect": false,
            "rangeEnabled": false,
            "rangeMax": 10,
            "rangeMin": 0,
            "resourceFilter": 1023,
            "value": "3",
            "varName": "knockback",
            "varType": 0
        }
    ],
    "solid": false,
    "spriteId": "c7aafd54-bb1a-4685-9e7a-22cb3b029b49",
    "visible": true
}