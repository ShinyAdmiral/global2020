{
    "id": "1c3fc53e-f08f-4016-8efc-9efc187250cb",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_RightLegAli",
    "eventList": [
        
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "8b1258c7-0f93-47cc-b0c4-3c5b85b89e57",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "085713ca-6042-463b-8853-89bcc7893e12",
    "visible": true
}