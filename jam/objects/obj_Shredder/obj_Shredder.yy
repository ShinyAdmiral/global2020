{
    "id": "1a9137e9-dad3-4821-b35a-67ec6488d49f",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_Shredder",
    "eventList": [
        {
            "id": "31500c84-45ea-4eea-952e-fd1870b1624f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "8b1258c7-0f93-47cc-b0c4-3c5b85b89e57",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "1a9137e9-dad3-4821-b35a-67ec6488d49f"
        },
        {
            "id": "188d5b63-1936-498b-9391-ef0fd6945b61",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "63a6f17f-ffea-4ed3-8a9d-e8b81ff64db8",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "1a9137e9-dad3-4821-b35a-67ec6488d49f"
        },
        {
            "id": "e8533b8e-b8c1-48b5-8501-b96d5ec79b38",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "3dcad30f-7ed7-4876-bdf8-709b7bcf0b49",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "1a9137e9-dad3-4821-b35a-67ec6488d49f"
        },
        {
            "id": "3bac51b7-9699-4531-9b85-e73902b12afc",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "a283155c-af76-4923-8c65-6b8add35ca77",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "1a9137e9-dad3-4821-b35a-67ec6488d49f"
        },
        {
            "id": "1f906f48-2ab7-4e98-a8d2-3d643fc4a70b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "46125afd-37a4-4ff0-9afc-d5df790f5ace",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "1a9137e9-dad3-4821-b35a-67ec6488d49f"
        },
        {
            "id": "c30f24d2-8145-4936-b947-05a380138e91",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "4c4dda49-bd52-4227-9c94-c9a3826f88c9",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "1a9137e9-dad3-4821-b35a-67ec6488d49f"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "80b59b4e-ed5e-4856-ba8c-11fa4b0b00df",
    "visible": true
}