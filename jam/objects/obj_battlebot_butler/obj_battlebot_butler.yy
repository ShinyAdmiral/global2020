{
    "id": "c622cbe0-7fd6-439d-8baf-52643f82756d",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_battlebot_butler",
    "eventList": [
        {
            "id": "ed0b6f41-bb2f-4b94-8d93-aac43535d5a0",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "c622cbe0-7fd6-439d-8baf-52643f82756d"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": [
        {
            "id": "4e81834b-3b60-47cc-ada6-b292ca001073",
            "modelName": "GMOverriddenProperty",
            "mvc": "1.0",
            "objectId": "5e45bd04-119a-4fa1-a822-c0ff2342673a",
            "propertyId": "64ae687f-d01c-4d9a-b0f5-893a7a9726cc",
            "value": "obj_attackSlash"
        },
        {
            "id": "49b53842-2da6-4632-aeb9-63849683f2f3",
            "modelName": "GMOverriddenProperty",
            "mvc": "1.0",
            "objectId": "5e45bd04-119a-4fa1-a822-c0ff2342673a",
            "propertyId": "57ca94ab-dfc6-41c9-a24b-ca24d9735148",
            "value": "90"
        },
        {
            "id": "d22b05c6-a99b-4938-93d9-c0388b178649",
            "modelName": "GMOverriddenProperty",
            "mvc": "1.0",
            "objectId": "5e45bd04-119a-4fa1-a822-c0ff2342673a",
            "propertyId": "632b9632-a0ab-4a56-b0c2-f2d26b852e1c",
            "value": "2"
        },
        {
            "id": "3ce17b7a-35f6-40af-9e3e-9da7a8eb5c54",
            "modelName": "GMOverriddenProperty",
            "mvc": "1.0",
            "objectId": "5e45bd04-119a-4fa1-a822-c0ff2342673a",
            "propertyId": "b2a569b3-3828-44bb-a728-11de0b6d8628",
            "value": "1"
        },
        {
            "id": "ba28a8f0-6da5-4b6e-b95e-234584e1e267",
            "modelName": "GMOverriddenProperty",
            "mvc": "1.0",
            "objectId": "5e45bd04-119a-4fa1-a822-c0ff2342673a",
            "propertyId": "bb048f5b-4d73-4dc6-9397-9b155e26fcdf",
            "value": "100"
        },
        {
            "id": "7c5e3958-5388-412f-a1ca-cf38bdc34aaa",
            "modelName": "GMOverriddenProperty",
            "mvc": "1.0",
            "objectId": "5e45bd04-119a-4fa1-a822-c0ff2342673a",
            "propertyId": "33cdc2fd-face-473f-9d03-762b69bcc1e2",
            "value": "1"
        }
    ],
    "parentObjectId": "c7a36923-acea-4a9a-a97e-58286e3a6c79",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "1c0e8d77-8544-497b-af6b-6b91d5f29275",
    "visible": true
}