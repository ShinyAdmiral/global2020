if(!pause) {

	sinCount += moveSpeed*20
	
	if(sinCount = 360) {
		if(distance_to_object(instance_nearest(x,y,obj_battlefieldTest))<10) {
			pause = true;
		} else {
			sinCount = 0;	
		}
	}

	var sinOut;
	sinOut = dsin(sinCount)

	image_angle = sinOut*wobble
} else {
	if(atkphase = 0){
		image_xscale -= 0.01;
		image_yscale += 0.01;
		
		if(image_yscale >= 1.1) atkphase++
		
	} else if (atkphase = 1) {
		
		image_xscale += 0.05;
		image_yscale -= 0.05;
		
		
		if(image_xscale = 1.5) {
			var attack, victim;
			
			victim = instance_nearest(x,y,obj_battlefieldTest);
			if(instance_exists(victim)){
				attack = instance_create_depth(x,y-15,-room_height,attackType);
			
				attack.target = victim;
				attack.damage = damage;
			
				attack.direction = point_direction(x,y,victim.x,victim.y);
				attack.image_angle = attack.direction;
		
				attack.x += lengthdir_x(15,attack.direction);
				attack.y += lengthdir_y(15,attack.direction);
			}
			atkphase ++
		}
	} else {
		
		image_xscale -= 0.02;
		image_yscale += 0.02;
		
		if(image_xscale <= 1) {
			atkphase = 0
			pause = false;
			sinCount = 0;
		}
	}
}

y = clamp(y,80,183)