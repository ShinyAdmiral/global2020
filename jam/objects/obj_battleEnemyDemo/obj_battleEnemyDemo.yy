{
    "id": "b66ea329-57bc-461a-9ea2-6ce526f6ab8f",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_battleEnemyDemo",
    "eventList": [
        {
            "id": "5b1bd842-a328-4796-8d28-ed0d79513cda",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "b66ea329-57bc-461a-9ea2-6ce526f6ab8f"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": [
        {
            "id": "dc009f85-90cb-4113-b029-a7b77e640a33",
            "modelName": "GMOverriddenProperty",
            "mvc": "1.0",
            "objectId": "ab0ddc07-19b6-4910-aecf-70b4be6d004e",
            "propertyId": "8321e759-916a-4133-963a-c007eb4a82ff",
            "value": "1"
        },
        {
            "id": "922784d3-4630-4e06-b620-764a50b63947",
            "modelName": "GMOverriddenProperty",
            "mvc": "1.0",
            "objectId": "ab0ddc07-19b6-4910-aecf-70b4be6d004e",
            "propertyId": "79b81f97-cf32-4203-ae09-a7fe063fe016",
            "value": "1"
        },
        {
            "id": "5fafd6f9-cdba-454d-8288-f2f9bb20afb0",
            "modelName": "GMOverriddenProperty",
            "mvc": "1.0",
            "objectId": "ab0ddc07-19b6-4910-aecf-70b4be6d004e",
            "propertyId": "53f89a47-09fa-4ea1-8532-3107149acb2b",
            "value": "obj_enemyJab"
        },
        {
            "id": "d1c4c1f3-2763-457d-bbe9-9a71ca347d1b",
            "modelName": "GMOverriddenProperty",
            "mvc": "1.0",
            "objectId": "ab0ddc07-19b6-4910-aecf-70b4be6d004e",
            "propertyId": "30da5f80-42af-42fa-8ef5-29063f517884",
            "value": "0.3"
        }
    ],
    "parentObjectId": "ab0ddc07-19b6-4910-aecf-70b4be6d004e",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "2437365e-6c32-4079-a474-4319cf354fda",
    "visible": true
}