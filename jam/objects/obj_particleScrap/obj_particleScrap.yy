{
    "id": "520bd639-2d4e-4468-b975-1c56bf3406ba",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_particleScrap",
    "eventList": [
        
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "87a69f5d-229b-4c90-a770-9c4739db115a",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "3f518875-de1d-4e47-bad2-9ea59b83649f",
    "visible": true
}