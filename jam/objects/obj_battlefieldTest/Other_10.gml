/// @description Aggro
if(instance_exists(obj_battlefieldTestEnemy)){
	target = instance_nearest(x,y,obj_battlefieldTestEnemy);
	direction = point_direction(x,y,target.x,target.y)
} else {
	idle = true;
}