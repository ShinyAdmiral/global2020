x = clamp(x,-30,162);

if(x<16) {
	x++
}

if (atkcount > 0){
	atkcount-=1;
}
	
if(idle){
	if (image_angle != 0){
		sinCount += moveSpeed*20
		if(sinCount >= 360) sinCount = 0

		var sinOut;
		sinOut = dsin(sinCount)

		y+=dsin(sinCount*2)*bobbing

		image_angle = sinOut*wobble
	}
	if(instance_exists(obj_battlefieldTestEnemy)) idle = false;
	
} else {
	if(instance_exists(target) && distance_to_object(target)>range){
	
		direction = point_direction(x,y,target.x,target.y)
		x+=lengthdir_x(moveSpeed,direction)
		y+=lengthdir_y(moveSpeed,direction)
		depth = -y
	
		sinCount += moveSpeed*20
		if(sinCount >= 360) sinCount = 0

		var sinOut;
		sinOut = dsin(sinCount)

		y+=dsin(sinCount*2)*bobbing

		image_angle = sinOut*wobble
	
	} else if (image_angle != 0){
		sinCount += moveSpeed*20
		if(sinCount >= 360) sinCount = 0

		var sinOut;
		sinOut = dsin(sinCount)

		y+=dsin(sinCount*2)*bobbing

		image_angle = sinOut*wobble
	} else {
		if(atkcount <= 0){
			var attack, victim;
		
			victim = instance_nearest(x,y,obj_battlefieldTestEnemy)
			if(instance_exists(victim)){
				attack = instance_create_depth(x,y-15,-room_height,projectile);
			
				attack.damage = damage
				attack.target = victim;
			
				attack.direction = point_direction(x,y,victim.x,victim.y);
				attack.image_angle = attack.direction
		
				atkcount = atkspd
			}
		
		}
	}
}