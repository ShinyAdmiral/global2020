{
    "id": "efce4ecd-758b-4e10-b892-f8ac44ef96fc",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_background",
    "eventList": [
        {
            "id": "b8f3e032-b583-4399-96e4-6495742a2795",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "efce4ecd-758b-4e10-b892-f8ac44ef96fc"
        },
        {
            "id": "1268f2e8-76ec-4742-81d6-f3f83d867f64",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "efce4ecd-758b-4e10-b892-f8ac44ef96fc"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "82c33977-7f37-441b-ae6d-bd8fbef96c4d",
    "visible": true
}