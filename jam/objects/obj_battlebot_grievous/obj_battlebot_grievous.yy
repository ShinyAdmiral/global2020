{
    "id": "148c98a1-7ead-4f33-9d7e-40f995218295",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_battlebot_grievous",
    "eventList": [
        
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": [
        {
            "id": "9abb465b-4f8f-4bf4-aefd-38f390b4e5c7",
            "modelName": "GMOverriddenProperty",
            "mvc": "1.0",
            "objectId": "5e45bd04-119a-4fa1-a822-c0ff2342673a",
            "propertyId": "64ae687f-d01c-4d9a-b0f5-893a7a9726cc",
            "value": "obj_attackQuad"
        },
        {
            "id": "7aadf3bf-8bd7-4616-a0f8-935a7e4aa1c6",
            "modelName": "GMOverriddenProperty",
            "mvc": "1.0",
            "objectId": "5e45bd04-119a-4fa1-a822-c0ff2342673a",
            "propertyId": "bb048f5b-4d73-4dc6-9397-9b155e26fcdf",
            "value": "40"
        },
        {
            "id": "28a7eb6c-9fb8-426b-8919-8f4760be8b93",
            "modelName": "GMOverriddenProperty",
            "mvc": "1.0",
            "objectId": "5e45bd04-119a-4fa1-a822-c0ff2342673a",
            "propertyId": "57ca94ab-dfc6-41c9-a24b-ca24d9735148",
            "value": "200"
        },
        {
            "id": "b886465e-7c12-4bec-b95e-41a986e04d96",
            "modelName": "GMOverriddenProperty",
            "mvc": "1.0",
            "objectId": "5e45bd04-119a-4fa1-a822-c0ff2342673a",
            "propertyId": "632b9632-a0ab-4a56-b0c2-f2d26b852e1c",
            "value": "2"
        },
        {
            "id": "c0e1c3cd-e69c-42ca-a157-dbcbd06b5f12",
            "modelName": "GMOverriddenProperty",
            "mvc": "1.0",
            "objectId": "5e45bd04-119a-4fa1-a822-c0ff2342673a",
            "propertyId": "b2a569b3-3828-44bb-a728-11de0b6d8628",
            "value": "0.35"
        }
    ],
    "parentObjectId": "5e45bd04-119a-4fa1-a822-c0ff2342673a",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "e34e63ef-947f-4c42-bf13-2164b3c7f6e0",
    "visible": true
}