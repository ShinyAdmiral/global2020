if (able)
{
	instance_destroy(obj_ChasisParent);
	instance_destroy(obj_BodyParent); 
	instance_destroy(obj_ScoreCalc);
	if(!sound)
	{
		audio_play_sound(DEMOLISH_Metal_Quick_stereo,10,false);
		sound = true;
	}
}