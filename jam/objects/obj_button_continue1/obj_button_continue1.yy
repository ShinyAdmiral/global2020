{
    "id": "96445661-0513-48dc-9950-842945c8fa22",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_button_continue1",
    "eventList": [
        {
            "id": "429821bf-fb8a-45ea-895c-0cb9af2f925b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 10,
            "eventtype": 6,
            "m_owner": "96445661-0513-48dc-9950-842945c8fa22"
        },
        {
            "id": "d5e9649b-8153-4019-a24b-036a69dff616",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 11,
            "eventtype": 6,
            "m_owner": "96445661-0513-48dc-9950-842945c8fa22"
        },
        {
            "id": "38e29d1e-7ffa-4e69-bd3e-dc10ff45542f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 6,
            "m_owner": "96445661-0513-48dc-9950-842945c8fa22"
        },
        {
            "id": "3325b9e6-9fbe-4cd8-80d8-006011cc729d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "96445661-0513-48dc-9950-842945c8fa22"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "e387651e-191a-41ba-bda4-ba8b1bc409f8",
    "visible": true
}