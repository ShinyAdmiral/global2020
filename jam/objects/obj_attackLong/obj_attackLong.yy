{
    "id": "68bbb1c5-b577-4150-9da5-24bf5e387b25",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_attackLong",
    "eventList": [
        
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": [
        {
            "id": "26cbb224-71e0-478a-adbc-40ffb2aef5cc",
            "modelName": "GMOverriddenProperty",
            "mvc": "1.0",
            "objectId": "933839b8-24f5-4e0d-a2d1-a01d1293106c",
            "propertyId": "c10c8a12-58c0-4e31-aceb-6f08dc8be41f",
            "value": "5"
        }
    ],
    "parentObjectId": "2bf3fbb7-0b28-4f01-aa34-5810ce2e0d1e",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": [
        {
            "id": "eaf6b6f4-c115-4be6-822a-291649134007",
            "modelName": "GMObjectProperty",
            "mvc": "1.0",
            "listItems": null,
            "multiselect": false,
            "rangeEnabled": false,
            "rangeMax": 10,
            "rangeMin": 0,
            "resourceFilter": 1023,
            "value": "2",
            "varName": "damage",
            "varType": 0
        },
        {
            "id": "c7068e1c-1b70-4db6-90c5-7257423d5c79",
            "modelName": "GMObjectProperty",
            "mvc": "1.0",
            "listItems": null,
            "multiselect": false,
            "rangeEnabled": false,
            "rangeMax": 10,
            "rangeMin": 0,
            "resourceFilter": 1023,
            "value": "15",
            "varName": "spawnOffset",
            "varType": 0
        }
    ],
    "solid": false,
    "spriteId": "cbfb92cb-849a-4c94-83d1-37db7e618842",
    "visible": true
}