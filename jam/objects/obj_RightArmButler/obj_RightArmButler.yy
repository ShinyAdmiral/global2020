{
    "id": "18eab5d0-7151-47ca-acc3-c5a0b02af7e9",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_RightArmButler",
    "eventList": [
        
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "8b1258c7-0f93-47cc-b0c4-3c5b85b89e57",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "c7c21cd3-ab59-4f54-b5a9-cad8cdc15515",
    "visible": true
}