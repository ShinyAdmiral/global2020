{
    "id": "2eabe443-7dde-4c82-a6fb-be9d52de4171",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_Chasis00Spaceman",
    "eventList": [
        
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": [
        {
            "id": "43de0a2c-4dc3-402d-9dbb-703d24ec5402",
            "modelName": "GMOverriddenProperty",
            "mvc": "1.0",
            "objectId": "3dcad30f-7ed7-4876-bdf8-709b7bcf0b49",
            "propertyId": "f80650f0-a2a6-4d63-8019-866686fbdaf5",
            "value": "30"
        },
        {
            "id": "5f639a98-7cef-428f-abc7-d3d4a9da5098",
            "modelName": "GMOverriddenProperty",
            "mvc": "1.0",
            "objectId": "3dcad30f-7ed7-4876-bdf8-709b7bcf0b49",
            "propertyId": "e8ec1153-9a24-410d-ad43-c8e2c75a090c",
            "value": "0"
        },
        {
            "id": "a400cd77-7960-4fa3-8539-5ad425755f8c",
            "modelName": "GMOverriddenProperty",
            "mvc": "1.0",
            "objectId": "3dcad30f-7ed7-4876-bdf8-709b7bcf0b49",
            "propertyId": "5a671739-5d5a-489c-8f4b-bf8e4d0762d0",
            "value": "2"
        },
        {
            "id": "55ef6bfa-8072-43d1-aaa6-eac2e712698e",
            "modelName": "GMOverriddenProperty",
            "mvc": "1.0",
            "objectId": "3dcad30f-7ed7-4876-bdf8-709b7bcf0b49",
            "propertyId": "f1e3d7b5-b812-4770-8972-336937f8d5a4",
            "value": "11"
        },
        {
            "id": "8cc972df-a6ee-44e4-bc3a-b51ecba62d5e",
            "modelName": "GMOverriddenProperty",
            "mvc": "1.0",
            "objectId": "3dcad30f-7ed7-4876-bdf8-709b7bcf0b49",
            "propertyId": "5bf989de-3bc9-48a3-ad93-30379707fdde",
            "value": "58"
        },
        {
            "id": "95f69057-e79e-413d-bf87-14d73591bab0",
            "modelName": "GMOverriddenProperty",
            "mvc": "1.0",
            "objectId": "3dcad30f-7ed7-4876-bdf8-709b7bcf0b49",
            "propertyId": "b97fbe8d-2226-4454-b330-0db58b8bdb4e",
            "value": "11"
        },
        {
            "id": "bde2caf3-2512-4ec2-9374-742febbecbcc",
            "modelName": "GMOverriddenProperty",
            "mvc": "1.0",
            "objectId": "3dcad30f-7ed7-4876-bdf8-709b7bcf0b49",
            "propertyId": "b22e7616-619d-4573-918a-3d8302b6088d",
            "value": "24"
        },
        {
            "id": "786b9d67-3401-42e3-849d-d41bb4c42e96",
            "modelName": "GMOverriddenProperty",
            "mvc": "1.0",
            "objectId": "3dcad30f-7ed7-4876-bdf8-709b7bcf0b49",
            "propertyId": "7a363fbd-eff1-4533-931f-52453782cb4d",
            "value": "33"
        },
        {
            "id": "d15f150c-4ad0-4174-908d-3cf8314d28c9",
            "modelName": "GMOverriddenProperty",
            "mvc": "1.0",
            "objectId": "3dcad30f-7ed7-4876-bdf8-709b7bcf0b49",
            "propertyId": "cbc008da-7b6c-4413-ae23-faa24a3ceed3",
            "value": "36"
        },
        {
            "id": "3be13426-5333-4fc9-bd71-8a64885326d6",
            "modelName": "GMOverriddenProperty",
            "mvc": "1.0",
            "objectId": "3dcad30f-7ed7-4876-bdf8-709b7bcf0b49",
            "propertyId": "b058e2a0-3ecd-46d7-a5a4-b5eed3665631",
            "value": "33"
        }
    ],
    "parentObjectId": "3dcad30f-7ed7-4876-bdf8-709b7bcf0b49",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "75ee01fb-a43c-45d1-8114-fd1c8d8a7903",
    "visible": true
}