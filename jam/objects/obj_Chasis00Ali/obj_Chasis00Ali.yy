{
    "id": "ff7a7097-a3fb-4b44-ae7b-a172afce20e6",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_Chasis00Ali",
    "eventList": [
        
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": [
        {
            "id": "4cf7152b-60cc-43ff-9c3f-db5a2be6b8d5",
            "modelName": "GMOverriddenProperty",
            "mvc": "1.0",
            "objectId": "3dcad30f-7ed7-4876-bdf8-709b7bcf0b49",
            "propertyId": "f80650f0-a2a6-4d63-8019-866686fbdaf5",
            "value": "37"
        },
        {
            "id": "2e9a3ffb-d628-4262-a293-18fd96f385fc",
            "modelName": "GMOverriddenProperty",
            "mvc": "1.0",
            "objectId": "3dcad30f-7ed7-4876-bdf8-709b7bcf0b49",
            "propertyId": "e8ec1153-9a24-410d-ad43-c8e2c75a090c",
            "value": "6"
        },
        {
            "id": "54ad475c-b946-4409-8ff8-95954ecd3502",
            "modelName": "GMOverriddenProperty",
            "mvc": "1.0",
            "objectId": "3dcad30f-7ed7-4876-bdf8-709b7bcf0b49",
            "propertyId": "5a671739-5d5a-489c-8f4b-bf8e4d0762d0",
            "value": "2"
        },
        {
            "id": "3bbd4e44-303b-4ee8-aa54-e84cf4087863",
            "modelName": "GMOverriddenProperty",
            "mvc": "1.0",
            "objectId": "3dcad30f-7ed7-4876-bdf8-709b7bcf0b49",
            "propertyId": "5bf989de-3bc9-48a3-ad93-30379707fdde",
            "value": "72"
        },
        {
            "id": "a3923e6f-76ae-407a-a13f-408e93f7fe43",
            "modelName": "GMOverriddenProperty",
            "mvc": "1.0",
            "objectId": "3dcad30f-7ed7-4876-bdf8-709b7bcf0b49",
            "propertyId": "f1e3d7b5-b812-4770-8972-336937f8d5a4",
            "value": "10"
        },
        {
            "id": "8ff12a56-f785-4e48-b7f6-893bc0afbdce",
            "modelName": "GMOverriddenProperty",
            "mvc": "1.0",
            "objectId": "3dcad30f-7ed7-4876-bdf8-709b7bcf0b49",
            "propertyId": "b97fbe8d-2226-4454-b330-0db58b8bdb4e",
            "value": "10"
        },
        {
            "id": "3c6618d0-7451-4afb-868b-127101c9c285",
            "modelName": "GMOverriddenProperty",
            "mvc": "1.0",
            "objectId": "3dcad30f-7ed7-4876-bdf8-709b7bcf0b49",
            "propertyId": "b22e7616-619d-4573-918a-3d8302b6088d",
            "value": "28"
        },
        {
            "id": "d007cc41-593b-4c49-92b2-1e8d83bd0d2d",
            "modelName": "GMOverriddenProperty",
            "mvc": "1.0",
            "objectId": "3dcad30f-7ed7-4876-bdf8-709b7bcf0b49",
            "propertyId": "cbc008da-7b6c-4413-ae23-faa24a3ceed3",
            "value": "46"
        },
        {
            "id": "65de2563-7e36-4ea3-a1e7-7ff01eb80f55",
            "modelName": "GMOverriddenProperty",
            "mvc": "1.0",
            "objectId": "3dcad30f-7ed7-4876-bdf8-709b7bcf0b49",
            "propertyId": "7a363fbd-eff1-4533-931f-52453782cb4d",
            "value": "49"
        },
        {
            "id": "2cffe47f-0f99-4ea5-8e4a-bc3ac21197c5",
            "modelName": "GMOverriddenProperty",
            "mvc": "1.0",
            "objectId": "3dcad30f-7ed7-4876-bdf8-709b7bcf0b49",
            "propertyId": "b058e2a0-3ecd-46d7-a5a4-b5eed3665631",
            "value": "49"
        }
    ],
    "parentObjectId": "3dcad30f-7ed7-4876-bdf8-709b7bcf0b49",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "9d55e109-9275-46e6-979e-349c79682995",
    "visible": true
}