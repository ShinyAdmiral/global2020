{
    "id": "1b31bbdb-76c5-4ee3-9df3-917bb2118897",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_LeftLeg0Bronze",
    "eventList": [
        
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "8b1258c7-0f93-47cc-b0c4-3c5b85b89e57",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "40e8d21a-7d6d-45b8-99ed-34978b8325cd",
    "visible": true
}