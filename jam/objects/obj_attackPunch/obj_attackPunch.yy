{
    "id": "325e7bfa-1963-4bd9-bd9b-0611a2c52119",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_attackPunch",
    "eventList": [
        {
            "id": "20e87d7a-0028-4617-92f4-4330db165f9c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "325e7bfa-1963-4bd9-bd9b-0611a2c52119"
        },
        {
            "id": "2876d4be-b25a-40c8-8999-2725a82f0741",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "325e7bfa-1963-4bd9-bd9b-0611a2c52119"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": [
        {
            "id": "44d557c6-3489-43a4-9a25-8dfd251fb106",
            "modelName": "GMOverriddenProperty",
            "mvc": "1.0",
            "objectId": "933839b8-24f5-4e0d-a2d1-a01d1293106c",
            "propertyId": "c10c8a12-58c0-4e31-aceb-6f08dc8be41f",
            "value": "4"
        },
        {
            "id": "ddd1e05a-45f0-4234-b84a-256c15b5e272",
            "modelName": "GMOverriddenProperty",
            "mvc": "1.0",
            "objectId": "2bf3fbb7-0b28-4f01-aa34-5810ce2e0d1e",
            "propertyId": "45378f07-4a1b-4ba8-b851-f4b1e87fa8e6",
            "value": "20"
        },
        {
            "id": "8eb2ce76-0610-492b-a36a-1e80eb4e095d",
            "modelName": "GMOverriddenProperty",
            "mvc": "1.0",
            "objectId": "2bf3fbb7-0b28-4f01-aa34-5810ce2e0d1e",
            "propertyId": "b1fdf3d3-b56b-465d-995e-c1adbd4c8200",
            "value": "4"
        }
    ],
    "parentObjectId": "2bf3fbb7-0b28-4f01-aa34-5810ce2e0d1e",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "c7aafd54-bb1a-4685-9e7a-22cb3b029b49",
    "visible": true
}