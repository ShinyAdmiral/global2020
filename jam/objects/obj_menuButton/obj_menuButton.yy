{
    "id": "ea5fa399-8de7-4866-9d78-3fddf1fd8ebb",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_menuButton",
    "eventList": [
        {
            "id": "a5ed92b2-7d95-48e0-a44e-a45804a38b68",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 10,
            "eventtype": 6,
            "m_owner": "ea5fa399-8de7-4866-9d78-3fddf1fd8ebb"
        },
        {
            "id": "ccd72a7b-d357-4eda-bf26-dd019e9c371d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 11,
            "eventtype": 6,
            "m_owner": "ea5fa399-8de7-4866-9d78-3fddf1fd8ebb"
        },
        {
            "id": "9acc561c-07de-45c4-b27a-cd53c1b0d4d1",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 6,
            "m_owner": "ea5fa399-8de7-4866-9d78-3fddf1fd8ebb"
        },
        {
            "id": "e9652abd-517f-479a-ae33-846d91917f04",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "ea5fa399-8de7-4866-9d78-3fddf1fd8ebb"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "77718bc0-8b04-4a20-934d-4947111d5abb",
    "visible": true
}