{
    "id": "ac9cf621-adad-4053-8214-c4ef8255e13a",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_bulb",
    "eventList": [
        {
            "id": "209d7f26-b202-431c-a719-60df04576c86",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "ac9cf621-adad-4053-8214-c4ef8255e13a"
        },
        {
            "id": "bc614776-721d-4fba-ba92-b84654884e36",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "ac9cf621-adad-4053-8214-c4ef8255e13a"
        },
        {
            "id": "76bb44a2-0ed8-4b04-96cb-fed86168b07a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 6,
            "m_owner": "ac9cf621-adad-4053-8214-c4ef8255e13a"
        },
        {
            "id": "32fb1553-d7a8-425a-b9dc-56a17bed7e99",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "ac9cf621-adad-4053-8214-c4ef8255e13a"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "9f833efb-e358-4705-b4c7-4bc235ed825f",
    "visible": true
}