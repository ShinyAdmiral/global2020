if(percent<90){
	draw_sprite(sprite_index,0,x,y);
	draw_sprite_part(spr_bulb_fill, 0, 0, 20-percent/5, 15, percent/5, x, y-percent/5+20);
} else draw_sprite(sprite_index,1,x,y)


if(instance_exists(obj_ChasisSpawner)){
	draw_set_halign(fa_left);
	draw_set_color(c_black);
	draw_set_font(fnt);
	draw_text(x+16,y+7,"Level " + string(obj_ChasisSpawner.skill-7))
	draw_text(x+17,y+6,"Level " + string(obj_ChasisSpawner.skill-7))
	draw_text(x+17,y+8,"Level " + string(obj_ChasisSpawner.skill-7))
	draw_text(x+18,y+7,"Level " + string(obj_ChasisSpawner.skill-7))
	draw_set_color(c_aqua);
	draw_text(x+17,y+7,"Level " + string(obj_ChasisSpawner.skill-7))
	draw_set_color(c_white);
} else instance_destroy();