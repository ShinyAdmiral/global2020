{
    "id": "87a69f5d-229b-4c90-a770-9c4739db115a",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_particleStuffing",
    "eventList": [
        {
            "id": "2c336c31-7ce7-435d-a9a8-bad850c5ef84",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "87a69f5d-229b-4c90-a770-9c4739db115a"
        },
        {
            "id": "20c419c8-321c-46e2-8030-8d4c3cfe889a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "87a69f5d-229b-4c90-a770-9c4739db115a"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "b6e06657-756f-4178-8c53-760aae9aee48",
    "visible": true
}