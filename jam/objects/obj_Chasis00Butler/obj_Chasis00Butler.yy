{
    "id": "81435835-a1a2-4492-919c-28881a6162e9",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_Chasis00Butler",
    "eventList": [
        {
            "id": "63c098c1-6c05-41fd-a171-a3600937b40b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "81435835-a1a2-4492-919c-28881a6162e9"
        },
        {
            "id": "7d4f619a-4d4c-4a60-a7b4-e6049f78021d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 1,
            "m_owner": "81435835-a1a2-4492-919c-28881a6162e9"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": [
        {
            "id": "201d2583-62ff-4d49-8e93-d241126e0131",
            "modelName": "GMOverriddenProperty",
            "mvc": "1.0",
            "objectId": "3dcad30f-7ed7-4876-bdf8-709b7bcf0b49",
            "propertyId": "f80650f0-a2a6-4d63-8019-866686fbdaf5",
            "value": "13"
        },
        {
            "id": "a1c4e757-c3f0-43e1-a132-f261c060f9f7",
            "modelName": "GMOverriddenProperty",
            "mvc": "1.0",
            "objectId": "3dcad30f-7ed7-4876-bdf8-709b7bcf0b49",
            "propertyId": "e8ec1153-9a24-410d-ad43-c8e2c75a090c",
            "value": "0"
        },
        {
            "id": "4458e715-7064-4f24-af3e-649ae2211b99",
            "modelName": "GMOverriddenProperty",
            "mvc": "1.0",
            "objectId": "3dcad30f-7ed7-4876-bdf8-709b7bcf0b49",
            "propertyId": "5a671739-5d5a-489c-8f4b-bf8e4d0762d0",
            "value": "2"
        },
        {
            "id": "10a296bf-5bc2-4f95-abcf-75e2760b1fd5",
            "modelName": "GMOverriddenProperty",
            "mvc": "1.0",
            "objectId": "3dcad30f-7ed7-4876-bdf8-709b7bcf0b49",
            "propertyId": "f1e3d7b5-b812-4770-8972-336937f8d5a4",
            "value": "4"
        },
        {
            "id": "fa1dd268-9666-4c29-8112-23594d98d3cc",
            "modelName": "GMOverriddenProperty",
            "mvc": "1.0",
            "objectId": "3dcad30f-7ed7-4876-bdf8-709b7bcf0b49",
            "propertyId": "5bf989de-3bc9-48a3-ad93-30379707fdde",
            "value": "24"
        },
        {
            "id": "d75fbc30-3e5f-4207-84f0-4d39627c8fa2",
            "modelName": "GMOverriddenProperty",
            "mvc": "1.0",
            "objectId": "3dcad30f-7ed7-4876-bdf8-709b7bcf0b49",
            "propertyId": "b97fbe8d-2226-4454-b330-0db58b8bdb4e",
            "value": "4"
        },
        {
            "id": "43f3a28b-6c24-41a9-a1f0-63cdd6727700",
            "modelName": "GMOverriddenProperty",
            "mvc": "1.0",
            "objectId": "3dcad30f-7ed7-4876-bdf8-709b7bcf0b49",
            "propertyId": "b22e7616-619d-4573-918a-3d8302b6088d",
            "value": "13"
        },
        {
            "id": "b69ad0db-3d80-4ea5-aa82-25a7adfd7bb1",
            "modelName": "GMOverriddenProperty",
            "mvc": "1.0",
            "objectId": "3dcad30f-7ed7-4876-bdf8-709b7bcf0b49",
            "propertyId": "7a363fbd-eff1-4533-931f-52453782cb4d",
            "value": "30"
        }
    ],
    "parentObjectId": "3dcad30f-7ed7-4876-bdf8-709b7bcf0b49",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "99e68f67-7096-4e00-a5f9-533c44a69f13",
    "visible": true
}