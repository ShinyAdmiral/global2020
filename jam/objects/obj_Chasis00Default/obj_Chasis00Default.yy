{
    "id": "384bd5be-1c2b-4ac6-9bc0-16a0ff65be9f",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_Chasis00Default",
    "eventList": [
        
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": [
        {
            "id": "93ee3e4c-afa8-4241-8ac8-f13f77104f46",
            "modelName": "GMOverriddenProperty",
            "mvc": "1.0",
            "objectId": "3dcad30f-7ed7-4876-bdf8-709b7bcf0b49",
            "propertyId": "f80650f0-a2a6-4d63-8019-866686fbdaf5",
            "value": "18"
        },
        {
            "id": "4199280f-129a-4d84-869e-eb09147bd8c9",
            "modelName": "GMOverriddenProperty",
            "mvc": "1.0",
            "objectId": "3dcad30f-7ed7-4876-bdf8-709b7bcf0b49",
            "propertyId": "e8ec1153-9a24-410d-ad43-c8e2c75a090c",
            "value": "4"
        },
        {
            "id": "7895bd7a-642e-4628-9da4-80c896092234",
            "modelName": "GMOverriddenProperty",
            "mvc": "1.0",
            "objectId": "3dcad30f-7ed7-4876-bdf8-709b7bcf0b49",
            "propertyId": "5a671739-5d5a-489c-8f4b-bf8e4d0762d0",
            "value": "4"
        },
        {
            "id": "881d5b1e-395e-40b5-b902-cb743c87a618",
            "modelName": "GMOverriddenProperty",
            "mvc": "1.0",
            "objectId": "3dcad30f-7ed7-4876-bdf8-709b7bcf0b49",
            "propertyId": "f1e3d7b5-b812-4770-8972-336937f8d5a4",
            "value": "10"
        },
        {
            "id": "3587181d-5b8d-401c-bbe2-8e0a2a805899",
            "modelName": "GMOverriddenProperty",
            "mvc": "1.0",
            "objectId": "3dcad30f-7ed7-4876-bdf8-709b7bcf0b49",
            "propertyId": "5bf989de-3bc9-48a3-ad93-30379707fdde",
            "value": "32"
        },
        {
            "id": "d4dcf2bb-7e70-4855-b524-5dbea00bd977",
            "modelName": "GMOverriddenProperty",
            "mvc": "1.0",
            "objectId": "3dcad30f-7ed7-4876-bdf8-709b7bcf0b49",
            "propertyId": "b97fbe8d-2226-4454-b330-0db58b8bdb4e",
            "value": "10"
        },
        {
            "id": "0f047129-1477-4a67-9219-d85db602ec93",
            "modelName": "GMOverriddenProperty",
            "mvc": "1.0",
            "objectId": "3dcad30f-7ed7-4876-bdf8-709b7bcf0b49",
            "propertyId": "b22e7616-619d-4573-918a-3d8302b6088d",
            "value": "9"
        },
        {
            "id": "dec249bf-4b08-416d-9713-5451b0488c17",
            "modelName": "GMOverriddenProperty",
            "mvc": "1.0",
            "objectId": "3dcad30f-7ed7-4876-bdf8-709b7bcf0b49",
            "propertyId": "7a363fbd-eff1-4533-931f-52453782cb4d",
            "value": "39"
        },
        {
            "id": "821056e9-a700-43bf-926c-b30820a9dff4",
            "modelName": "GMOverriddenProperty",
            "mvc": "1.0",
            "objectId": "3dcad30f-7ed7-4876-bdf8-709b7bcf0b49",
            "propertyId": "cbc008da-7b6c-4413-ae23-faa24a3ceed3",
            "value": "27"
        },
        {
            "id": "85c7b91a-6b57-40ce-9825-6ca427c75883",
            "modelName": "GMOverriddenProperty",
            "mvc": "1.0",
            "objectId": "3dcad30f-7ed7-4876-bdf8-709b7bcf0b49",
            "propertyId": "b058e2a0-3ecd-46d7-a5a4-b5eed3665631",
            "value": "39"
        }
    ],
    "parentObjectId": "3dcad30f-7ed7-4876-bdf8-709b7bcf0b49",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "2e205880-51df-457f-b7d2-30a67815e735",
    "visible": true
}