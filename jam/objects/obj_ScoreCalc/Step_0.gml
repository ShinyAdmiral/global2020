if (!complete)
{
	if (instance_exists(obj_ChasisParent))
	{
		if(instance_number(obj_BoltFixed) >= obj_ChasisParent.ammount_required)
		{
			total_score =
			head_score +
			Larm_score +
			Rarm_score +
			Lleg_score +
			Rleg_score +
			other1_score +
			other2_score +
			other3_score;
		
			average = total_score/obj_ChasisParent.ammount_required;
			
			with(obj_buildToCombat){
				qualityScore = 1/obj_ScoreCalc.average;
				qualityScore = clamp(qualityScore, 0.25, 1);
				robotName = object_get_name(obj_ChasisParent);
				if(instance_exists(obj_hintToRight))
				instance_destroy(obj_hintToRight);
				event_user(0);
			}
			show_debug_message("FINAL " + string(average));
			complete = true;
		}
	}
}
else
{
	if (!go)
	{	
		for (var i = 1; i < obj_ChasisParent.ammount_required + 1; i++)
		{
			part[i].hspeed = neg;
			//show_debug_message(part[i])
			bolt[i].hspeed = neg;
		}
		part[1].hspeed = neg;
		obj_ChasisParent.hspeed = neg;
		alarm[0] = room_speed/4;
		go = true;
	}
}