{
    "id": "ad9e7373-3668-4bbe-8d84-a1cdb835e4a8",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_ScoreCalc",
    "eventList": [
        {
            "id": "eb3c16d5-545d-46eb-9e39-50e027151486",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "ad9e7373-3668-4bbe-8d84-a1cdb835e4a8"
        },
        {
            "id": "824d6c05-68fc-444d-b2cc-d4df3551e185",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "ad9e7373-3668-4bbe-8d84-a1cdb835e4a8"
        },
        {
            "id": "00db261f-c20d-48d9-970f-f7a54cedcc90",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "ad9e7373-3668-4bbe-8d84-a1cdb835e4a8"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}