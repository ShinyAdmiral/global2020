{
    "id": "5bf9b31a-d4bd-4eb7-b4a6-c4f289fa6e7e",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_battlebot_sephiroth",
    "eventList": [
        
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": [
        {
            "id": "63bc24fb-3858-4d79-bdfe-790efbab90ec",
            "modelName": "GMOverriddenProperty",
            "mvc": "1.0",
            "objectId": "5e45bd04-119a-4fa1-a822-c0ff2342673a",
            "propertyId": "632b9632-a0ab-4a56-b0c2-f2d26b852e1c",
            "value": "2"
        },
        {
            "id": "4732906c-a13d-42f4-8c46-45b9c06f7a44",
            "modelName": "GMOverriddenProperty",
            "mvc": "1.0",
            "objectId": "5e45bd04-119a-4fa1-a822-c0ff2342673a",
            "propertyId": "57ca94ab-dfc6-41c9-a24b-ca24d9735148",
            "value": "60"
        },
        {
            "id": "45e928f5-5538-4a83-a872-b414e77add30",
            "modelName": "GMOverriddenProperty",
            "mvc": "1.0",
            "objectId": "5e45bd04-119a-4fa1-a822-c0ff2342673a",
            "propertyId": "bb048f5b-4d73-4dc6-9397-9b155e26fcdf",
            "value": "100"
        },
        {
            "id": "945a5e67-c945-459e-978f-274a22c7af84",
            "modelName": "GMOverriddenProperty",
            "mvc": "1.0",
            "objectId": "5e45bd04-119a-4fa1-a822-c0ff2342673a",
            "propertyId": "64ae687f-d01c-4d9a-b0f5-893a7a9726cc",
            "value": "obj_attackFlurry"
        }
    ],
    "parentObjectId": "5e45bd04-119a-4fa1-a822-c0ff2342673a",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "fb8302d0-8a47-4c19-8b0a-a317959df8fe",
    "visible": true
}