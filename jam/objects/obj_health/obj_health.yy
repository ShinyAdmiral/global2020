{
    "id": "8ad8ca55-1686-4d30-8065-aac1ea8e3ff3",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_health",
    "eventList": [
        {
            "id": "00829295-f713-459e-aa62-7553514e484d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "8ad8ca55-1686-4d30-8065-aac1ea8e3ff3"
        },
        {
            "id": "9bfc60a9-c7e3-4b17-8672-d651cf5fb54f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "8ad8ca55-1686-4d30-8065-aac1ea8e3ff3"
        },
        {
            "id": "aa423a6d-9065-4aa3-9841-cf62617767d2",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "8ad8ca55-1686-4d30-8065-aac1ea8e3ff3"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "ccd03ea8-ccc6-42a7-b864-21c00e7e02cb",
    "visible": true
}