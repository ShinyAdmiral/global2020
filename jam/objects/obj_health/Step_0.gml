hp = clamp(hp, 0, 20)

if (hp <= 0 && !dead)
{
	instance_create_layer(244, 108,"Battle", obj_SpawnerDead);
	instance_deactivate_object(obj_Spawner);
	instance_deactivate_object(obj_battlefieldTest);
	instance_deactivate_object(obj_ChasisSpawner);
	instance_deactivate_object(obj_ChasisParent);
	instance_deactivate_object(obj_BodyParent);
	instance_deactivate_object(obj_ScoreCalc);
	instance_destroy(obj_battlefieldTest);
	instance_destroy(obj_bulb);
	instance_create_layer(x,y,"a", obj_loss);

	
	dead = true
}

percent = hp/20 * 100;