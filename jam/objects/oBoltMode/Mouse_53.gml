/// @description Insert description here
// You can write your code in this editor
if (active == 1)
{
	if (pressed)
	{
		instance_create_layer(mouse_x,mouse_y, "Bolts",obj_Bolt);
		audio_sound_pitch(RivetGunClick, random_range(0.8,1.2))
		audio_play_sound(RivetGunClick,10,false)
		pressed = false;
	}
}