if(active = 0) 
{
	active = 1;
	if (sound)
	{
		num = choose(1,2,3)
		if (num == 1)
		{
			audio_sound_pitch(Rivets1, random_range(0.8,1.2))
			audio_play_sound(Rivets1,10,false)
		}
		else if (num == 2)
		{
			audio_sound_pitch(Rivets2, random_range(0.8,1.2))
			audio_play_sound(Rivets2,10,false)
		}
		else
		{
			audio_sound_pitch(Rivets3, random_range(0.8,1.2))
			audio_play_sound(Rivets3,10,false)
		}
		sound = false;
	}
}

else
{
	active = 0;
	instance_destroy(obj_Bolt)
	sound = true;
}