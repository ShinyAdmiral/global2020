{
    "id": "d6a111c9-0363-4601-a802-7c86a49d638f",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oBoltMode",
    "eventList": [
        {
            "id": "89bc0cfa-3bbd-4873-80ed-3d37bdfb4db1",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 6,
            "m_owner": "d6a111c9-0363-4601-a802-7c86a49d638f"
        },
        {
            "id": "cf134ff6-fc20-4cf9-95d2-f1535b28df16",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "d6a111c9-0363-4601-a802-7c86a49d638f"
        },
        {
            "id": "6a1c17e6-e8c4-4840-b10c-be581ca0ab2a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "d6a111c9-0363-4601-a802-7c86a49d638f"
        },
        {
            "id": "64fc0d5d-222d-4d96-9ec0-b4002a6b544e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 53,
            "eventtype": 6,
            "m_owner": "d6a111c9-0363-4601-a802-7c86a49d638f"
        },
        {
            "id": "662c73c3-0bf4-4416-a9d8-4ad57be516ac",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 56,
            "eventtype": 6,
            "m_owner": "d6a111c9-0363-4601-a802-7c86a49d638f"
        },
        {
            "id": "831282e4-f1ec-46d5-ad99-522aed676b49",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "d6a111c9-0363-4601-a802-7c86a49d638f"
        },
        {
            "id": "7838fc04-8005-4553-90d8-51dc967b2829",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 54,
            "eventtype": 6,
            "m_owner": "d6a111c9-0363-4601-a802-7c86a49d638f"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "642b6a61-2497-4f4e-9c49-e542fde750d5",
    "visible": true
}