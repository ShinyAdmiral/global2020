{
    "id": "68800126-136c-4215-b97d-0894e7875698",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_hintToLeft",
    "eventList": [
        {
            "id": "fb925ce0-de46-45f1-9d73-6b70f4e3fd01",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "68800126-136c-4215-b97d-0894e7875698"
        },
        {
            "id": "5e3191fd-ad00-44db-8593-4df78e060078",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 10,
            "eventtype": 6,
            "m_owner": "68800126-136c-4215-b97d-0894e7875698"
        },
        {
            "id": "3c9fefaa-1a94-4df0-a38d-3877f892699c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 11,
            "eventtype": 6,
            "m_owner": "68800126-136c-4215-b97d-0894e7875698"
        },
        {
            "id": "bd477f1b-607a-4377-87b7-488369a7d54a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "68800126-136c-4215-b97d-0894e7875698"
        },
        {
            "id": "1bdf05a7-59b8-4fa1-94bc-906e8a98ba88",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 6,
            "m_owner": "68800126-136c-4215-b97d-0894e7875698"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": [
        {
            "id": "719c11d6-2eb8-4936-aeb7-53eb197fd065",
            "modelName": "GMObjectProperty",
            "mvc": "1.0",
            "listItems": null,
            "multiselect": false,
            "rangeEnabled": false,
            "rangeMax": 10,
            "rangeMin": 0,
            "resourceFilter": 1023,
            "value": "obama",
            "varName": "hintText",
            "varType": 2
        }
    ],
    "solid": false,
    "spriteId": "8c586a24-9055-4011-9827-4cea34ac9260",
    "visible": true
}