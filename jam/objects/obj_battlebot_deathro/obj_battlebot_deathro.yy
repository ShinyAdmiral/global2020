{
    "id": "3273d614-b97a-4c8c-b126-077e46f8bc20",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_battlebot_deathro",
    "eventList": [
        
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": [
        {
            "id": "04f9dec9-6e43-4dd0-9ab8-63e694a0273d",
            "modelName": "GMOverriddenProperty",
            "mvc": "1.0",
            "objectId": "5e45bd04-119a-4fa1-a822-c0ff2342673a",
            "propertyId": "64ae687f-d01c-4d9a-b0f5-893a7a9726cc",
            "value": "obj_attackMortar"
        },
        {
            "id": "38a2c27d-2f44-4125-9b82-c16a3caa98e5",
            "modelName": "GMOverriddenProperty",
            "mvc": "1.0",
            "objectId": "5e45bd04-119a-4fa1-a822-c0ff2342673a",
            "propertyId": "632b9632-a0ab-4a56-b0c2-f2d26b852e1c",
            "value": "8"
        },
        {
            "id": "065e6d13-12a0-4290-9b65-e18b78a2f8a3",
            "modelName": "GMOverriddenProperty",
            "mvc": "1.0",
            "objectId": "5e45bd04-119a-4fa1-a822-c0ff2342673a",
            "propertyId": "bb048f5b-4d73-4dc6-9397-9b155e26fcdf",
            "value": "40"
        },
        {
            "id": "586c9c41-301d-430c-bfee-d15611bf97c2",
            "modelName": "GMOverriddenProperty",
            "mvc": "1.0",
            "objectId": "5e45bd04-119a-4fa1-a822-c0ff2342673a",
            "propertyId": "33cdc2fd-face-473f-9d03-762b69bcc1e2",
            "value": "40"
        },
        {
            "id": "a8420ed6-8549-48ea-9b8c-73cc1c407c96",
            "modelName": "GMOverriddenProperty",
            "mvc": "1.0",
            "objectId": "5e45bd04-119a-4fa1-a822-c0ff2342673a",
            "propertyId": "57ca94ab-dfc6-41c9-a24b-ca24d9735148",
            "value": "180"
        },
        {
            "id": "c25c08c1-8898-458e-ac7b-a02835c2842d",
            "modelName": "GMOverriddenProperty",
            "mvc": "1.0",
            "objectId": "5e45bd04-119a-4fa1-a822-c0ff2342673a",
            "propertyId": "b2a569b3-3828-44bb-a728-11de0b6d8628",
            "value": "0.10"
        }
    ],
    "parentObjectId": "5e45bd04-119a-4fa1-a822-c0ff2342673a",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "78016081-3531-4435-b71a-8f5dbbc15e71",
    "visible": true
}