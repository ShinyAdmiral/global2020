{
    "id": "2969aeec-336a-40dd-9e2f-4ca2bada5484",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_battlebot_bronze",
    "eventList": [
        
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": [
        {
            "id": "bd231063-0d49-4faa-ad94-7848c517c15d",
            "modelName": "GMOverriddenProperty",
            "mvc": "1.0",
            "objectId": "5e45bd04-119a-4fa1-a822-c0ff2342673a",
            "propertyId": "64ae687f-d01c-4d9a-b0f5-893a7a9726cc",
            "value": "obj_attackLaser"
        },
        {
            "id": "909d3afa-0727-4620-9d41-838f6f230e67",
            "modelName": "GMOverriddenProperty",
            "mvc": "1.0",
            "objectId": "5e45bd04-119a-4fa1-a822-c0ff2342673a",
            "propertyId": "33cdc2fd-face-473f-9d03-762b69bcc1e2",
            "value": "40"
        },
        {
            "id": "18534fba-e356-4e56-8149-1af04461234a",
            "modelName": "GMOverriddenProperty",
            "mvc": "1.0",
            "objectId": "5e45bd04-119a-4fa1-a822-c0ff2342673a",
            "propertyId": "632b9632-a0ab-4a56-b0c2-f2d26b852e1c",
            "value": "2"
        },
        {
            "id": "1b7755f0-b6ad-44eb-945b-f28f323d715b",
            "modelName": "GMOverriddenProperty",
            "mvc": "1.0",
            "objectId": "5e45bd04-119a-4fa1-a822-c0ff2342673a",
            "propertyId": "57ca94ab-dfc6-41c9-a24b-ca24d9735148",
            "value": "90"
        }
    ],
    "parentObjectId": "5e45bd04-119a-4fa1-a822-c0ff2342673a",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "8586ec10-bae1-4056-9bfc-4635c4530034",
    "visible": true
}