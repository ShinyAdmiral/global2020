{
    "id": "c1d7d772-5754-4f24-9c28-9c8b37da4939",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_battlebot_spaceman",
    "eventList": [
        {
            "id": "b4349774-7f74-46f6-b0fc-125e8fa65333",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "c1d7d772-5754-4f24-9c28-9c8b37da4939"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": [
        {
            "id": "da2dfb52-75dc-4060-9950-9ef57c7c0377",
            "modelName": "GMOverriddenProperty",
            "mvc": "1.0",
            "objectId": "5e45bd04-119a-4fa1-a822-c0ff2342673a",
            "propertyId": "33cdc2fd-face-473f-9d03-762b69bcc1e2",
            "value": "60"
        },
        {
            "id": "5fa4f124-5c89-4b78-bbbf-633bf51eb54e",
            "modelName": "GMOverriddenProperty",
            "mvc": "1.0",
            "objectId": "5e45bd04-119a-4fa1-a822-c0ff2342673a",
            "propertyId": "64ae687f-d01c-4d9a-b0f5-893a7a9726cc",
            "value": "obj_lightningRadius"
        }
    ],
    "parentObjectId": "5e45bd04-119a-4fa1-a822-c0ff2342673a",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "59846448-dbbd-4f23-bfb5-2b909c03f1a6",
    "visible": true
}