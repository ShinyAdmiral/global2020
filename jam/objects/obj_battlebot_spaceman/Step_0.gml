if(x<16) {
	x++
}

if(idle){
	if (image_angle != 0){
		sinCount += moveSpeed*20
		if(sinCount >= 360) sinCount = 0

		var sinOut;
		sinOut = dsin(sinCount)

		y+=dsin(sinCount*2)*bobbing

		image_angle = sinOut*wobble
	}
	if(instance_exists(obj_battlefieldTestEnemy)) idle = false;
} else {
	if (atkcount > 0){
		atkcount-=1;
	}
	if(instance_exists(target) && distance_to_object(target)>range){
	
		direction = point_direction(x,y,target.x,target.y)
		x+=lengthdir_x(moveSpeed,direction)
		y+=lengthdir_y(moveSpeed,direction)
		depth = -y
	
		sinCount += moveSpeed*20
		if(sinCount >= 360) sinCount = 0

		var sinOut;
		sinOut = dsin(sinCount)

		y+=dsin(sinCount*2)*bobbing

		image_angle = sinOut*wobble
	
	} else if (image_angle != 0){
		sinCount += moveSpeed*20
		if(sinCount >= 360) sinCount = 0

		var sinOut;
		sinOut = dsin(sinCount)

		y+=dsin(sinCount*2)*bobbing

		image_angle = sinOut*wobble
	} else {
		if(atkcount = 0){
			
			instance_create_depth(x,y-sprite_height/2,-room_height,projectile)
		
			atkcount = atkspd

		
		}
	}
}