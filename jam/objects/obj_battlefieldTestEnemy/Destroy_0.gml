if(instance_exists(obj_bulb))
{
	with(obj_battlefieldTest) event_user(0);
	obj_bulb.percent+=30;
	audio_sound_pitch(StuffedDeath, random_range(0.8,1.2))
	audio_play_sound(StuffedDeath,10,false);
}
repeat(irandom(stuffingCount)+4) instance_create_depth(x,y,depth,obj_particleStuffing);