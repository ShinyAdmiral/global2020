{
    "id": "a4f81076-52c6-4dd0-9ccf-17320ad04e0a",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_button_continue",
    "eventList": [
        {
            "id": "115fb8a4-45dc-4f26-8af9-126234a3f0e0",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 10,
            "eventtype": 6,
            "m_owner": "a4f81076-52c6-4dd0-9ccf-17320ad04e0a"
        },
        {
            "id": "97e5d369-41e1-454e-bb2c-6e505168b0fd",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 11,
            "eventtype": 6,
            "m_owner": "a4f81076-52c6-4dd0-9ccf-17320ad04e0a"
        },
        {
            "id": "48067ea0-33da-4431-88f1-6db4f0a23186",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 6,
            "m_owner": "a4f81076-52c6-4dd0-9ccf-17320ad04e0a"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "e02b5fee-c8d2-40cb-a4e9-67527daf7859",
    "visible": true
}