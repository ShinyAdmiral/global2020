{
    "id": "8b1258c7-0f93-47cc-b0c4-3c5b85b89e57",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_PartParent",
    "eventList": [
        {
            "id": "7f797ed7-2143-4f37-a6d0-363cc85bd86a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 6,
            "m_owner": "8b1258c7-0f93-47cc-b0c4-3c5b85b89e57"
        },
        {
            "id": "05d30052-ed7d-422e-8b52-8ae9b12a5481",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "8b1258c7-0f93-47cc-b0c4-3c5b85b89e57"
        },
        {
            "id": "d0337f1a-78ab-467f-824b-1aa11fb21678",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "8b1258c7-0f93-47cc-b0c4-3c5b85b89e57"
        },
        {
            "id": "927c0901-3df8-4c21-9c24-021636f22cd2",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 56,
            "eventtype": 6,
            "m_owner": "8b1258c7-0f93-47cc-b0c4-3c5b85b89e57"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}