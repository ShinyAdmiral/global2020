if(selected)
{ 
	x = mouse_x + offsetX;
	y = mouse_y + offsetY;
}

if (place_meeting(x,y,obj_Bolt) && precision_grader)
{
	percent_bolt = (sqrt(sqr(obj_Bolt.x - x) + sqr(obj_Bolt.y- y)) / max_level * 100);
	
	percent_limb = 
	
	
	instance_create_layer(obj_Bolt.x, obj_Bolt.y, "Bolts", obj_BoltPlaced);
	instance_destroy(obj_Bolt);
	
	precision_bolt_grade = calc_Precision(percent_bolt);
	
	
	precision_grader = false;
}

if (precision_grader = false)
{
	show_debug_message(precision_bolt_grade);
}