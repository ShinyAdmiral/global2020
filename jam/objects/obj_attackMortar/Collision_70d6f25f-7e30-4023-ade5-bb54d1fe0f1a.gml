repeat(irandom(6)+4) instance_create_depth(other.x-8,other.y-16,-room_height,obj_particleLaser)
var shoot
if(instance_exists(target)) {
	
	repeat(irandom_range(6,10)) {
		shoot = instance_create_depth(x,y,-room_height,obj_attackLaser);
		shoot.direction = random(360)
		shoot.image_angle = shoot.direction;
		shoot.blacklist = other
	}
	
	target.hp -= damage;
	if(target.hp<=0) instance_destroy(target);
	instance_destroy();
	with(obj_battlefieldTest) event_user(0);
}