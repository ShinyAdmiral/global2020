{
    "id": "f1b58d15-efba-496d-8b11-1dfde335e1aa",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_attackMortar",
    "eventList": [
        {
            "id": "70d6f25f-7e30-4023-ade5-bb54d1fe0f1a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "ab0ddc07-19b6-4910-aecf-70b4be6d004e",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "f1b58d15-efba-496d-8b11-1dfde335e1aa"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "13b28ffe-b51e-4c6b-82c3-2034fa3164ac",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "4a152593-0928-4786-a3da-862161822fe0",
    "visible": true
}