if (start)
{
	strp = string_copy(str,13, string_length(str));
	ran = choose(0,1);

	show_debug_message(strp)

	if (ran = 0)
	{
		switch (strp)
		{
			case "Default":
			ranS = irandom_range(0,4)
		
			switch (ranS)
			{
				case 0:
				if (!instance_exists(obj_Head0000Default))
				instance_create_layer(xpos, ypos, "Under_Instance", obj_Head0000Default);
				else if (!instance_exists(obj_RightArmDefault))
				instance_create_layer(xpos, ypos, "Under_Instance", obj_RightArmDefault);
				else if (!instance_exists(obj_LeftArm0Default))
				instance_create_layer(xpos, ypos, "Under_Instance", obj_LeftArm0Default);
				else if (!instance_exists(obj_RightLegDefault))
				instance_create_layer(xpos, ypos, "Under_Instance", obj_RightLegDefault);
				else 
				instance_create_layer(xpos, ypos, "Under_Instance", obj_LeftLeg0Default);
				break;
			
				case 1:
				if (!instance_exists(obj_RightArmDefault))
				instance_create_layer(xpos, ypos, "Under_Instance", obj_RightArmDefault);
				else if (!instance_exists(obj_LeftArm0Default))
				instance_create_layer(xpos, ypos, "Under_Instance", obj_LeftArm0Default);
				else if (!instance_exists(obj_RightLegDefault))
				instance_create_layer(xpos, ypos, "Under_Instance", obj_RightLegDefault);
				else if (!instance_exists(obj_LeftLeg0Default))
				instance_create_layer(xpos, ypos, "Under_Instance", obj_LeftLeg0Default);
				else 
				instance_create_layer(xpos, ypos, "Under_Instance", obj_Head0000Default);
				break;
			
				case 2:
				if (!instance_exists(obj_LeftArm0Default))
				instance_create_layer(xpos, ypos, "Under_Instance", obj_LeftArm0Default);
				else if (!instance_exists(obj_RightLegDefault))
				instance_create_layer(xpos, ypos, "Under_Instance", obj_RightLegDefault);
				else if (!instance_exists(obj_LeftLeg0Default))
				instance_create_layer(xpos, ypos, "Under_Instance", obj_LeftLeg0Default);
				else if (!instance_exists(obj_Head0000Default))
				instance_create_layer(xpos, ypos, "Under_Instance", obj_Head0000Default);
				else 
				instance_create_layer(xpos, ypos, "Under_Instance", obj_RightArmDefault);
				break;
			
				case 3:
				if (!instance_exists(obj_RightLegDefault))
				instance_create_layer(xpos, ypos, "Under_Instance", obj_RightLegDefault);
				else if (!instance_exists(obj_LeftLeg0Default))
				instance_create_layer(xpos, ypos, "Under_Instance", obj_LeftLeg0Default);
				else if (!instance_exists(obj_Head0000Default))
				instance_create_layer(xpos, ypos, "Under_Instance", obj_Head0000Default);
				else if (!instance_exists(obj_RightArmDefault))
				instance_create_layer(xpos, ypos, "Under_Instance", obj_RightArmDefault);
				else 
				instance_create_layer(xpos, ypos, "Under_Instance", obj_LeftArm0Default);
				break;
			
				case 4:
				if (!instance_exists(obj_LeftLeg0Default))
				instance_create_layer(xpos, ypos, "Under_Instance", obj_LeftLeg0Default);
				else if (!instance_exists(obj_RightLegDefault))
				instance_create_layer(xpos, ypos, "Under_Instance", obj_RightLegDefault);
				else if (!instance_exists(obj_Head0000Default))
				instance_create_layer(xpos, ypos, "Under_Instance", obj_Head0000Default);
				else if (!instance_exists(obj_RightArmDefault))
				instance_create_layer(xpos, ypos, "Under_Instance", obj_RightArmDefault);
				else 
				instance_create_layer(xpos, ypos, "Under_Instance", obj_LeftArm0Default);
				break;
			}
			break;
		
			case "Grievous":
			ranS = irandom_range(0,6)
		
			switch (ranS)
			{
				case 0:
				if (!instance_exists(obj_Head0000Grievous))
				instance_create_layer(xpos, ypos, "Under_Instance", obj_Head0000Grievous);
				else if (!instance_exists(obj_RightArmGrievous))
				instance_create_layer(xpos, ypos, "Under_Instance", obj_RightArmGrievous);
				else if (!instance_exists(obj_LeftArm0Grievous))
				instance_create_layer(xpos, ypos, "Under_Instance", obj_LeftArm0Grievous);
				else if (!instance_exists(obj_RightLegGrievous))
				instance_create_layer(xpos, ypos, "Under_Instance", obj_RightLegGrievous);
				else if (!instance_exists(obj_LeftLeg0Grievous))
				instance_create_layer(xpos, ypos, "Under_Instance", obj_LeftLeg0Grievous);
				else if(!instance_exists(obj_Other100Grievous))
				instance_create_layer(xpos, ypos, "Under_Instance", obj_Other100Grievous);
				else
				instance_create_layer(xpos, ypos, "Under_Instance", obj_Other200Grievous);
				break;
			
				case 1:
				if (!instance_exists(obj_RightArmDefault))
				instance_create_layer(xpos, ypos, "Under_Instance", obj_RightArmGrievous);
				else if (!instance_exists(obj_LeftArm0Grievous))
				instance_create_layer(xpos, ypos, "Under_Instance", obj_LeftArm0Grievous);
				else if (!instance_exists(obj_RightLegGrievous))
				instance_create_layer(xpos, ypos, "Under_Instance", obj_RightLegGrievous);
				else if (!instance_exists(obj_LeftLeg0Grievous))
				instance_create_layer(xpos, ypos, "Under_Instance", obj_LeftLeg0Grievous);
				else if(!instance_exists(obj_Other100Grievous))
				instance_create_layer(xpos, ypos, "Under_Instance", obj_Other100Grievous);
				else if (!instance_exists(obj_Other200Grievous))
				instance_create_layer(xpos, ypos, "Under_Instance", obj_Other200Grievous);
				else
				instance_create_layer(xpos, ypos, "Under_Instance", obj_Head0000Grievous);
				break;
			
				case 2:
				if (!instance_exists(obj_LeftArm0Default))
				instance_create_layer(xpos, ypos, "Under_Instance", obj_LeftArm0Grievous);
				else if (!instance_exists(obj_RightLegGrievous))
				instance_create_layer(xpos, ypos, "Under_Instance", obj_RightLegGrievous);
				else if (!instance_exists(obj_LeftLeg0Grievous))
				instance_create_layer(xpos, ypos, "Under_Instance", obj_LeftLeg0Grievous);
				else if(!instance_exists(obj_Other100Grievous))
				instance_create_layer(xpos, ypos, "Under_Instance", obj_Other100Grievous);
				else if (!instance_exists(obj_Other200Grievous))
				instance_create_layer(xpos, ypos, "Under_Instance", obj_Other200Grievous);
				else if (!instance_exists(obj_Head0000Grievous))
				instance_create_layer(xpos, ypos, "Under_Instance", obj_Head0000Grievous);
				else 
				instance_create_layer(xpos, ypos, "Under_Instance", obj_RightArmGrievous);
				break;
			
				case 3:
				if (!instance_exists(obj_RightLegGrievous))
				instance_create_layer(xpos, ypos, "Under_Instance", obj_RightLegGrievous);
				else if (!instance_exists(obj_LeftLeg0Grievous))
				instance_create_layer(xpos, ypos, "Under_Instance", obj_LeftLeg0Grievous);
				else if(!instance_exists(obj_Other100Grievous))
				instance_create_layer(xpos, ypos, "Under_Instance", obj_Other100Grievous);
				else if (!instance_exists(obj_Other200Grievous))
				instance_create_layer(xpos, ypos, "Under_Instance", obj_Other200Grievous);
				else if (!instance_exists(obj_Head0000Grievous))
				instance_create_layer(xpos, ypos, "Under_Instance", obj_Head0000Grievous);
				else if (!instance_exists(obj_RightArmGrievous))
				instance_create_layer(xpos, ypos, "Under_Instance", obj_RightArmGrievous);
				else 
				instance_create_layer(xpos, ypos, "Under_Instance", obj_LeftArm0Grievous);
				break;
			
				case 4:
				if (!instance_exists(obj_LeftLeg0Grievous))
				instance_create_layer(xpos, ypos, "Under_Instance", obj_LeftLeg0Grievous);
				else if(!instance_exists(obj_Other100Grievous))
				instance_create_layer(xpos, ypos, "Under_Instance", obj_Other100Grievous);
				else if (!instance_exists(obj_Other200Grievous))
				instance_create_layer(xpos, ypos, "Under_Instance", obj_Other200Grievous);
				else if (!instance_exists(obj_Head0000Grievous))
				instance_create_layer(xpos, ypos, "Under_Instance", obj_Head0000Grievous);
				else if (!instance_exists(obj_RightArmGrievous))
				instance_create_layer(xpos, ypos, "Under_Instance", obj_RightArmGrievous);
				else if (!instance_exists(obj_LeftArm0Grievous))
				instance_create_layer(xpos, ypos, "Under_Instance", obj_LeftArm0Grievous);
				else 
				instance_create_layer(xpos, ypos, "Under_Instance", obj_RightLegGrievous);
				break;
			
				case 5:
				if(!instance_exists(obj_Other100Grievous))
				instance_create_layer(xpos, ypos, "Under_Instance", obj_Other100Grievous);
				else if (!instance_exists(obj_Other200Grievous))
				instance_create_layer(xpos, ypos, "Under_Instance", obj_Other200Grievous);
				else if (!instance_exists(obj_Head0000Grievous))
				instance_create_layer(xpos, ypos, "Under_Instance", obj_Head0000Grievous);
				else if (!instance_exists(obj_RightArmGrievous))
				instance_create_layer(xpos, ypos, "Under_Instance", obj_RightArmGrievous);
				else if (!instance_exists(obj_LeftArm0Grievous))
				instance_create_layer(xpos, ypos, "Under_Instance", obj_LeftArm0Grievous);
				else if (!instance_exists(obj_RightLegGrievous))
				instance_create_layer(xpos, ypos, "Under_Instance", obj_RightLegGrievous);
				else
				instance_create_layer(xpos, ypos, "Under_Instance", obj_LeftLeg0Grievous);
				break;
			
				case 6:
				if (!instance_exists(obj_Other200Grievous))
				instance_create_layer(xpos, ypos, "Under_Instance", obj_Other200Grievous);
				else if (!instance_exists(obj_Head0000Grievous))
				instance_create_layer(xpos, ypos, "Under_Instance", obj_Head0000Grievous);
				else if (!instance_exists(obj_RightArmGrievous))
				instance_create_layer(xpos, ypos, "Under_Instance", obj_RightArmGrievous);
				else if (!instance_exists(obj_LeftArm0Grievous))
				instance_create_layer(xpos, ypos, "Under_Instance", obj_LeftArm0Grievous);
				else if (!instance_exists(obj_RightLegGrievous))
				instance_create_layer(xpos, ypos, "Under_Instance", obj_RightLegGrievous);
				else if (!instance_exists(obj_LeftLeg0Grievous))
				instance_create_layer(xpos, ypos, "Under_Instance", obj_LeftLeg0Grievous);
				else
				instance_create_layer(xpos, ypos, "Under_Instance", obj_Other100Grievous);
				break;
			}
			break;
		
			case "Bronze":
			ranS = irandom_range(0,4)
		
			switch (ranS)
			{
				case 0:
				if (!instance_exists(obj_Head0000Bronze))
				instance_create_layer(xpos, ypos, "Under_Instance", obj_Head0000Bronze);
				else if (!instance_exists(obj_RightArmBronze))
				instance_create_layer(xpos, ypos, "Under_Instance", obj_RightArmBronze);
				else if (!instance_exists(obj_LeftArm0Bronze))
				instance_create_layer(xpos, ypos, "Under_Instance", obj_LeftArm0Bronze);
				else if (!instance_exists(obj_RightLegBronze))
				instance_create_layer(xpos, ypos, "Under_Instance", obj_RightLegBronze);
				else
				instance_create_layer(xpos, ypos, "Under_Instance", obj_LeftLeg0Bronze);
				break;
			
				case 1:
				if (!instance_exists(obj_RightArmBronze))
				instance_create_layer(xpos, ypos, "Under_Instance", obj_RightArmBronze);
				else if (!instance_exists(obj_LeftArm0Bronze))
				instance_create_layer(xpos, ypos, "Under_Instance", obj_LeftArm0Bronze);
				else if (!instance_exists(obj_RightLegBronze))
				instance_create_layer(xpos, ypos, "Under_Instance", obj_RightLegBronze);
				else if (!instance_exists(obj_LeftLeg0Bronze))
				instance_create_layer(xpos, ypos, "Under_Instance", obj_LeftLeg0Bronze);
				else
				instance_create_layer(xpos, ypos, "Under_Instance", obj_Head0000Bronze);
				break;
			
				case 2:
				if (!instance_exists(obj_LeftArm0Bronze))
				instance_create_layer(xpos, ypos, "Under_Instance", obj_LeftArm0Bronze);
				else if (!instance_exists(obj_RightLegBronze))
				instance_create_layer(xpos, ypos, "Under_Instance", obj_RightLegBronze);
				else if (!instance_exists(obj_LeftLeg0Bronze))
				instance_create_layer(xpos, ypos, "Under_Instance", obj_LeftLeg0Bronze);
				else if (!instance_exists(obj_Head0000Bronze))
				instance_create_layer(xpos, ypos, "Under_Instance", obj_Head0000Bronze);
				else
				instance_create_layer(xpos, ypos, "Under_Instance", obj_RightArmBronze);
				break;
			
				case 3:
				if (!instance_exists(obj_RightLegBronze))
				instance_create_layer(xpos, ypos, "Under_Instance", obj_RightLegBronze);
				else if (!instance_exists(obj_LeftLeg0Bronze))
				instance_create_layer(xpos, ypos, "Under_Instance", obj_LeftLeg0Bronze);
				else if (!instance_exists(obj_Head0000Bronze))
				instance_create_layer(xpos, ypos, "Under_Instance", obj_Head0000Bronze);
				else if (!instance_exists(obj_RightArmBronze))
				instance_create_layer(xpos, ypos, "Under_Instance", obj_RightArmBronze);
				else
				instance_create_layer(xpos, ypos, "Under_Instance", obj_LeftArm0Bronze);
				break;
			
				case 4:
				if (!instance_exists(obj_LeftLeg0Bronze))
				instance_create_layer(xpos, ypos, "Under_Instance", obj_LeftLeg0Bronze);
				else if (!instance_exists(obj_Head0000Bronze))
				instance_create_layer(xpos, ypos, "Under_Instance", obj_Head0000Bronze);
				else if (!instance_exists(obj_RightArmBronze))
				instance_create_layer(xpos, ypos, "Under_Instance", obj_RightArmBronze);
				else if (!instance_exists(obj_LeftArm0Bronze))
				instance_create_layer(xpos, ypos, "Under_Instance", obj_LeftArm0Bronze);
				else
				instance_create_layer(xpos, ypos, "Under_Instance", obj_RightLegBronze);
				break;
			}
			break;
		
			case "Ali":
			ranS = irandom_range(0,4)
		
			switch (ranS)
			{
				case 0:
				if (!instance_exists(obj_Head0000Ali))
				instance_create_layer(xpos, ypos, "Under_Instance", obj_Head0000Ali);
				else if (!instance_exists(obj_RightArmAli))
				instance_create_layer(xpos, ypos, "Under_Instance", obj_RightArmAli);
				else if (!instance_exists(obj_LeftArm0Ali))
				instance_create_layer(xpos, ypos, "Under_Instance", obj_LeftArm0Ali);
				else if (!instance_exists(obj_RightLegAli))
				instance_create_layer(xpos, ypos, "Under_Instance", obj_RightLegAli);
				else
				instance_create_layer(xpos, ypos, "Under_Instance", obj_LeftLeg0Ali);
				break;
			
				case 1:
				if (!instance_exists(obj_RightArmAli))
				instance_create_layer(xpos, ypos, "Under_Instance", obj_RightArmAli);
				else if (!instance_exists(obj_LeftArm0Ali))
				instance_create_layer(xpos, ypos, "Under_Instance", obj_LeftArm0Ali);
				else if (!instance_exists(obj_RightLegAli))
				instance_create_layer(xpos, ypos, "Under_Instance", obj_RightLegAli);
				else if (!instance_exists(obj_LeftLeg0Ali))
				instance_create_layer(xpos, ypos, "Under_Instance", obj_LeftLeg0Ali);
				else
				instance_create_layer(xpos, ypos, "Under_Instance", obj_Head0000Ali);
				break;
			
				case 2:
				if (!instance_exists(obj_LeftArm0Ali))
				instance_create_layer(xpos, ypos, "Under_Instance", obj_LeftArm0Ali);
				else if (!instance_exists(obj_RightLegAli))
				instance_create_layer(xpos, ypos, "Under_Instance", obj_RightLegAli);
				else if (!instance_exists(obj_LeftLeg0Ali))
				instance_create_layer(xpos, ypos, "Under_Instance", obj_LeftLeg0Ali);
				else if (!instance_exists(obj_Head0000Ali))
				instance_create_layer(xpos, ypos, "Under_Instance", obj_Head0000Ali);
				else
				instance_create_layer(xpos, ypos, "Under_Instance", obj_RightArmAli);
				break;
			
				case 3:
				if (!instance_exists(obj_RightLegAli))
				instance_create_layer(xpos, ypos, "Under_Instance", obj_RightLegAli);
				else if (!instance_exists(obj_LeftLeg0Ali))
				instance_create_layer(xpos, ypos, "Under_Instance", obj_LeftLeg0Ali);
				else if (!instance_exists(obj_Head0000Ali))
				instance_create_layer(xpos, ypos, "Under_Instance", obj_Head0000Ali);
				else if (!instance_exists(obj_RightArmAli))
				instance_create_layer(xpos, ypos, "Under_Instance", obj_RightArmAli);
				else
				instance_create_layer(xpos, ypos, "Under_Instance", obj_LeftArm0Ali);
				break;
			
				case 4:
				if (!instance_exists(obj_LeftLeg0Ali))
				instance_create_layer(xpos, ypos, "Under_Instance", obj_LeftLeg0Ali);
				else if (!instance_exists(obj_Head0000Ali))
				instance_create_layer(xpos, ypos, "Under_Instance", obj_Head0000Ali);
				else if (!instance_exists(obj_RightArmAli))
				instance_create_layer(xpos, ypos, "Under_Instance", obj_RightArmAli);
				else if (!instance_exists(obj_LeftArm0Ali))
				instance_create_layer(xpos, ypos, "Under_Instance", obj_LeftArm0Ali);
				else
				instance_create_layer(xpos, ypos, "Under_Instance", obj_RightLegAli);
				break;
			}
			break;
			ranS = irandom_range(0,3)
		
			case "Butler":
			show_debug_message("here")
			switch (ranS)
			{
				case 0:
				if (!instance_exists(obj_Head0000Butler))
				instance_create_layer(xpos, ypos, "Under_Instance", obj_Head0000Butler);
				else if (!instance_exists(obj_RightArmButler))
				instance_create_layer(xpos, ypos, "Under_Instance", obj_RightArmButler);
				else if (!instance_exists(obj_LeftArm0Butler))
				instance_create_layer(xpos, ypos, "Under_Instance", obj_LeftArm0Butler);
				else
				instance_create_layer(xpos, ypos, "Under_Instance", obj_LeftLeg0Butler);
				break;
			
				case 1:
				if (!instance_exists(obj_RightArmButler))
				instance_create_layer(xpos, ypos, "Under_Instance", obj_RightArmButler);
				else if (!instance_exists(obj_LeftArm0Butler))
				instance_create_layer(xpos, ypos, "Under_Instance", obj_LeftArm0Butler);
				else if (!instance_exists(obj_LeftLeg0Butler))
				instance_create_layer(xpos, ypos, "Under_Instance", obj_LeftLeg0Butler);
				else
				instance_create_layer(xpos, ypos, "Under_Instance", obj_Head0000Butler);
				break;
			
				case 2:
				if (!instance_exists(obj_LeftArm0Butler))
				instance_create_layer(xpos, ypos, "Under_Instance", obj_LeftArm0Butler);
				else if (!instance_exists(obj_LeftLeg0Butler))
				instance_create_layer(xpos, ypos, "Under_Instance", obj_LeftLeg0Butler);
				else if (!instance_exists(obj_Head0000Butler))
				instance_create_layer(xpos, ypos, "Under_Instance", obj_Head0000Butler);
				else
				instance_create_layer(xpos, ypos, "Under_Instance", obj_RightArmButler);
				break;
			
				case 3:
				if (!instance_exists(obj_LeftLeg0Butler))
				instance_create_layer(xpos, ypos, "Under_Instance", obj_LeftLeg0Butler);
				else if (!instance_exists(obj_Head0000Butler))
				instance_create_layer(xpos, ypos, "Under_Instance", obj_Head0000Butler);
				else if (!instance_exists(obj_RightArmButler))
				instance_create_layer(xpos, ypos, "Under_Instance", obj_RightArmButler);
				else
				instance_create_layer(xpos, ypos, "Under_Instance", obj_LeftArm0Butler);
				break;
			}
			break;
		
			case "Spaceman":
			ranS = irandom_range(0,4)
		
			switch (ranS)
			{
				case 0:
				if (!instance_exists(obj_Head0000Spaceman))
				instance_create_layer(xpos, ypos, "Under_Instance", obj_Head0000Spaceman);
				else if (!instance_exists(obj_RightArmSpaceman))
				instance_create_layer(xpos, ypos, "Under_Instance", obj_RightArmSpaceman);
				else if (!instance_exists(obj_LeftArm0Spaceman))
				instance_create_layer(xpos, ypos, "Under_Instance", obj_LeftArm0Spaceman);
				else if (!instance_exists(obj_RightLegSpaceman))
				instance_create_layer(xpos, ypos, "Under_Instance", obj_RightLegSpaceman);
				else
				instance_create_layer(xpos, ypos, "Under_Instance", obj_LeftLeg0Spaceman);
				break;
			
				case 1:
				if (!instance_exists(obj_RightArmSpaceman))
				instance_create_layer(xpos, ypos, "Under_Instance", obj_RightArmSpaceman);
				else if (!instance_exists(obj_LeftArm0Spaceman))
				instance_create_layer(xpos, ypos, "Under_Instance", obj_LeftArm0Spaceman);
				else if (!instance_exists(obj_RightLegSpaceman))
				instance_create_layer(xpos, ypos, "Under_Instance", obj_RightLegSpaceman);
				else if (!instance_exists(obj_LeftLeg0Spaceman))
				instance_create_layer(xpos, ypos, "Under_Instance", obj_LeftLeg0Spaceman);
				else
				instance_create_layer(xpos, ypos, "Under_Instance", obj_Head0000Spaceman);
				break;
			
				case 2:
				if (!instance_exists(obj_LeftArm0Spaceman))
				instance_create_layer(xpos, ypos, "Under_Instance", obj_LeftArm0Spaceman);
				else if (!instance_exists(obj_RightLegSpaceman))
				instance_create_layer(xpos, ypos, "Under_Instance", obj_RightLegSpaceman);
				else if (!instance_exists(obj_LeftLeg0Spaceman))
				instance_create_layer(xpos, ypos, "Under_Instance", obj_LeftLeg0Spaceman);
				else if (!instance_exists(obj_Head0000Spaceman))
				instance_create_layer(xpos, ypos, "Under_Instance", obj_Head0000Spaceman);
				else
				instance_create_layer(xpos, ypos, "Under_Instance", obj_RightArmSpaceman);
				break;
			
				case 3:
				if (!instance_exists(obj_RightLegSpaceman))
				instance_create_layer(xpos, ypos, "Under_Instance", obj_RightLegSpaceman);
				else if (!instance_exists(obj_LeftLeg0Spaceman))
				instance_create_layer(xpos, ypos, "Under_Instance", obj_LeftLeg0Spaceman);
				else if (!instance_exists(obj_Head0000Spaceman))
				instance_create_layer(xpos, ypos, "Under_Instance", obj_Head0000Spaceman);
				else if (!instance_exists(obj_RightArmSpaceman))
				instance_create_layer(xpos, ypos, "Under_Instance", obj_RightArmSpaceman);
				else
				instance_create_layer(xpos, ypos, "Under_Instance", obj_LeftArm0Spaceman);
				break;
			
				case 4:
				if (!instance_exists(obj_LeftLeg0Spaceman))
				instance_create_layer(xpos, ypos, "Under_Instance", obj_LeftLeg0Spaceman);
				else if (!instance_exists(obj_Head0000Spaceman))
				instance_create_layer(xpos, ypos, "Under_Instance", obj_Head0000Spaceman);
				else if (!instance_exists(obj_RightArmSpaceman))
				instance_create_layer(xpos, ypos, "Under_Instance", obj_RightArmSpaceman);
				else if (!instance_exists(obj_LeftArm0Spaceman))
				instance_create_layer(xpos, ypos, "Under_Instance", obj_LeftArm0Spaceman);
				else
				instance_create_layer(xpos, ypos, "Under_Instance", obj_RightLegSpaceman);
				break;
			}
			break;
		
			case "Sephiroth":
			ranS = irandom_range(0,6)
		
			switch (ranS)
			{
				case 0:
				if (!instance_exists(obj_Head0000Sephiroth))
				instance_create_layer(xpos, ypos, "Under_Instance", obj_Head0000Sephiroth);
				else if (!instance_exists(obj_RightArmSephiroth))
				instance_create_layer(xpos, ypos, "Under_Instance", obj_RightArmSephiroth);
				else if (!instance_exists(obj_LeftArm0Sephiroth))
				instance_create_layer(xpos, ypos, "Under_Instance", obj_LeftArm0Sephiroth);
				else if (!instance_exists(obj_RightLegSephiroth))
				instance_create_layer(xpos, ypos, "Under_Instance", obj_RightLegSephiroth);
				else if (!instance_exists(obj_LeftLeg0Sephiroth))
				instance_create_layer(xpos, ypos, "Under_Instance", obj_LeftLeg0Sephiroth);
				else if (!instance_exists(obj_Other100Sephiroth))
				instance_create_layer(xpos, ypos, "Under_Instance", obj_Other100Sephiroth);
				else if (!instance_exists(obj_Other200Sephiroth))
				instance_create_layer(xpos, ypos, "Under_Instance", obj_Other200Sephiroth);
				else if (!instance_exists(obj_Other300Sephiroth))
				instance_create_layer(xpos, ypos, "Under_Instance", obj_Other300Sephiroth);
				break;
			
				case 1:
				if (!instance_exists(obj_RightArmSephiroth))
				instance_create_layer(xpos, ypos, "Under_Instance", obj_RightArmSephiroth);
				else if (!instance_exists(obj_LeftArm0Sephiroth))
				instance_create_layer(xpos, ypos, "Under_Instance", obj_LeftArm0Sephiroth);
				else if (!instance_exists(obj_RightLegSephiroth))
				instance_create_layer(xpos, ypos, "Under_Instance", obj_RightLegSephiroth);
				else if (!instance_exists(obj_LeftLeg0Sephiroth))
				instance_create_layer(xpos, ypos, "Under_Instance", obj_LeftLeg0Sephiroth);
				else if (!instance_exists(obj_Other100Sephiroth))
				instance_create_layer(xpos, ypos, "Under_Instance", obj_Other100Sephiroth);
				else if (!instance_exists(obj_Other200Sephiroth))
				instance_create_layer(xpos, ypos, "Under_Instance", obj_Other200Sephiroth);
				else if (!instance_exists(obj_Other300Sephiroth))
				instance_create_layer(xpos, ypos, "Under_Instance", obj_Other300Sephiroth);
				else if (!instance_exists(obj_Head0000Sephiroth))
				instance_create_layer(xpos, ypos, "Under_Instance", obj_Head0000Sephiroth);
				break;
			
				case 2:
				if (!instance_exists(obj_LeftArm0Sephiroth))
				instance_create_layer(xpos, ypos, "Under_Instance", obj_LeftArm0Sephiroth);
				else if (!instance_exists(obj_RightLegSephiroth))
				instance_create_layer(xpos, ypos, "Under_Instance", obj_RightLegSephiroth);
				else if (!instance_exists(obj_LeftLeg0Sephiroth))
				instance_create_layer(xpos, ypos, "Under_Instance", obj_LeftLeg0Sephiroth);
				else if (!instance_exists(obj_Other100Sephiroth))
				instance_create_layer(xpos, ypos, "Under_Instance", obj_Other100Sephiroth);
				else if (!instance_exists(obj_Other200Sephiroth))
				instance_create_layer(xpos, ypos, "Under_Instance", obj_Other200Sephiroth);
				else if (!instance_exists(obj_Other300Sephiroth))
				instance_create_layer(xpos, ypos, "Under_Instance", obj_Other300Sephiroth);
				else if (!instance_exists(obj_Head0000Sephiroth))
				instance_create_layer(xpos, ypos, "Under_Instance", obj_Head0000Sephiroth);
				else if (!instance_exists(obj_RightArmSephiroth))
				instance_create_layer(xpos, ypos, "Under_Instance", obj_RightArmSephiroth);
				break;
			
				case 3:
				if (!instance_exists(obj_RightLegSephiroth))
				instance_create_layer(xpos, ypos, "Under_Instance", obj_RightLegSephiroth);
				else if (!instance_exists(obj_LeftLeg0Sephiroth))
				instance_create_layer(xpos, ypos, "Under_Instance", obj_LeftLeg0Sephiroth);
				else if (!instance_exists(obj_Other100Sephiroth))
				instance_create_layer(xpos, ypos, "Under_Instance", obj_Other100Sephiroth);
				else if (!instance_exists(obj_Other200Sephiroth))
				instance_create_layer(xpos, ypos, "Under_Instance", obj_Other200Sephiroth);
				else if (!instance_exists(obj_Other300Sephiroth))
				instance_create_layer(xpos, ypos, "Under_Instance", obj_Other300Sephiroth);
				else if (!instance_exists(obj_Head0000Sephiroth))
				instance_create_layer(xpos, ypos, "Under_Instance", obj_Head0000Sephiroth);
				else if (!instance_exists(obj_RightArmSephiroth))
				instance_create_layer(xpos, ypos, "Under_Instance", obj_RightArmSephiroth);
				else if (!instance_exists(obj_LeftArm0Sephiroth))
				instance_create_layer(xpos, ypos, "Under_Instance", obj_LeftArm0Sephiroth);
				break;
			
				case 4:
				if (!instance_exists(obj_LeftLeg0Sephiroth))
				instance_create_layer(xpos, ypos, "Under_Instance", obj_LeftLeg0Sephiroth);
				else if (!instance_exists(obj_Other100Sephiroth))
				instance_create_layer(xpos, ypos, "Under_Instance", obj_Other100Sephiroth);
				else if (!instance_exists(obj_Other200Sephiroth))
				instance_create_layer(xpos, ypos, "Under_Instance", obj_Other200Sephiroth);
				else if (!instance_exists(obj_Other300Sephiroth))
				instance_create_layer(xpos, ypos, "Under_Instance", obj_Other300Sephiroth);
				else if (!instance_exists(obj_Head0000Sephiroth))
				instance_create_layer(xpos, ypos, "Under_Instance", obj_Head0000Sephiroth);
				else if (!instance_exists(obj_RightArmSephiroth))
				instance_create_layer(xpos, ypos, "Under_Instance", obj_RightArmSephiroth);
				else if (!instance_exists(obj_LeftArm0Sephiroth))
				instance_create_layer(xpos, ypos, "Under_Instance", obj_LeftArm0Sephiroth);
				else if (!instance_exists(obj_RightLegSephiroth))
				instance_create_layer(xpos, ypos, "Under_Instance", obj_RightLegSephiroth);
				break;
			
				case 5:
				if (!instance_exists(obj_Other100Sephiroth))
				instance_create_layer(xpos, ypos, "Under_Instance", obj_Other100Sephiroth);
				else if (!instance_exists(obj_Other200Sephiroth))
				instance_create_layer(xpos, ypos, "Under_Instance", obj_Other200Sephiroth);
				else if (!instance_exists(obj_Other300Sephiroth))
				instance_create_layer(xpos, ypos, "Under_Instance", obj_Other300Sephiroth);
				else if (!instance_exists(obj_Head0000Sephiroth))
				instance_create_layer(xpos, ypos, "Under_Instance", obj_Head0000Sephiroth);
				else if (!instance_exists(obj_RightArmSephiroth))
				instance_create_layer(xpos, ypos, "Under_Instance", obj_RightArmSephiroth);
				else if (!instance_exists(obj_LeftArm0Sephiroth))
				instance_create_layer(xpos, ypos, "Under_Instance", obj_LeftArm0Sephiroth);
				else if (!instance_exists(obj_RightLegSephiroth))
				instance_create_layer(xpos, ypos, "Under_Instance", obj_RightLegSephiroth);
				else if (!instance_exists(obj_LeftLeg0Sephiroth))
				instance_create_layer(xpos, ypos, "Under_Instance", obj_LeftLeg0Sephiroth);
				break;
			
				case 6:
				if (!instance_exists(obj_Other200Sephiroth))
				instance_create_layer(xpos, ypos, "Under_Instance", obj_Other200Sephiroth);
				else if (!instance_exists(obj_Other300Sephiroth))
				instance_create_layer(xpos, ypos, "Under_Instance", obj_Other300Sephiroth);
				else if (!instance_exists(obj_Head0000Sephiroth))
				instance_create_layer(xpos, ypos, "Under_Instance", obj_Head0000Sephiroth);
				else if (!instance_exists(obj_RightArmSephiroth))
				instance_create_layer(xpos, ypos, "Under_Instance", obj_RightArmSephiroth);
				else if (!instance_exists(obj_LeftArm0Sephiroth))
				instance_create_layer(xpos, ypos, "Under_Instance", obj_LeftArm0Sephiroth);
				else if (!instance_exists(obj_RightLegSephiroth))
				instance_create_layer(xpos, ypos, "Under_Instance", obj_RightLegSephiroth);
				else if (!instance_exists(obj_LeftLeg0Sephiroth))
				instance_create_layer(xpos, ypos, "Under_Instance", obj_LeftLeg0Sephiroth);
				else if (!instance_exists(obj_Other100Sephiroth))
				instance_create_layer(xpos, ypos, "Under_Instance", obj_Other100Sephiroth);
				break;
			
				case 7:
				if (!instance_exists(obj_Other300Sephiroth))
				instance_create_layer(xpos, ypos, "Under_Instance", obj_Other300Sephiroth);
				else if (!instance_exists(obj_Head0000Sephiroth))
				instance_create_layer(xpos, ypos, "Under_Instance", obj_Head0000Sephiroth);
				else if (!instance_exists(obj_RightArmSephiroth))
				instance_create_layer(xpos, ypos, "Under_Instance", obj_RightArmSephiroth);
				else if (!instance_exists(obj_LeftArm0Sephiroth))
				instance_create_layer(xpos, ypos, "Under_Instance", obj_LeftArm0Sephiroth);
				else if (!instance_exists(obj_RightLegSephiroth))
				instance_create_layer(xpos, ypos, "Under_Instance", obj_RightLegSephiroth);
				else if (!instance_exists(obj_LeftLeg0Sephiroth))
				instance_create_layer(xpos, ypos, "Under_Instance", obj_LeftLeg0Sephiroth);
				else if (!instance_exists(obj_Other100Sephiroth))
				instance_create_layer(xpos, ypos, "Under_Instance", obj_Other100Sephiroth);
				else if (!instance_exists(obj_Other200Sephiroth))
				instance_create_layer(xpos, ypos, "Under_Instance", obj_Other200Sephiroth);
				break;
			}
			break;
		
			case "Tim":
			instance_create_layer(xpos, ypos, "Under_Instance", obj_Head0000Tim);
			break;
		
			case "Deathro":
			instance_create_layer(xpos, ypos, "Under_Instance", obj_Head0000Deathro);
			break;
		}
	}
	else
	{
		ranU = irandom_range(0,40)
		switch (ranU)
		{
			case 0:
			instance_create_layer(xpos, ypos, "Under_Instance", obj_Head0000Ali);
			break;
		
			case 1:
			instance_create_layer(xpos, ypos, "Under_Instance", obj_Head0000Bronze);
			break;
		
			case 2:
			instance_create_layer(xpos, ypos, "Under_Instance", obj_Head0000Butler);
			break;
		
			case 3:
			instance_create_layer(xpos, ypos, "Under_Instance", obj_Head0000Deathro);
			break;
		
			case 4:
			instance_create_layer(xpos, ypos, "Under_Instance", obj_Head0000Default);
			break;
		
			case 5:
			instance_create_layer(xpos, ypos, "Under_Instance", obj_Head0000Grievous);
			break;
		
			case 6:
			instance_create_layer(xpos, ypos, "Under_Instance", obj_Head0000Sephiroth);
			break;
		
			case 7:
			instance_create_layer(xpos, ypos + offset, "Under_Instance", obj_Head0000Spaceman);
			break;
		
			case 8:
			instance_create_layer(xpos, ypos, "Under_Instance", obj_Head0000Tim);
			break;
		
			case 9:
			instance_create_layer(xpos, ypos, "Under_Instance", obj_RightArmAli);
			break;
		
			case 10:
			instance_create_layer(xpos, ypos, "Under_Instance", obj_RightArmBronze);
			break;
		
			case 11:
			instance_create_layer(xpos, ypos, "Under_Instance", obj_RightArmButler);
			break;
		
			case 12:
			instance_create_layer(xpos, ypos, "Under_Instance", obj_RightArmDefault);
			break;
		
			case 13:
			instance_create_layer(xpos, ypos, "Under_Instance", obj_RightArmGrievous);
			break;
		
			case 14:
			instance_create_layer(xpos, ypos, "Under_Instance", obj_RightLegAli);
			break;
		
			case 15:
			instance_create_layer(xpos, ypos, "Under_Instance", obj_RightArmSephiroth);
			break;
		
			case 16:
			instance_create_layer(xpos, ypos, "Under_Instance", obj_RightArmSpaceman);
			break;
		
			case 17:
			instance_create_layer(xpos, ypos, "Under_Instance", obj_LeftArm0Ali);
			break;
		
			case 18:
			instance_create_layer(xpos, ypos, "Under_Instance", obj_LeftArm0Bronze);
			break;
	
			case 19:
			instance_create_layer(xpos, ypos, "Under_Instance", obj_LeftArm0Butler);
			break;
		
			case 20:
			instance_create_layer(xpos, ypos, "Under_Instance", obj_LeftArm0Default);
			break;
		
			case 21:
			instance_create_layer(xpos, ypos, "Under_Instance", obj_LeftArm0Grievous);
			break;
		
			case 22:
			instance_create_layer(xpos, ypos, "Under_Instance", obj_LeftArm0Sephiroth);
			break;
		
			case 23:
			instance_create_layer(xpos, ypos, "Under_Instance", obj_LeftArm0Spaceman);
			break;
		
			case 24:
			instance_create_layer(xpos, ypos, "Under_Instance", obj_RightLegBronze);
			break;
		
			case 25:
			instance_create_layer(xpos, ypos, "Under_Instance", obj_RightLegDefault);
			break;
		
			case 26:
			instance_create_layer(xpos, ypos, "Under_Instance", obj_RightLegGrievous);
			break;
		
			case 27:
			instance_create_layer(xpos, ypos, "Under_Instance", obj_RightLegSephiroth);
			break;
		
			case 28:
			instance_create_layer(xpos, ypos, "Under_Instance", obj_RightLegSpaceman);
			break;
		
			case 29:
			instance_create_layer(xpos, ypos, "Under_Instance", obj_LeftLeg0Ali);
			break;
		
			case 30:
			instance_create_layer(xpos, ypos, "Under_Instance", obj_LeftLeg0Bronze);
			break;
		
			case 31:
			instance_create_layer(xpos, ypos, "Under_Instance", obj_LeftLeg0Butler);
			break;
		
			case 32:
			instance_create_layer(xpos, ypos, "Under_Instance", obj_LeftLeg0Default);
			break;
		
			case 33:
			instance_create_layer(xpos, ypos, "Under_Instance", obj_LeftLeg0Grievous);
			break;
		
			case 34:
			instance_create_layer(xpos, ypos, "Under_Instance", obj_LeftLeg0Sephiroth);
			break;
		
			case 35:
			instance_create_layer(xpos, ypos, "Under_Instance", obj_LeftLeg0Spaceman);
			break;
		
			case 36:
			instance_create_layer(xpos, ypos, "Under_Instance", obj_Other100Grievous);
			break;
		
			case 37:
			instance_create_layer(xpos, ypos, "Under_Instance", obj_Other100Sephiroth);
			break;
		
			case 38:
			instance_create_layer(xpos, ypos, "Under_Instance", obj_Other200Grievous);
			break;
		
			case 39:
			instance_create_layer(xpos, ypos, "Under_Instance", obj_Other200Sephiroth);
			break;
		
			case 40:
			instance_create_layer(xpos, ypos, "Under_Instance", obj_Other300Sephiroth);
			break;
		}
	}
}
alarm[0] = room_speed;