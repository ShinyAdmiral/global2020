{
    "id": "42f74d97-2850-4c9e-8986-2e5da3e4c199",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_loss",
    "eventList": [
        {
            "id": "5645a3f8-c8cf-42e2-8889-5680c682c3ec",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "42f74d97-2850-4c9e-8986-2e5da3e4c199"
        },
        {
            "id": "446b11f0-0278-465a-84f0-4c90411bef04",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "42f74d97-2850-4c9e-8986-2e5da3e4c199"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "934c5aed-6bb6-476a-9a41-db2187a5051f",
    "visible": true
}