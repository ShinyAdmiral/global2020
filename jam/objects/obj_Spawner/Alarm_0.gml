timer = STARTING_TIME / sqrt(difficulty);

show_debug_message(timer)

for (var i = 0; i < (difficulty+STARTING_AMMOUNT); i ++)
{
	tier = (difficulty + 2);
	tier = clamp(tier,1,6);
	ran = irandom_range(1, tier)
	
	ran_x = irandom_range(x, x + 50)
	ran_y = irandom_range(y - 40, y + 40)
	
	if (ran <= 3)
	{
		instance_create_layer(ran_x, ran_y, "Battle", obj_battleEnemy2)
	}
	else if (ran <= 5)
	{
		instance_create_layer(ran_x, ran_y, "Battle", obj_battleEnemy1)
	}
	else 
	{
		instance_create_layer(ran_x, ran_y, "Battle", obj_battleEnemy3)
	}
}

alarm[0] = room_speed * timer;