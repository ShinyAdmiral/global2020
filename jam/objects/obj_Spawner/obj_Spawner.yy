{
    "id": "8d5c752f-8978-4c34-8070-3336c69968b3",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_Spawner",
    "eventList": [
        {
            "id": "38654945-fb2f-43e5-9476-7fdec85e4600",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "8d5c752f-8978-4c34-8070-3336c69968b3"
        },
        {
            "id": "5a931c8d-7c28-4e2d-abe7-8a2d8ce4d69e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "8d5c752f-8978-4c34-8070-3336c69968b3"
        },
        {
            "id": "a8825394-9d04-4ff7-b187-4713e86cb940",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 1,
            "eventtype": 2,
            "m_owner": "8d5c752f-8978-4c34-8070-3336c69968b3"
        },
        {
            "id": "695287d1-5bee-4b98-8bf8-95a24b7731be",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "8d5c752f-8978-4c34-8070-3336c69968b3"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}