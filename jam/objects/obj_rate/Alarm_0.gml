if (ave_score <= 1)
sprite_index = spr_Perfect;
else if (ave_score <= 2)
sprite_index = spr_Great;
else if (ave_score <= 3)
sprite_index = spr_Good;
else if (ave_score <= 4)
sprite_index = spr_OK;
else if (ave_score <= 5)
sprite_index = spr_Bad;
else
{
	sprite_index = spr_Miss;
	audio_play_sound(RejectedClick,10,false);
}
image_xscale /=3;
image_yscale /=3;

image_speed = 0;

alarm[1] = 10;