{
    "id": "e0aa9fcb-18af-4aac-a8b2-349439274b59",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_rate",
    "eventList": [
        {
            "id": "b4c36934-7296-43db-835a-5695290dda1d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "e0aa9fcb-18af-4aac-a8b2-349439274b59"
        },
        {
            "id": "96e84ec0-4ca6-40e3-bfde-a1a46d7151b3",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 7,
            "eventtype": 7,
            "m_owner": "e0aa9fcb-18af-4aac-a8b2-349439274b59"
        },
        {
            "id": "e17f4631-90cc-4fcd-8fd3-2247006cb8f6",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "e0aa9fcb-18af-4aac-a8b2-349439274b59"
        },
        {
            "id": "28af6a5c-080b-4a55-80d8-2f90d1911191",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "e0aa9fcb-18af-4aac-a8b2-349439274b59"
        },
        {
            "id": "1b541c36-2237-4418-a7c7-3a6bd9efe412",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 1,
            "eventtype": 2,
            "m_owner": "e0aa9fcb-18af-4aac-a8b2-349439274b59"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": [
        {
            "id": "2d1a6e1b-3264-49fe-8aab-55b633804299",
            "modelName": "GMObjectProperty",
            "mvc": "1.0",
            "listItems": null,
            "multiselect": false,
            "rangeEnabled": false,
            "rangeMax": 10,
            "rangeMin": 0,
            "resourceFilter": 1023,
            "value": "0",
            "varName": "ave_score",
            "varType": 0
        }
    ],
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}