vspeed = clamp(vspeed, -0.5, 0)

if (start)
{
	vspeed -=0.05;
	if (finish)
	{
		image_alpha -= .1;
	}
}

if (image_alpha <= 0)
{
	instance_destroy(self);
}