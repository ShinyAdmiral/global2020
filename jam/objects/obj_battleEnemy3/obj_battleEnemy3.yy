{
    "id": "4c4dda49-bd52-4227-9c94-c9a3826f88c9",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_battleEnemy3",
    "eventList": [
        
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": [
        {
            "id": "77d0a27c-0300-4640-8283-5d7a25957d28",
            "modelName": "GMOverriddenProperty",
            "mvc": "1.0",
            "objectId": "ab0ddc07-19b6-4910-aecf-70b4be6d004e",
            "propertyId": "53f89a47-09fa-4ea1-8532-3107149acb2b",
            "value": "obj_enemySweep"
        },
        {
            "id": "76238409-32b0-44d8-9c59-56a036d71494",
            "modelName": "GMOverriddenProperty",
            "mvc": "1.0",
            "objectId": "ab0ddc07-19b6-4910-aecf-70b4be6d004e",
            "propertyId": "8321e759-916a-4133-963a-c007eb4a82ff",
            "value": "5"
        },
        {
            "id": "020495cc-0159-4f3d-bd77-1806ace830fd",
            "modelName": "GMOverriddenProperty",
            "mvc": "1.0",
            "objectId": "ab0ddc07-19b6-4910-aecf-70b4be6d004e",
            "propertyId": "2c1d3c90-f4e7-4723-b1d3-7136726e02bc",
            "value": "30"
        },
        {
            "id": "16195c5e-2ea1-48d1-bbdb-7cb12a35655f",
            "modelName": "GMOverriddenProperty",
            "mvc": "1.0",
            "objectId": "ab0ddc07-19b6-4910-aecf-70b4be6d004e",
            "propertyId": "79b81f97-cf32-4203-ae09-a7fe063fe016",
            "value": "16"
        },
        {
            "id": "4e310045-05d4-459e-8df0-1f2fe4847e96",
            "modelName": "GMOverriddenProperty",
            "mvc": "1.0",
            "objectId": "ab0ddc07-19b6-4910-aecf-70b4be6d004e",
            "propertyId": "30da5f80-42af-42fa-8ef5-29063f517884",
            "value": "0.2"
        }
    ],
    "parentObjectId": "ab0ddc07-19b6-4910-aecf-70b4be6d004e",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "252ae9d0-db8f-4552-9df6-9e02d15d9cd9",
    "visible": true
}