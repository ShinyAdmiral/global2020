//if(selected){ 
//	x = mouse_x + offsetX
//	y = mouse_y + offsetY
//}

if (started && !setup)
{
	xhere = x - (sprite_width/2);
	yhere = y - (sprite_height/2);

	if((head_pos_x != -1) && (head_pos_y != -1))
	{
		head_pos_x += xhere;
		head_pos_y += yhere;
		ammount_required++;
	}
	if((Rarm_pos_x != -1) && (Rarm_pos_y != -1))
	{
		Rarm_pos_x += xhere;
		Rarm_pos_y += yhere;
		ammount_required++;
	}
	if((Larm_pos_x != -1) && (Larm_pos_y != -1))
	{
		Larm_pos_x += xhere;
		Larm_pos_y += yhere;
		ammount_required++;
	}
	if((Rleg_pos_x != -1) && (Rleg_pos_y != -1))
	{
		Rleg_pos_x += xhere;
		Rleg_pos_y += yhere;
		ammount_required++;
	}
	if((Lleg_pos_x != -1) && (Lleg_pos_y != -1))
	{
		Lleg_pos_x += xhere;
		Lleg_pos_y += yhere;
		ammount_required++;
	}
	if((other1_pos_x != -1) && (other1_pos_y != -1))
	{
		other1_pos_x += xhere;
		other1_pos_x += yhere;
		ammount_required++;
	}
	if((other2_pos_x != -1) && (other2_pos_y != -1))
	{
		other2_pos_x += xhere;
		other2_pos_x += yhere;
		ammount_required++;
	}
	if((other3_pos_x != -1) && (other3_pos_y != -1))
	{
		other3_pos_x += xhere;
		other3_pos_x += yhere;
		ammount_required++;
	}
	setup = true;
	instance_create_layer(x, y, "Instances", obj_ScoreCalc);
}

if(!started)
{
	if (x > obj_UILeft.x + 180)
	{
		instance_destroy(self);
	}

	if (!start)
	{
		if (speed = 0)
		{
			selected = true;
		}
		else
		{
			selected = false;
		}
	}
	if (start)
	{
		mp_potential_step(obj_UILeft.x + 64, 70, 4, false);
		if (x = obj_UILeft.x + 64)
		{
			started = true;
		}
	}
}

//show_debug_message("Head Pos: " + string(head_pos_x) +", " +string(head_pos_y))
//	show_debug_message("Mouse Pos: " + string(mouse_x) +", " +string(mouse_y))