//offsetX = x-mouse_x
//offsetY = y-mouse_y
//selected = true;
if(!start)
{
	if (selected)
	{
		str = object_get_name(object_index);
		strc = string_copy(str,13, string_length(str));
		
		if (strc != "Default")
		{
			instance_destroy(obj_Chasis00Default);
		}
		if (strc != "Grievous")
		{
			instance_destroy(obj_Chasis00Grievous);
		}
		if (strc != "Bronze")
		{
			instance_destroy(obj_Chasis00Bronze);
		}
		if (strc != "Ali")
		{
			instance_destroy(obj_Chasis00Ali);
		}
		if (strc != "Butler")
		{
			instance_destroy(obj_Chasis00Butler);
		}
		if (strc != "Spaceman")
		{
			instance_destroy(obj_Chasis00Spaceman);
		}
		if (strc != "Sephiroth")
		{
			instance_destroy(obj_Chasis00Sephiroth);
		}
		if (strc != "Tim")
		{
			instance_destroy(obj_Chasis00Tim);
		}
		if (strc != "Deathro")
		{
			instance_destroy(obj_Chasis00Deathro);
		}
		
		
		start = true;
		obj_ChasisSpawner.stop = true;
		obj_PartSpawner.start = true;
		obj_PartSpawner.str = object_get_name(object_index);
		image_xscale = 1;
		image_yscale = 1;
		audio_play_sound(ChassisPick,10,false);
	}
}