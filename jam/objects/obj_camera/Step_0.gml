if (!instance_exists(obj_loss))
{
	if (mag)
	{
		ran_x = random_range(-shake,shake);
		ran_y = random_range(-shake,shake);
	
		x = x + ran_x;
		y = y + ran_y;
	}
	else
	{
		x = reset_x;
		y = reset_y;
	}
}
else
{
	x = reset_x;
	y = reset_y;
}