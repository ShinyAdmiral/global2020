{
    "id": "43a26d25-42d8-4106-a3f0-85d31bf0c7cc",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_RighLeftDefault",
    "eventList": [
        {
            "id": "a648659b-6838-4a30-bb49-d5c3c0d8ad05",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "43a26d25-42d8-4106-a3f0-85d31bf0c7cc"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "8b1258c7-0f93-47cc-b0c4-3c5b85b89e57",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "0d3951cf-1cee-43d1-84e1-2c730ffb9ac4",
    "visible": true
}