#macro max_distance 5
#macro conveyer_speed .25

image_xscale = .5;
image_yscale = .5;

sound = false;

selected = false;
counted = false;
bolted = false;
bolt_part_score = 0;
bolt_chasis_score = 0;
part_chasis_score = 0;

average_score = -1;