if (oBoltMode.active = 0)
{
	if((!place_meeting(x,y,obj_conveyer)) || (x > obj_conveyer.x))
	{
		if (!bolted)
		{
			offsetX = x-mouse_x;
			offsetY = y-mouse_y;
			selected = true;
			if (!sound)
			{
				audio_sound_pitch(IMPACT_Metal_Cling_Clean_mono, random_range(0.8,1.2))
				audio_play_sound(IMPACT_Metal_Cling_Clean_mono,10,false)
				sound = true
			}
		}	
	}
}
else
selected = false;