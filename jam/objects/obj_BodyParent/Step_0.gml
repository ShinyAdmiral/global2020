if (!counted)
{
	x = clamp(x, obj_UILeft.x, obj_UILeft.x + 128);
	y = clamp(y, obj_UILeft.y, obj_UILeft.y + 180);
}
else
{
	if (x > obj_UILeft.x + 260)
	{
		instance_destroy(self);
	}
}

if(selected){ 
	x = clamp(mouse_x + offsetX, obj_UILeft.x, obj_UILeft.x + 128);
	y = clamp(mouse_y + offsetY, obj_UILeft.y, obj_UILeft.y + 180);
}

if (place_meeting(x,y,obj_Bolt) && (!bolted) && place_meeting(x,y,obj_ChasisParent))
{
	show_debug_message("---------------New------------------")
	
	Screen_Shake(5);
	
	//calc precision from bolt to part
	bolt_part_score = Bolt_to_Part();
	show_debug_message("bolt_part_score: " + string(bolt_part_score));
	
	//calc precision from bolt to chasis
	bolt_chasis_score = Bolt_to_Chasis();
	show_debug_message("bolt_chasis_score: " + string(bolt_chasis_score));
	
	//calc precision from limb to chasis
	part_chasis_score = Part_to_Chasis();
	show_debug_message("part_chasis_score: " + string(part_chasis_score));
	
	//calculate average
	average_score = (bolt_part_score + bolt_chasis_score + part_chasis_score) / 3;
	
	average_score = floor(Check_Compatibility(average_score));
	
	instance_create_layer(obj_Bolt.x, obj_Bolt.y,"Bolts",obj_BoltFixed);
	instance_destroy(obj_Bolt);
	show_debug_message("average: " + string(average_score));
	
	var a = instance_create_layer(mouse_x, mouse_y,"Battle",obj_rate);
	a.ave_score = average_score;
	
	obj_ScoreCalc.part[instance_number(obj_BoltFixed)] = id;
	bolted = true;
}

if (bolted && !counted)
{
	show_debug_message("ID " + string(id))
	Send_Score(average_score);
	show_debug_message("Bolt Fixed?" + string(instance_number(obj_BoltFixed)))
	counted = true;
}

//movement of conveyer
if (!counted)
{
	if (place_meeting(x,y,obj_UILeft))
	{
		image_xscale = 1;
		image_yscale = 1;
		layer = layer_get_id("Instances")
		hspeed = 0;
	}
	else
	{
		image_xscale = .5;
		image_yscale = .5;
		if(place_meeting(x,y,obj_conveyer))
		{
			hspeed = conveyer_speed;
			layer = layer_get_id("Under_Instance")
		}
		else
		{
			hspeed = 0;
			layer = layer_get_id("Instances")
		
			if(place_meeting(x,y,obj_pit) && !selected)
			{
				instance_destroy(self);
			}
		}
	}
}