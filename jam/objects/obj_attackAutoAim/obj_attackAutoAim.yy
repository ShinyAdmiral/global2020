{
    "id": "61a6d93a-3484-41d2-b38d-555cad46c91e",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_attackAutoAim",
    "eventList": [
        {
            "id": "04a82c1b-d375-41da-a7f8-337496fa390d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "61a6d93a-3484-41d2-b38d-555cad46c91e"
        },
        {
            "id": "4bb9d49b-8fe3-4eec-8056-7b6f43850471",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "61a6d93a-3484-41d2-b38d-555cad46c91e"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": [
        {
            "id": "60ce3e3f-2be1-43f8-8758-0d3534aeb086",
            "modelName": "GMOverriddenProperty",
            "mvc": "1.0",
            "objectId": "933839b8-24f5-4e0d-a2d1-a01d1293106c",
            "propertyId": "c10c8a12-58c0-4e31-aceb-6f08dc8be41f",
            "value": "5"
        }
    ],
    "parentObjectId": "933839b8-24f5-4e0d-a2d1-a01d1293106c",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": [
        {
            "id": "5e596ba6-9d39-45d6-8ee6-a6780547e250",
            "modelName": "GMObjectProperty",
            "mvc": "1.0",
            "listItems": null,
            "multiselect": false,
            "rangeEnabled": false,
            "rangeMax": 10,
            "rangeMin": 0,
            "resourceFilter": 1023,
            "value": "2",
            "varName": "damage",
            "varType": 0
        },
        {
            "id": "28147946-910a-4199-8da1-0213e2f3bad2",
            "modelName": "GMObjectProperty",
            "mvc": "1.0",
            "listItems": null,
            "multiselect": false,
            "rangeEnabled": false,
            "rangeMax": 10,
            "rangeMin": 0,
            "resourceFilter": 1023,
            "value": "15",
            "varName": "spawnOffset",
            "varType": 0
        }
    ],
    "solid": false,
    "spriteId": "54afa862-f8af-4f16-bab3-7719d59fd1ad",
    "visible": true
}