part[0] = obj_ChasisParent;
part[1] = obj_ChasisParent;
part[2] = obj_ChasisParent;

for(var i = 0; i < 3; i++)
{
	ran = irandom_range(0,skill);
		
	if (ran < 4 && !instance_exists(obj_Chasis00Default))
	{
		part[i] = instance_create_layer(obj_UILeft.x-80,70,"Chasis", obj_Chasis00Default);
	}
	//spawned 
	else if (ran < 7 && !instance_exists(obj_Chasis00Bronze))
	{
		part[i] = instance_create_layer(obj_UILeft.x-80,70,"Chasis", obj_Chasis00Bronze);
	}
	else if (ran < 9 )
	{
		part[i] = instance_create_layer(obj_UILeft.x-80,70,"Chasis", obj_Chasis00Grievous);
	}
	else if (ran < 12 && !instance_exists(obj_Chasis00Butler))
	{
		part[i] = instance_create_layer(obj_UILeft.x-80,70,"Chasis", obj_Chasis00Butler);
	}
	else if (ran < 15 && !instance_exists(obj_Chasis00Ali))
	{
		part[i] = instance_create_layer(obj_UILeft.x-80,70,"Chasis", obj_Chasis00Ali);
	}
	else if (ran < 17 && !instance_exists(obj_Chasis00Spaceman))
	{
		part[i] = instance_create_layer(obj_UILeft.x-80,70,"Chasis", obj_Chasis00Spaceman);
	}
	else if (ran < 21 && !instance_exists(obj_Chasis00Tim))
	{
		part[i] = instance_create_layer(obj_UILeft.x-80,70,"Chasis", obj_Chasis00Tim);
	}
	else if (ran < 22 && !instance_exists(obj_Chasis00Deathro))
	{
		part[i] = instance_create_layer(obj_UILeft.x-80,70,"Chasis", obj_Chasis00Deathro);
	}
	else if (ran < 23 && !instance_exists(obj_Chasis00Bronze))
	{
		part[i] = instance_create_layer(obj_UILeft.x-80,70,"Chasis", obj_Chasis00Sephiroth);
	}
	else
	{
		part[i] = instance_create_layer(obj_UILeft.x-80,70,"Chasis", obj_Chasis00Default);
	}
		
	part[i].hspeed = 5;
}
