str = object_get_name(object_index);
str = string_copy(str,5,8);
found = false;
distance = 0;
percentage = 0;
ratio = 100/5;

if(str == "Head0000")
{
	distance = sqrt(sqr(x-obj_ChasisParent.head_pos_x)+sqr(y-obj_ChasisParent.head_pos_y));
	percentage = distance/max_distance * 100;
}
else if(str == "RightArm")
{
	distance = sqrt(sqr(x-obj_ChasisParent.Rarm_pos_x)+sqr(y-obj_ChasisParent.Rarm_pos_y));
	percentage = distance/max_distance * 100;
}
else if(str == "LeftArm0")
{
	distance = sqrt(sqr(x-obj_ChasisParent.Larm_pos_x)+sqr(y-obj_ChasisParent.Larm_pos_y));
	percentage = distance/max_distance * 100;
}
else if(str == "RightLeg")
{
	distance = sqrt(sqr(x-obj_ChasisParent.Rleg_pos_x)+sqr(y-obj_ChasisParent.Rleg_pos_y));
	percentage = distance/max_distance * 100;
}
else if(str == "LeftLeg0")
{
	distance = sqrt(sqr(x-obj_ChasisParent.Lleg_pos_x)+sqr(y-obj_ChasisParent.Lleg_pos_y));
	percentage = distance/max_distance * 100;
}
else if(str == "Other100")
{
	distance = sqrt(sqr(obj_Bolt.x-obj_ChasisParent.other1_pos_x)+sqr(obj_Bolt.y-obj_ChasisParent.other1_pos_y));
	percentage = distance/max_distance * 100;
}
else if(str == "Other200")
{
	distance = sqrt(sqr(obj_Bolt.x-obj_ChasisParent.other2_pos_x)+sqr(obj_Bolt.y-obj_ChasisParent.other2_pos_y));
	percentage = distance/max_distance * 100;
}
else if(str == "Other300")
{
	distance = sqrt(sqr(obj_Bolt.x-obj_ChasisParent.other3_pos_x)+sqr(obj_Bolt.y-obj_ChasisParent.other3_pos_y));
	percentage = distance/max_distance * 100;
}
else
{
	show_debug_message("Naming Convention not Consistent");
	show_error("Naming Convention not Consistent", true);
}

show_debug_message(distance)
show_debug_message(str)

var part_to_c = 6;
for (var a = 1; a<6; a++)
{
	if (!found)
	{
		if (percentage < (ratio * a))
		{
			part_to_c = a;
			found = true;
			show_debug_message("Here")
		}
	}
}

return part_to_c;