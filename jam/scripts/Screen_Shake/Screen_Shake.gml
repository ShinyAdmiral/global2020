/// @desc ScreenShake(Duration)
/// @arg Duration Determines how long the object lasts for

with (obj_camera)
{
	mag = true;
	alarm[0] = argument0;
}