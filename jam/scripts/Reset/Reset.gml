obj_ScoreCalc.head_score = 0;
obj_ScoreCalc.Larm_score = 0;
obj_ScoreCalc.Rarm_score = 0;
obj_ScoreCalc.Lleg_score = 0;
obj_ScoreCalc.Rleg_score = 0;
obj_ScoreCalc.other1_score = 0;
obj_ScoreCalc.other2_score = 0;
obj_ScoreCalc.other3_score = 0;

obj_ScoreCalc.part[1] = obj_BodyParent;
obj_ScoreCalc.part[2] = obj_BodyParent;
obj_ScoreCalc.part[3] = obj_BodyParent;
obj_ScoreCalc.part[4] = obj_BodyParent;
obj_ScoreCalc.part[5] = obj_BodyParent;
obj_ScoreCalc.part[6] = obj_BodyParent;
obj_ScoreCalc.part[7] = obj_BodyParent;
obj_ScoreCalc.part[8] = obj_BodyParent;

obj_ScoreCalc.bolt[1] = obj_BoltFixed;
obj_ScoreCalc.bolt[2] = obj_BoltFixed;
obj_ScoreCalc.bolt[3] = obj_BoltFixed;
obj_ScoreCalc.bolt[4] = obj_BoltFixed;
obj_ScoreCalc.bolt[5] = obj_BoltFixed;
obj_ScoreCalc.bolt[6] = obj_BoltFixed;
obj_ScoreCalc.bolt[7] = obj_BoltFixed;
obj_ScoreCalc.bolt[8] = obj_BoltFixed;

obj_ScoreCalc.complete = false;
obj_ScoreCalc.go = false;