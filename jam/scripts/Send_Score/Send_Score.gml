str = object_get_name(object_index);
str = string_copy(str,5,8);

if(str == "Head0000")
{
	obj_ScoreCalc.head_score = argument0;
}
else if(str == "RightArm")
{
	obj_ScoreCalc.Rarm_score = argument0;
}
else if(str == "LeftArm0")
{
	obj_ScoreCalc.Larm_score = argument0;
}
else if(str == "RightLeg")
{
	obj_ScoreCalc.Rleg_score = argument0;
}
else if(str == "LeftLeg0")
{
	obj_ScoreCalc.Lleg_score = argument0;
}
else if(str == "Other100")
{
	obj_ScoreCalc.other1_score = argument0;
}
else if(str == "Other200")
{
	obj_ScoreCalc.other2_score = argument0;
}
else if(str == "Other300")
{
	obj_ScoreCalc.other3_score = argument0;
}
else
{
	show_debug_message("Naming Convention not Consistent");
	show_error("Naming Convention not Consistent", true);
}