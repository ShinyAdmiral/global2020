var bolt_to_part = 6;
found = false;

distance = sqrt(sqr(obj_Bolt.x-x)+sqr(obj_Bolt.y-y));
percentage = distance/max_distance * 100;
ratio = 100/5;

for (var i = 1; i<6; i++)
{
	if (!found)
	{
		if (percentage < (ratio * i))
		{
			bolt_to_part = i;
			found = true;
		}
	}
}

return bolt_to_part;