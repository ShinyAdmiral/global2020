{
    "id": "e387651e-191a-41ba-bda4-ba8b1bc409f8",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "Background_clone_clone_2",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 32,
    "bbox_left": 11,
    "bbox_right": 24,
    "bbox_top": 26,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "b0b4fb15-893e-45f5-b316-b330a35f8184",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e387651e-191a-41ba-bda4-ba8b1bc409f8",
            "compositeImage": {
                "id": "a14ef3c4-78b7-4678-93a5-e1559f6e2668",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b0b4fb15-893e-45f5-b316-b330a35f8184",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "64707f2e-b007-4b26-88db-098b16bd7a85",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b0b4fb15-893e-45f5-b316-b330a35f8184",
                    "LayerId": "1c563ca0-9096-4bb9-9c1a-f6ab76dcf595"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 220,
    "layers": [
        {
            "id": "1c563ca0-9096-4bb9-9c1a-f6ab76dcf595",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "e387651e-191a-41ba-bda4-ba8b1bc409f8",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 400,
    "xorig": 0,
    "yorig": 0
}