{
    "id": "0d3951cf-1cee-43d1-84e1-2c730ffb9ac4",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sRob1ArmR",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 61,
    "bbox_left": 0,
    "bbox_right": 184,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "f1ae502e-9e5b-45c7-8ddf-f2c67343b08a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0d3951cf-1cee-43d1-84e1-2c730ffb9ac4",
            "compositeImage": {
                "id": "811bad2c-d042-43b6-92f1-e901f7122ae6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f1ae502e-9e5b-45c7-8ddf-f2c67343b08a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c4dc424b-2438-4221-aeea-5b4760c54f30",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f1ae502e-9e5b-45c7-8ddf-f2c67343b08a",
                    "LayerId": "312afc37-4b7f-42ee-8b98-fda82dd103e4"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 62,
    "layers": [
        {
            "id": "312afc37-4b7f-42ee-8b98-fda82dd103e4",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "0d3951cf-1cee-43d1-84e1-2c730ffb9ac4",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 185,
    "xorig": 0,
    "yorig": 0
}