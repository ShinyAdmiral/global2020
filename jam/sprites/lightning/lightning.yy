{
    "id": "33ef0d51-96ef-445c-86fc-6a1eee7a6ecb",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "lightning",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 29,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "1e775faf-6868-4ab8-94a3-aab86d8e94e9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "33ef0d51-96ef-445c-86fc-6a1eee7a6ecb",
            "compositeImage": {
                "id": "b24552ab-baed-49e0-af3d-791b5351b36c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1e775faf-6868-4ab8-94a3-aab86d8e94e9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "59238213-ced5-4d19-94df-bec7b151a325",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1e775faf-6868-4ab8-94a3-aab86d8e94e9",
                    "LayerId": "febfd90f-5e73-4785-ab96-ab16ed23e6dc"
                }
            ]
        },
        {
            "id": "bba1f966-06ba-4c8f-972a-fa412e11599f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "33ef0d51-96ef-445c-86fc-6a1eee7a6ecb",
            "compositeImage": {
                "id": "fe662b3e-ae8d-48db-bd18-97fc24d19599",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bba1f966-06ba-4c8f-972a-fa412e11599f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9a9ff0ce-474f-415f-ad04-9438fe348540",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bba1f966-06ba-4c8f-972a-fa412e11599f",
                    "LayerId": "febfd90f-5e73-4785-ab96-ab16ed23e6dc"
                }
            ]
        },
        {
            "id": "4a0087d8-cd1f-4dbc-842f-eb3fa9042e6e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "33ef0d51-96ef-445c-86fc-6a1eee7a6ecb",
            "compositeImage": {
                "id": "f18e1b49-7bee-4d8c-ad16-feddd35a0636",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4a0087d8-cd1f-4dbc-842f-eb3fa9042e6e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "88c6d5a2-2e37-4ba9-b393-567380a14268",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4a0087d8-cd1f-4dbc-842f-eb3fa9042e6e",
                    "LayerId": "febfd90f-5e73-4785-ab96-ab16ed23e6dc"
                }
            ]
        },
        {
            "id": "8e8b03a1-3881-4e68-871b-8d74b91ae219",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "33ef0d51-96ef-445c-86fc-6a1eee7a6ecb",
            "compositeImage": {
                "id": "76bbe407-dbfc-4139-8134-87973c2fd0f2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8e8b03a1-3881-4e68-871b-8d74b91ae219",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6d607f58-d7c6-4ace-8ffb-fb50b417a9c7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8e8b03a1-3881-4e68-871b-8d74b91ae219",
                    "LayerId": "febfd90f-5e73-4785-ab96-ab16ed23e6dc"
                }
            ]
        },
        {
            "id": "68ed0f44-e491-4e24-aff9-edc678153a3d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "33ef0d51-96ef-445c-86fc-6a1eee7a6ecb",
            "compositeImage": {
                "id": "809f2d8b-7e2f-4de5-9c5f-1486120104cd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "68ed0f44-e491-4e24-aff9-edc678153a3d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b8dcc790-bfd2-4006-badf-59c3ce7ca97f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "68ed0f44-e491-4e24-aff9-edc678153a3d",
                    "LayerId": "febfd90f-5e73-4785-ab96-ab16ed23e6dc"
                }
            ]
        },
        {
            "id": "47a0474d-e94e-4f11-9b70-fe978ee64aa9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "33ef0d51-96ef-445c-86fc-6a1eee7a6ecb",
            "compositeImage": {
                "id": "6c455405-3eb0-4c34-9164-57dcd1195d02",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "47a0474d-e94e-4f11-9b70-fe978ee64aa9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "47727040-9441-44fa-b992-1ab4861832c2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "47a0474d-e94e-4f11-9b70-fe978ee64aa9",
                    "LayerId": "febfd90f-5e73-4785-ab96-ab16ed23e6dc"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "febfd90f-5e73-4785-ab96-ab16ed23e6dc",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "33ef0d51-96ef-445c-86fc-6a1eee7a6ecb",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 3,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 0,
    "yorig": 16
}