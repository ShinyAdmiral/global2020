{
    "id": "aefc553d-ba5a-4a4a-aecf-1f94dead19e6",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sRob1Chasis",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 248,
    "bbox_left": 26,
    "bbox_right": 225,
    "bbox_top": 12,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "af62dcb1-0160-4175-a413-23ef094ce5ee",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "aefc553d-ba5a-4a4a-aecf-1f94dead19e6",
            "compositeImage": {
                "id": "982ce61c-3b26-4fc4-bf03-3ffbb9e407fb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "af62dcb1-0160-4175-a413-23ef094ce5ee",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ec978e54-7b3c-4735-882b-d20aa8cd5fbd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "af62dcb1-0160-4175-a413-23ef094ce5ee",
                    "LayerId": "0c87ed32-9f4f-452f-9c1a-e368f1651b4d"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 256,
    "layers": [
        {
            "id": "0c87ed32-9f4f-452f-9c1a-e368f1651b4d",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "aefc553d-ba5a-4a4a-aecf-1f94dead19e6",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 256,
    "xorig": 0,
    "yorig": 0
}