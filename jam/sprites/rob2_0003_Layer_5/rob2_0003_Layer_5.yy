{
    "id": "5858630c-8e61-495e-83c2-44423a4d8e25",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "rob2_0003_Layer_5",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 38,
    "bbox_left": 0,
    "bbox_right": 24,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 0,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "25e97b55-4778-4edf-bc1a-baffe7651f2d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5858630c-8e61-495e-83c2-44423a4d8e25",
            "compositeImage": {
                "id": "2ed9c183-efdb-466e-9295-783492ea220b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "25e97b55-4778-4edf-bc1a-baffe7651f2d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "31123d66-4a77-465e-829d-924b9267b8e5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "25e97b55-4778-4edf-bc1a-baffe7651f2d",
                    "LayerId": "e44ae292-131c-4e85-a915-6ede50e56566"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 39,
    "layers": [
        {
            "id": "e44ae292-131c-4e85-a915-6ede50e56566",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "5858630c-8e61-495e-83c2-44423a4d8e25",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 25,
    "xorig": 22,
    "yorig": 4
}