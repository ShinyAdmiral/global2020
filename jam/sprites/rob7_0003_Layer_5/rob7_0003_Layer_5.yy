{
    "id": "221d7f51-8387-49c7-9777-b8ada01886d6",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "rob7_0003_Layer_5",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 17,
    "bbox_left": 0,
    "bbox_right": 16,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 0,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "48ec9036-9274-4b03-926c-120b10cd9382",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "221d7f51-8387-49c7-9777-b8ada01886d6",
            "compositeImage": {
                "id": "663d4edb-fa02-4c8d-9117-9def3cd44bcf",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "48ec9036-9274-4b03-926c-120b10cd9382",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9bd07141-a1c4-4157-aab3-de7ed8a144bc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "48ec9036-9274-4b03-926c-120b10cd9382",
                    "LayerId": "ac657179-593f-4ee3-9690-bac21219cd3c"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 18,
    "layers": [
        {
            "id": "ac657179-593f-4ee3-9690-bac21219cd3c",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "221d7f51-8387-49c7-9777-b8ada01886d6",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 17,
    "xorig": 3,
    "yorig": 3
}