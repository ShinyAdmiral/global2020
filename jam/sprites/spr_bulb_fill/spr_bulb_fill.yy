{
    "id": "79768d43-d27b-44f8-94b0-c3bb9cc4f6eb",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_bulb_fill",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 19,
    "bbox_left": 2,
    "bbox_right": 12,
    "bbox_top": 2,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "3323292e-1c65-426c-8b6e-8cb7c244387a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "79768d43-d27b-44f8-94b0-c3bb9cc4f6eb",
            "compositeImage": {
                "id": "94de30c3-ed87-4a88-ab51-a596689c16b6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3323292e-1c65-426c-8b6e-8cb7c244387a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "150885f1-c73c-4e57-a850-1369f2fba451",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3323292e-1c65-426c-8b6e-8cb7c244387a",
                    "LayerId": "5f252f2a-7c45-434c-91df-56599d687dee"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 28,
    "layers": [
        {
            "id": "5f252f2a-7c45-434c-91df-56599d687dee",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "79768d43-d27b-44f8-94b0-c3bb9cc4f6eb",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 3,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 15,
    "xorig": 0,
    "yorig": 0
}