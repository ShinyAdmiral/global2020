{
    "id": "f3e725e3-302b-4e0b-9b61-df306852b205",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "small_laser",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 9,
    "bbox_left": 0,
    "bbox_right": 14,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "2dda48a9-ceff-434e-8bec-6a53fa870746",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f3e725e3-302b-4e0b-9b61-df306852b205",
            "compositeImage": {
                "id": "9b7497b8-49d3-4093-bcf6-4c63b2e483ef",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2dda48a9-ceff-434e-8bec-6a53fa870746",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e7c2cd82-4892-45b1-aedb-5b346787d325",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2dda48a9-ceff-434e-8bec-6a53fa870746",
                    "LayerId": "f6623482-4e61-4cce-8c54-5cb9efde5b13"
                }
            ]
        },
        {
            "id": "870a9ccf-a384-4904-9da0-51e6e91470f0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f3e725e3-302b-4e0b-9b61-df306852b205",
            "compositeImage": {
                "id": "7c5fca5e-7695-43c9-80c5-8c493b51a56a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "870a9ccf-a384-4904-9da0-51e6e91470f0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7a81d421-e803-4ec9-a768-0cedc8eabc53",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "870a9ccf-a384-4904-9da0-51e6e91470f0",
                    "LayerId": "f6623482-4e61-4cce-8c54-5cb9efde5b13"
                }
            ]
        },
        {
            "id": "6d878118-27ed-4950-83e8-f0e0bd3267aa",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f3e725e3-302b-4e0b-9b61-df306852b205",
            "compositeImage": {
                "id": "6073f472-9d72-44b0-aa83-2a46093331ba",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6d878118-27ed-4950-83e8-f0e0bd3267aa",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5d108bca-0062-4897-93c2-bd16eeebcee9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6d878118-27ed-4950-83e8-f0e0bd3267aa",
                    "LayerId": "f6623482-4e61-4cce-8c54-5cb9efde5b13"
                }
            ]
        },
        {
            "id": "ecef1922-db71-4cc6-ad90-cc5d3704efd1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f3e725e3-302b-4e0b-9b61-df306852b205",
            "compositeImage": {
                "id": "7199cb45-3abb-4505-87f1-5f6c103956b5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ecef1922-db71-4cc6-ad90-cc5d3704efd1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "74564ce7-9fcc-44ca-b436-71dc64b5785d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ecef1922-db71-4cc6-ad90-cc5d3704efd1",
                    "LayerId": "f6623482-4e61-4cce-8c54-5cb9efde5b13"
                }
            ]
        },
        {
            "id": "451bb47c-3129-48b4-9a3a-21cd6dfa84e6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f3e725e3-302b-4e0b-9b61-df306852b205",
            "compositeImage": {
                "id": "0bc8b0ad-83d5-4bdd-87ce-5a3d35f6b698",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "451bb47c-3129-48b4-9a3a-21cd6dfa84e6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f0ef10cc-e51b-45ad-8997-3f22dd51b8bf",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "451bb47c-3129-48b4-9a3a-21cd6dfa84e6",
                    "LayerId": "f6623482-4e61-4cce-8c54-5cb9efde5b13"
                }
            ]
        },
        {
            "id": "549db51b-3e1c-4330-9262-e6e26b5db95a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f3e725e3-302b-4e0b-9b61-df306852b205",
            "compositeImage": {
                "id": "ea12a7a7-4c25-4b7f-a354-43861e2c9dc9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "549db51b-3e1c-4330-9262-e6e26b5db95a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4b19d27c-6ad9-45ed-8740-09793836e304",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "549db51b-3e1c-4330-9262-e6e26b5db95a",
                    "LayerId": "f6623482-4e61-4cce-8c54-5cb9efde5b13"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "f6623482-4e61-4cce-8c54-5cb9efde5b13",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "f3e725e3-302b-4e0b-9b61-df306852b205",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 3,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 0,
    "yorig": 8
}