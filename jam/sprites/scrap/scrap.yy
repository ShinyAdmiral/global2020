{
    "id": "3f518875-de1d-4e47-bad2-9ea59b83649f",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "scrap",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 11,
    "bbox_left": 4,
    "bbox_right": 11,
    "bbox_top": 4,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "843d1a8c-d7cd-4d72-be02-a40e233139ab",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3f518875-de1d-4e47-bad2-9ea59b83649f",
            "compositeImage": {
                "id": "01076500-9ac1-4015-b7ea-b316eaea5fe6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "843d1a8c-d7cd-4d72-be02-a40e233139ab",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3a4ea19a-937e-47d4-b315-6e68924349de",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "843d1a8c-d7cd-4d72-be02-a40e233139ab",
                    "LayerId": "07cb2a88-00a2-4341-99d8-c69307b7e6f6"
                }
            ]
        },
        {
            "id": "a5f0a037-43a9-4747-be40-c6516c849615",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3f518875-de1d-4e47-bad2-9ea59b83649f",
            "compositeImage": {
                "id": "809ac537-deac-4895-8010-955b86b00c01",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a5f0a037-43a9-4747-be40-c6516c849615",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "58761a54-9ee9-4282-9e85-5396562f2fff",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a5f0a037-43a9-4747-be40-c6516c849615",
                    "LayerId": "07cb2a88-00a2-4341-99d8-c69307b7e6f6"
                }
            ]
        },
        {
            "id": "9b805562-2158-4f66-88aa-b72deab08bba",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3f518875-de1d-4e47-bad2-9ea59b83649f",
            "compositeImage": {
                "id": "cd90cc43-6050-4ad7-bfd2-e5ba24c0d285",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9b805562-2158-4f66-88aa-b72deab08bba",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9334e4b0-dad5-4438-a97f-221005618d4a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9b805562-2158-4f66-88aa-b72deab08bba",
                    "LayerId": "07cb2a88-00a2-4341-99d8-c69307b7e6f6"
                }
            ]
        },
        {
            "id": "9455f077-9040-4c29-9a1f-e7a902bfbd61",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3f518875-de1d-4e47-bad2-9ea59b83649f",
            "compositeImage": {
                "id": "8e0ce1f3-75a1-4793-8533-7063a8915ba0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9455f077-9040-4c29-9a1f-e7a902bfbd61",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "43df36ec-cccc-4ed0-bbe4-763bd239b0d2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9455f077-9040-4c29-9a1f-e7a902bfbd61",
                    "LayerId": "07cb2a88-00a2-4341-99d8-c69307b7e6f6"
                }
            ]
        },
        {
            "id": "9b166c56-fb03-4a34-bd28-35cdd2287b91",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3f518875-de1d-4e47-bad2-9ea59b83649f",
            "compositeImage": {
                "id": "41378977-10a5-4643-87f5-3a8fd17dbb72",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9b166c56-fb03-4a34-bd28-35cdd2287b91",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "acca3fbc-801e-4f86-b6ee-18ae4c001b72",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9b166c56-fb03-4a34-bd28-35cdd2287b91",
                    "LayerId": "07cb2a88-00a2-4341-99d8-c69307b7e6f6"
                }
            ]
        },
        {
            "id": "ef28bde4-3a85-4ab6-9bbc-b854a68226ee",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3f518875-de1d-4e47-bad2-9ea59b83649f",
            "compositeImage": {
                "id": "a37489a9-89d3-4cb8-851d-4e7e22b39ce6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ef28bde4-3a85-4ab6-9bbc-b854a68226ee",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d88cb39e-da9f-4922-b955-e9ed7301057e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ef28bde4-3a85-4ab6-9bbc-b854a68226ee",
                    "LayerId": "07cb2a88-00a2-4341-99d8-c69307b7e6f6"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "07cb2a88-00a2-4341-99d8-c69307b7e6f6",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "3f518875-de1d-4e47-bad2-9ea59b83649f",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}