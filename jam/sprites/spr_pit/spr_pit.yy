{
    "id": "f532f7c0-5df7-4d13-a900-41639ecbd2a8",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_pit",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 32,
    "bbox_left": 53,
    "bbox_right": 63,
    "bbox_top": 2,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "964fcddd-d521-4d5e-ada0-a7468e3c8bbf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f532f7c0-5df7-4d13-a900-41639ecbd2a8",
            "compositeImage": {
                "id": "301a9a82-7305-4305-980b-10f212d83303",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "964fcddd-d521-4d5e-ada0-a7468e3c8bbf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f77a3658-8927-4b2d-9661-9dbb4cc960b0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "964fcddd-d521-4d5e-ada0-a7468e3c8bbf",
                    "LayerId": "f9388975-26f3-436b-a142-4825bc922dec"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 35,
    "layers": [
        {
            "id": "f9388975-26f3-436b-a142-4825bc922dec",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "f532f7c0-5df7-4d13-a900-41639ecbd2a8",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 67,
    "xorig": 0,
    "yorig": 0
}