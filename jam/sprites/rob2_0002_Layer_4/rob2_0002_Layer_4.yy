{
    "id": "a8d87b06-ee16-44c5-a602-6c6b6eedc54c",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "rob2_0002_Layer_4",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 39,
    "bbox_left": 0,
    "bbox_right": 24,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 0,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "4eb9dc7d-2fce-431f-bcd8-22e40e2bb43c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a8d87b06-ee16-44c5-a602-6c6b6eedc54c",
            "compositeImage": {
                "id": "efc9aa48-b316-4147-84f2-cf6cb6c6765e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4eb9dc7d-2fce-431f-bcd8-22e40e2bb43c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5125890a-367c-4d2c-ad71-79243c6e8397",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4eb9dc7d-2fce-431f-bcd8-22e40e2bb43c",
                    "LayerId": "0bcd0b6f-dcc5-4f1c-a94e-afa5950602e9"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 40,
    "layers": [
        {
            "id": "0bcd0b6f-dcc5-4f1c-a94e-afa5950602e9",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "a8d87b06-ee16-44c5-a602-6c6b6eedc54c",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 25,
    "xorig": 22,
    "yorig": 35
}