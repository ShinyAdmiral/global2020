{
    "id": "fb8302d0-8a47-4c19-8b0a-a317959df8fe",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "battle_sephiroth",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 34,
    "bbox_left": 0,
    "bbox_right": 41,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "4dc13af7-8b25-4939-abe6-497ec7b487cc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fb8302d0-8a47-4c19-8b0a-a317959df8fe",
            "compositeImage": {
                "id": "7b8bd43e-9683-4936-a725-cf86a0b68dbc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4dc13af7-8b25-4939-abe6-497ec7b487cc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5b7ca3f3-7bce-43ac-a991-37783b3861e2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4dc13af7-8b25-4939-abe6-497ec7b487cc",
                    "LayerId": "04d46b1e-40f7-43ae-ac3a-b88db04b6d25"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 35,
    "layers": [
        {
            "id": "04d46b1e-40f7-43ae-ac3a-b88db04b6d25",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "fb8302d0-8a47-4c19-8b0a-a317959df8fe",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 7,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 42,
    "xorig": 21,
    "yorig": 34
}