{
    "id": "2437365e-6c32-4079-a474-4319cf354fda",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "enemy_teddy",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 28,
    "bbox_left": 0,
    "bbox_right": 29,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "ccb753d8-9c8d-4710-816d-2f387b933001",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2437365e-6c32-4079-a474-4319cf354fda",
            "compositeImage": {
                "id": "c9c45982-b11f-4c22-93a7-3f72e512ec08",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ccb753d8-9c8d-4710-816d-2f387b933001",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8a9772fb-994a-4dca-8100-23c1226776e2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ccb753d8-9c8d-4710-816d-2f387b933001",
                    "LayerId": "0bfa61fc-04cf-4a5c-a262-a55f57a32955"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 29,
    "layers": [
        {
            "id": "0bfa61fc-04cf-4a5c-a262-a55f57a32955",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "2437365e-6c32-4079-a474-4319cf354fda",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 7,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 30,
    "xorig": 15,
    "yorig": 28
}