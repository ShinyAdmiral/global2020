{
    "id": "de31515b-2fb5-48be-9ff6-b598f5839d68",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_OK",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 32,
    "bbox_left": 2,
    "bbox_right": 78,
    "bbox_top": 3,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "73664d0a-22ea-40d0-afe0-e88d94c453df",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "de31515b-2fb5-48be-9ff6-b598f5839d68",
            "compositeImage": {
                "id": "4ce488cc-f565-413f-8179-582438819755",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "73664d0a-22ea-40d0-afe0-e88d94c453df",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e3b445cd-5df4-4126-9678-e69a5894fa40",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "73664d0a-22ea-40d0-afe0-e88d94c453df",
                    "LayerId": "b493eaa4-785a-464f-a552-81f8ad414eab"
                }
            ]
        },
        {
            "id": "8afd21ea-9316-4682-887b-be10b137a609",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "de31515b-2fb5-48be-9ff6-b598f5839d68",
            "compositeImage": {
                "id": "fa62f3fa-5ae8-429f-a9a3-72339afa3aab",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8afd21ea-9316-4682-887b-be10b137a609",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c0822dc4-63e2-47b0-afa2-d242e095b9c6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8afd21ea-9316-4682-887b-be10b137a609",
                    "LayerId": "b493eaa4-785a-464f-a552-81f8ad414eab"
                }
            ]
        },
        {
            "id": "93e0ab20-1de9-408a-be94-6df4bf3bf99c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "de31515b-2fb5-48be-9ff6-b598f5839d68",
            "compositeImage": {
                "id": "c1d481a7-5cf6-4350-b73f-8815c6a354b5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "93e0ab20-1de9-408a-be94-6df4bf3bf99c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9b4eb0e7-b5a3-4392-99f0-1845c9811031",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "93e0ab20-1de9-408a-be94-6df4bf3bf99c",
                    "LayerId": "b493eaa4-785a-464f-a552-81f8ad414eab"
                }
            ]
        },
        {
            "id": "57e5c632-9e6c-4449-b11d-74c1ccc2ffb1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "de31515b-2fb5-48be-9ff6-b598f5839d68",
            "compositeImage": {
                "id": "084b1fdf-3c3d-4ec6-921f-e08426f9b4dd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "57e5c632-9e6c-4449-b11d-74c1ccc2ffb1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c89bb06e-73b3-4b52-b149-1925c95cfb54",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "57e5c632-9e6c-4449-b11d-74c1ccc2ffb1",
                    "LayerId": "b493eaa4-785a-464f-a552-81f8ad414eab"
                }
            ]
        },
        {
            "id": "96a17b09-ce70-4218-87b6-875cebc925b0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "de31515b-2fb5-48be-9ff6-b598f5839d68",
            "compositeImage": {
                "id": "1a6e0880-7da3-428b-ac9c-140780b91497",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "96a17b09-ce70-4218-87b6-875cebc925b0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d7660138-f45f-48ee-a620-3dafecce673a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "96a17b09-ce70-4218-87b6-875cebc925b0",
                    "LayerId": "b493eaa4-785a-464f-a552-81f8ad414eab"
                }
            ]
        },
        {
            "id": "156f9e21-959a-49c7-9b0f-2470de4e2ff9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "de31515b-2fb5-48be-9ff6-b598f5839d68",
            "compositeImage": {
                "id": "254555a0-cfa5-4a78-aaf5-5f8b54f0e650",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "156f9e21-959a-49c7-9b0f-2470de4e2ff9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f56857ec-4db4-407a-ad3b-f6ba0b87acaf",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "156f9e21-959a-49c7-9b0f-2470de4e2ff9",
                    "LayerId": "b493eaa4-785a-464f-a552-81f8ad414eab"
                }
            ]
        },
        {
            "id": "28739f59-a722-4f7f-a63d-90e51c646f13",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "de31515b-2fb5-48be-9ff6-b598f5839d68",
            "compositeImage": {
                "id": "398165cb-5e1c-4760-87c9-b5cd8d000709",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "28739f59-a722-4f7f-a63d-90e51c646f13",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "90ea233d-348a-4419-83c5-0a6a814e0074",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "28739f59-a722-4f7f-a63d-90e51c646f13",
                    "LayerId": "b493eaa4-785a-464f-a552-81f8ad414eab"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 35,
    "layers": [
        {
            "id": "b493eaa4-785a-464f-a552-81f8ad414eab",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "de31515b-2fb5-48be-9ff6-b598f5839d68",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 5,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 80,
    "xorig": -3,
    "yorig": 37
}