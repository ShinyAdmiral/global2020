{
    "id": "70fc20ed-87cc-4cb2-b64f-d4369ebe7624",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "rob7_0004_Layer_6",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 0,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "c7d6f7f4-1151-4e7b-ba42-8442c1476244",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "70fc20ed-87cc-4cb2-b64f-d4369ebe7624",
            "compositeImage": {
                "id": "e38318d7-21c0-4214-84e1-5137305459a6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c7d6f7f4-1151-4e7b-ba42-8442c1476244",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "50455115-315f-4622-8ced-9b77cc257966",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c7d6f7f4-1151-4e7b-ba42-8442c1476244",
                    "LayerId": "642de1bb-6f90-45d5-bb42-e82d0c83e52b"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "642de1bb-6f90-45d5-bb42-e82d0c83e52b",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "70fc20ed-87cc-4cb2-b64f-d4369ebe7624",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 4,
    "yorig": 0
}