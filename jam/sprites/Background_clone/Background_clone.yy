{
    "id": "3557e8bb-a441-48a3-9a88-53325b6ceb6a",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "Background_clone",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 42,
    "bbox_left": 21,
    "bbox_right": 54,
    "bbox_top": 19,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "cdbdde56-b8ad-4394-86a6-6660d495270b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3557e8bb-a441-48a3-9a88-53325b6ceb6a",
            "compositeImage": {
                "id": "74a1f16b-739e-48bf-9ecc-bed1a1de4bd8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cdbdde56-b8ad-4394-86a6-6660d495270b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "089fddb9-22db-481e-aefc-9a2314f5fff8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cdbdde56-b8ad-4394-86a6-6660d495270b",
                    "LayerId": "dbd5b3d6-f9e6-4af8-9652-6b1b7675751e"
                }
            ]
        },
        {
            "id": "4e4ef483-653d-45e8-a18e-6f1989d566f3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3557e8bb-a441-48a3-9a88-53325b6ceb6a",
            "compositeImage": {
                "id": "376aa2e1-0e0f-451d-8471-5857259459e3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4e4ef483-653d-45e8-a18e-6f1989d566f3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b9277bf0-d185-4365-bbc9-580c73557ad9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4e4ef483-653d-45e8-a18e-6f1989d566f3",
                    "LayerId": "dbd5b3d6-f9e6-4af8-9652-6b1b7675751e"
                }
            ]
        },
        {
            "id": "869cf550-65c1-4e7a-9133-c505b64962c2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3557e8bb-a441-48a3-9a88-53325b6ceb6a",
            "compositeImage": {
                "id": "82fc68ad-1bc2-45f0-a0a8-824018085b15",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "869cf550-65c1-4e7a-9133-c505b64962c2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8f1ef358-c61d-4704-a9d0-1a4da08179e5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "869cf550-65c1-4e7a-9133-c505b64962c2",
                    "LayerId": "dbd5b3d6-f9e6-4af8-9652-6b1b7675751e"
                }
            ]
        },
        {
            "id": "f1340f72-3dd9-413f-ae5d-9ac47ad80f0f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3557e8bb-a441-48a3-9a88-53325b6ceb6a",
            "compositeImage": {
                "id": "a6fca310-78ab-4fd2-9fbf-5ed9e6b6e503",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f1340f72-3dd9-413f-ae5d-9ac47ad80f0f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fcae489c-9055-471b-a70f-57088c249ea6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f1340f72-3dd9-413f-ae5d-9ac47ad80f0f",
                    "LayerId": "dbd5b3d6-f9e6-4af8-9652-6b1b7675751e"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 60,
    "layers": [
        {
            "id": "dbd5b3d6-f9e6-4af8-9652-6b1b7675751e",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "3557e8bb-a441-48a3-9a88-53325b6ceb6a",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 2,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 80,
    "xorig": 0,
    "yorig": 0
}