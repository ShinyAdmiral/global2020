{
    "id": "c7aafd54-bb1a-4685-9e7a-22cb3b029b49",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "punch",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 22,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 9,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "f4da126d-d258-4e4f-89c9-f8e11a7e7047",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c7aafd54-bb1a-4685-9e7a-22cb3b029b49",
            "compositeImage": {
                "id": "204674be-12e1-4dd3-b448-7df9e6410f13",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f4da126d-d258-4e4f-89c9-f8e11a7e7047",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8836e19b-2963-47f8-9950-b69e4ecd984c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f4da126d-d258-4e4f-89c9-f8e11a7e7047",
                    "LayerId": "47df67af-f680-406d-96a7-52300b0a8edd"
                }
            ]
        },
        {
            "id": "78e61dff-0750-43f3-a81d-85cbd94cc0ad",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c7aafd54-bb1a-4685-9e7a-22cb3b029b49",
            "compositeImage": {
                "id": "ca9b8168-5991-4d09-af89-51543305a447",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "78e61dff-0750-43f3-a81d-85cbd94cc0ad",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7f224c4e-56de-4313-a40d-43a0fba6dc72",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "78e61dff-0750-43f3-a81d-85cbd94cc0ad",
                    "LayerId": "47df67af-f680-406d-96a7-52300b0a8edd"
                }
            ]
        },
        {
            "id": "b7170b62-ca60-443d-ba4e-bb99e6a33820",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c7aafd54-bb1a-4685-9e7a-22cb3b029b49",
            "compositeImage": {
                "id": "da8f3e45-352b-4ff7-aa93-28d3853993f0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b7170b62-ca60-443d-ba4e-bb99e6a33820",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3e4e9d77-656e-4828-a063-f707e59a5dd7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b7170b62-ca60-443d-ba4e-bb99e6a33820",
                    "LayerId": "47df67af-f680-406d-96a7-52300b0a8edd"
                }
            ]
        },
        {
            "id": "e2aab496-2ea9-49f2-afb6-c36951da413f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c7aafd54-bb1a-4685-9e7a-22cb3b029b49",
            "compositeImage": {
                "id": "13984b29-9206-40ff-b471-b4e1a219c77c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e2aab496-2ea9-49f2-afb6-c36951da413f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ad6dd373-a5c1-410f-9ede-016a3472daac",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e2aab496-2ea9-49f2-afb6-c36951da413f",
                    "LayerId": "47df67af-f680-406d-96a7-52300b0a8edd"
                }
            ]
        },
        {
            "id": "208c7645-663c-4acd-be91-ac1932c5cde1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c7aafd54-bb1a-4685-9e7a-22cb3b029b49",
            "compositeImage": {
                "id": "999ec627-8539-4981-85a5-992f1b6c1e77",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "208c7645-663c-4acd-be91-ac1932c5cde1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a64b3708-aaea-4fa6-8364-18fcb2bbd882",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "208c7645-663c-4acd-be91-ac1932c5cde1",
                    "LayerId": "47df67af-f680-406d-96a7-52300b0a8edd"
                }
            ]
        },
        {
            "id": "24627808-3ba5-4048-8233-7ac0b66ff8bc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c7aafd54-bb1a-4685-9e7a-22cb3b029b49",
            "compositeImage": {
                "id": "18fffca8-07d8-4a0f-ba66-2af2f2b4639f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "24627808-3ba5-4048-8233-7ac0b66ff8bc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "88e3b60c-f525-4edc-a104-5c43dd0074c7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "24627808-3ba5-4048-8233-7ac0b66ff8bc",
                    "LayerId": "47df67af-f680-406d-96a7-52300b0a8edd"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "47df67af-f680-406d-96a7-52300b0a8edd",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "c7aafd54-bb1a-4685-9e7a-22cb3b029b49",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 3,
    "originLocked": false,
    "playbackSpeed": 20,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 16
}