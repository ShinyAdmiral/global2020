{
    "id": "75ee01fb-a43c-45d1-8114-fd1c8d8a7903",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "rob6_0002_Layer_4",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 35,
    "bbox_left": 0,
    "bbox_right": 60,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 0,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "e1cadcc9-3b4b-4936-a52c-84e64d0ed58c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "75ee01fb-a43c-45d1-8114-fd1c8d8a7903",
            "compositeImage": {
                "id": "298b9117-1e87-4c87-a282-3f97143e649e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e1cadcc9-3b4b-4936-a52c-84e64d0ed58c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ce28b37d-387f-4e17-b370-deca443afc5a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e1cadcc9-3b4b-4936-a52c-84e64d0ed58c",
                    "LayerId": "464f4e8a-d9c1-4f5b-bdc4-964ff5f05055"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 36,
    "layers": [
        {
            "id": "464f4e8a-d9c1-4f5b-bdc4-964ff5f05055",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "75ee01fb-a43c-45d1-8114-fd1c8d8a7903",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 61,
    "xorig": 30,
    "yorig": 18
}