{
    "id": "d111cd6d-f5bc-4c57-aefe-c2c324e0a795",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "rob7_0006_Layer_8",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 20,
    "bbox_left": 0,
    "bbox_right": 8,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 0,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "5eae854d-7426-4e37-9aea-577f172436e4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d111cd6d-f5bc-4c57-aefe-c2c324e0a795",
            "compositeImage": {
                "id": "89ec8937-b066-4b06-a5f1-32362abd6084",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5eae854d-7426-4e37-9aea-577f172436e4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "99471b90-278a-47b9-9919-27981fcef407",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5eae854d-7426-4e37-9aea-577f172436e4",
                    "LayerId": "3cad83b9-8e3b-4688-8713-62a4855046fa"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 21,
    "layers": [
        {
            "id": "3cad83b9-8e3b-4688-8713-62a4855046fa",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "d111cd6d-f5bc-4c57-aefe-c2c324e0a795",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 1,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 9,
    "xorig": 4,
    "yorig": 0
}