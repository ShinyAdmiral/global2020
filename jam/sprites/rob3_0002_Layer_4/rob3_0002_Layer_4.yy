{
    "id": "f7030a54-3d1d-4b2c-b8d3-2a4d13a7e8cf",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "rob3_0002_Layer_4",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 8,
    "bbox_left": 0,
    "bbox_right": 25,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 0,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "6417552a-d243-4c5a-b830-f8e87ce697de",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f7030a54-3d1d-4b2c-b8d3-2a4d13a7e8cf",
            "compositeImage": {
                "id": "a9972e86-23b8-456c-8d7a-06884b761a19",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6417552a-d243-4c5a-b830-f8e87ce697de",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0165c5d7-c28a-46a3-a429-3457b1b372af",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6417552a-d243-4c5a-b830-f8e87ce697de",
                    "LayerId": "c99eccae-5a42-4271-9e88-91025b536d2a"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 9,
    "layers": [
        {
            "id": "c99eccae-5a42-4271-9e88-91025b536d2a",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "f7030a54-3d1d-4b2c-b8d3-2a4d13a7e8cf",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 26,
    "xorig": 21,
    "yorig": 4
}