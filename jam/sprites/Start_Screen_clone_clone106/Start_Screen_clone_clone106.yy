{
    "id": "e02b5fee-c8d2-40cb-a4e9-67527daf7859",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "Start_Screen_clone_clone106",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 113,
    "bbox_left": 111,
    "bbox_right": 186,
    "bbox_top": 84,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "fa6e0c77-6e4b-4a79-95e7-7b5f199d2a36",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e02b5fee-c8d2-40cb-a4e9-67527daf7859",
            "compositeImage": {
                "id": "04d7add7-763c-4537-8824-618c28291497",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fa6e0c77-6e4b-4a79-95e7-7b5f199d2a36",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6ebf4e71-3427-46c9-b0c9-8d624a8c62b7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fa6e0c77-6e4b-4a79-95e7-7b5f199d2a36",
                    "LayerId": "b5e3402e-2837-448f-b870-0860d8daeec8"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 180,
    "layers": [
        {
            "id": "b5e3402e-2837-448f-b870-0860d8daeec8",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "e02b5fee-c8d2-40cb-a4e9-67527daf7859",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 320,
    "xorig": 150,
    "yorig": 89
}