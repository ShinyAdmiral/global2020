{
    "id": "37863f91-d3e5-44d8-a6fb-534d08b64e7a",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "rob1_0001_Layer_3",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 14,
    "bbox_left": 0,
    "bbox_right": 27,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 0,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "a3bf2d01-c409-4604-a7b6-0859c7ee8943",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "37863f91-d3e5-44d8-a6fb-534d08b64e7a",
            "compositeImage": {
                "id": "2a6aa875-7358-4055-b333-bb71ba84ff5d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a3bf2d01-c409-4604-a7b6-0859c7ee8943",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "64f60623-19d7-41f5-8658-d38668c9b5b4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a3bf2d01-c409-4604-a7b6-0859c7ee8943",
                    "LayerId": "009ea323-74d8-42b4-8488-60c5af8885d3"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 15,
    "layers": [
        {
            "id": "009ea323-74d8-42b4-8488-60c5af8885d3",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "37863f91-d3e5-44d8-a6fb-534d08b64e7a",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 28,
    "xorig": 4,
    "yorig": 7
}