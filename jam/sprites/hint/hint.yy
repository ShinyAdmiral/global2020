{
    "id": "8c586a24-9055-4011-9827-4cea34ac9260",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "hint",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 14,
    "bbox_left": 1,
    "bbox_right": 14,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "16bb85fa-4d0f-4b0c-aa79-665cc7299d41",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8c586a24-9055-4011-9827-4cea34ac9260",
            "compositeImage": {
                "id": "8aa48e8a-9e99-4ee2-b84e-7772cb9a75dd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "16bb85fa-4d0f-4b0c-aa79-665cc7299d41",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "53243a3a-be78-4cd7-9dad-0f43008e81d1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "16bb85fa-4d0f-4b0c-aa79-665cc7299d41",
                    "LayerId": "7053ea48-2a61-4861-a86c-11da9fe2ca15"
                }
            ]
        },
        {
            "id": "f168a057-e1f3-4609-930d-fda8ba76aae7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8c586a24-9055-4011-9827-4cea34ac9260",
            "compositeImage": {
                "id": "a6baddf4-992e-4916-af17-65cb4151ffdb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f168a057-e1f3-4609-930d-fda8ba76aae7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "26846439-1753-4c6b-84db-2ec026f5405f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f168a057-e1f3-4609-930d-fda8ba76aae7",
                    "LayerId": "7053ea48-2a61-4861-a86c-11da9fe2ca15"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "7053ea48-2a61-4861-a86c-11da9fe2ca15",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "8c586a24-9055-4011-9827-4cea34ac9260",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 1,
    "originLocked": false,
    "playbackSpeed": 12,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 0
}