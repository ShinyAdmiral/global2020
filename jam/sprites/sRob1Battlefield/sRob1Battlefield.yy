{
    "id": "1affd885-2bd6-4265-a0c8-304818bc17c9",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sRob1Battlefield",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 2,
    "bbox_right": 29,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "1e9a1692-0261-4a23-9816-fac06ef97982",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1affd885-2bd6-4265-a0c8-304818bc17c9",
            "compositeImage": {
                "id": "2e577502-48fa-4ec5-a31d-9101da8501ac",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1e9a1692-0261-4a23-9816-fac06ef97982",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3f09a999-8b9b-4df1-9ecb-7aec8348d3ab",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1e9a1692-0261-4a23-9816-fac06ef97982",
                    "LayerId": "3a2a0518-9526-44dc-a50e-d4061febc97b"
                }
            ]
        },
        {
            "id": "374bf04b-bfb8-423c-b087-5c9a58d36ac7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1affd885-2bd6-4265-a0c8-304818bc17c9",
            "compositeImage": {
                "id": "88afdeee-f4de-4746-b7b1-6313932e3cdd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "374bf04b-bfb8-423c-b087-5c9a58d36ac7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "617e07a8-2f9d-4c6b-bf1f-f18580873630",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "374bf04b-bfb8-423c-b087-5c9a58d36ac7",
                    "LayerId": "3a2a0518-9526-44dc-a50e-d4061febc97b"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "3a2a0518-9526-44dc-a50e-d4061febc97b",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "1affd885-2bd6-4265-a0c8-304818bc17c9",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 5,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}