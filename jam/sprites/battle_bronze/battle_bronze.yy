{
    "id": "8586ec10-bae1-4056-9bfc-4635c4530034",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "battle_bronze",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 28,
    "bbox_left": 0,
    "bbox_right": 27,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "5d9689c7-67e8-4d68-8d18-adcf0459a57a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8586ec10-bae1-4056-9bfc-4635c4530034",
            "compositeImage": {
                "id": "b6565c58-1552-4b62-adff-bad68c4ad965",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5d9689c7-67e8-4d68-8d18-adcf0459a57a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5b379a80-f1a5-4fdc-bbde-9b431a7f6cdb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5d9689c7-67e8-4d68-8d18-adcf0459a57a",
                    "LayerId": "41f5501b-1de4-4182-8de4-9300903443f1"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 29,
    "layers": [
        {
            "id": "41f5501b-1de4-4182-8de4-9300903443f1",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "8586ec10-bae1-4056-9bfc-4635c4530034",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 7,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 28,
    "xorig": 14,
    "yorig": 28
}