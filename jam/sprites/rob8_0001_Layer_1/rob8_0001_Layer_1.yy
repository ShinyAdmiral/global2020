{
    "id": "43d652cb-f702-4abd-8a76-5d200d4cd30f",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "rob8_0001_Layer_1",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 23,
    "bbox_left": 0,
    "bbox_right": 40,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 0,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "325f6cc6-a19d-4a4c-a034-41f31a6710e1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "43d652cb-f702-4abd-8a76-5d200d4cd30f",
            "compositeImage": {
                "id": "8159b2dd-e6b9-4188-b7c0-c82adfb51ee5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "325f6cc6-a19d-4a4c-a034-41f31a6710e1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "46a673db-23a4-4b2b-8e79-b70c7aa14ea4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "325f6cc6-a19d-4a4c-a034-41f31a6710e1",
                    "LayerId": "b1e704c5-4f5c-4384-a908-b7f81434e345"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 24,
    "layers": [
        {
            "id": "b1e704c5-4f5c-4384-a908-b7f81434e345",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "43d652cb-f702-4abd-8a76-5d200d4cd30f",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 41,
    "xorig": 20,
    "yorig": 12
}