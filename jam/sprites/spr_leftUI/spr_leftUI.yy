{
    "id": "9d9a869f-bbb1-443b-960c-f3e1f79bbca4",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_leftUI",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 157,
    "bbox_left": 10,
    "bbox_right": 140,
    "bbox_top": 25,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "6ebb75ee-7947-4304-beca-32787abd1037",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9d9a869f-bbb1-443b-960c-f3e1f79bbca4",
            "compositeImage": {
                "id": "b2de03a5-87c1-4b5b-b5c3-297d5904b122",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6ebb75ee-7947-4304-beca-32787abd1037",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "22a30082-70db-4c99-a392-79b2935422ac",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6ebb75ee-7947-4304-beca-32787abd1037",
                    "LayerId": "695b8f11-f47c-43f8-a5b2-980d700c4fa7"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 220,
    "layers": [
        {
            "id": "695b8f11-f47c-43f8-a5b2-980d700c4fa7",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "9d9a869f-bbb1-443b-960c-f3e1f79bbca4",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 200,
    "xorig": 10,
    "yorig": 25
}