{
    "id": "59846448-dbbd-4f23-bfb5-2b909c03f1a6",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "battle_spaceman",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 30,
    "bbox_left": 0,
    "bbox_right": 29,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "7882e374-452b-4945-8ecb-092a511dd19a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "59846448-dbbd-4f23-bfb5-2b909c03f1a6",
            "compositeImage": {
                "id": "742eaa21-76ca-48f8-b2e3-2ce5d5447422",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7882e374-452b-4945-8ecb-092a511dd19a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3c594a4f-872f-4ecd-93f1-f56f4f8daf3c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7882e374-452b-4945-8ecb-092a511dd19a",
                    "LayerId": "c65d7b88-a3a8-497e-8e26-9c961ac703db"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 31,
    "layers": [
        {
            "id": "c65d7b88-a3a8-497e-8e26-9c961ac703db",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "59846448-dbbd-4f23-bfb5-2b909c03f1a6",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 7,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 30,
    "xorig": 15,
    "yorig": 30
}