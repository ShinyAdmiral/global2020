{
    "id": "858cad37-435a-4663-9934-5ac3d78042db",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "rob7_0008_Layer_1",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 51,
    "bbox_left": 0,
    "bbox_right": 54,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 0,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "b7a073f1-5b80-492d-8ff5-82797d27004d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "858cad37-435a-4663-9934-5ac3d78042db",
            "compositeImage": {
                "id": "61644477-70cc-4642-80fe-bb81ff65a476",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b7a073f1-5b80-492d-8ff5-82797d27004d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "af6fad8d-2d19-4e14-86f6-40ef876e2294",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b7a073f1-5b80-492d-8ff5-82797d27004d",
                    "LayerId": "f9a4916e-7d38-45a4-b910-b989cf68fd37"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 52,
    "layers": [
        {
            "id": "f9a4916e-7d38-45a4-b910-b989cf68fd37",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "858cad37-435a-4663-9934-5ac3d78042db",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 55,
    "xorig": 27,
    "yorig": 26
}