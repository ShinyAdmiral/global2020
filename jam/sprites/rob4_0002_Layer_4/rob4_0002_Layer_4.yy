{
    "id": "6b46e56b-837b-4611-a69f-78a3d82705be",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "rob4_0002_Layer_4",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 37,
    "bbox_left": 0,
    "bbox_right": 27,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 0,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "ec478836-f81e-4b65-bb73-7581d75f057d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6b46e56b-837b-4611-a69f-78a3d82705be",
            "compositeImage": {
                "id": "7630a808-2f64-4b8b-947d-3a2b3622cfba",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ec478836-f81e-4b65-bb73-7581d75f057d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "098b4225-283e-4378-873b-25bb446c5418",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ec478836-f81e-4b65-bb73-7581d75f057d",
                    "LayerId": "820fca50-8af7-4618-9be7-421d1e961eb4"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 38,
    "layers": [
        {
            "id": "820fca50-8af7-4618-9be7-421d1e961eb4",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "6b46e56b-837b-4611-a69f-78a3d82705be",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 28,
    "xorig": 14,
    "yorig": 2
}