{
    "id": "e34e63ef-947f-4c42-bf13-2164b3c7f6e0",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "battle_grievous",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 29,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "1a81ac3b-09f6-4ae3-b0bc-6aeee1f979bb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e34e63ef-947f-4c42-bf13-2164b3c7f6e0",
            "compositeImage": {
                "id": "f0a55cb2-a52a-4cfc-beb0-a62fd73f4b9c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1a81ac3b-09f6-4ae3-b0bc-6aeee1f979bb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a9d6fd27-0d38-44f3-b9bc-550ec462f0eb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1a81ac3b-09f6-4ae3-b0bc-6aeee1f979bb",
                    "LayerId": "1ef0e383-0709-425f-b5fa-59fb6c49a27b"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "1ef0e383-0709-425f-b5fa-59fb6c49a27b",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "e34e63ef-947f-4c42-bf13-2164b3c7f6e0",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 7,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 30,
    "xorig": 15,
    "yorig": 31
}