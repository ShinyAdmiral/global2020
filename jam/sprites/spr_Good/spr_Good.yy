{
    "id": "51ae6d00-80c5-4777-8632-4e788d4e74f7",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_Good",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 1,
    "bbox_right": 78,
    "bbox_top": 3,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "76d9b36d-f87d-432e-80d7-76f86b297a14",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "51ae6d00-80c5-4777-8632-4e788d4e74f7",
            "compositeImage": {
                "id": "7ea916a8-8cfc-4bd9-86a9-d5637cdc8167",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "76d9b36d-f87d-432e-80d7-76f86b297a14",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f82d5c0a-8571-49c5-8d61-8ad3dfbe9253",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "76d9b36d-f87d-432e-80d7-76f86b297a14",
                    "LayerId": "23e6a526-8cec-4f6b-8991-e8aeb06cf784"
                }
            ]
        },
        {
            "id": "8bedd112-afee-4547-b70d-aceef446fe64",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "51ae6d00-80c5-4777-8632-4e788d4e74f7",
            "compositeImage": {
                "id": "32b958e0-cf1d-4c9d-bb6a-46316c0831e8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8bedd112-afee-4547-b70d-aceef446fe64",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "054d6ca6-5dbe-4bf2-8f3a-780fb348dd0f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8bedd112-afee-4547-b70d-aceef446fe64",
                    "LayerId": "23e6a526-8cec-4f6b-8991-e8aeb06cf784"
                }
            ]
        },
        {
            "id": "ffa3af39-af89-4826-b1d5-813ae5b35d52",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "51ae6d00-80c5-4777-8632-4e788d4e74f7",
            "compositeImage": {
                "id": "2a41830f-e091-4eed-b533-f53d66408bab",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ffa3af39-af89-4826-b1d5-813ae5b35d52",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c6398b15-6dce-40ae-8f67-e27f318687ca",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ffa3af39-af89-4826-b1d5-813ae5b35d52",
                    "LayerId": "23e6a526-8cec-4f6b-8991-e8aeb06cf784"
                }
            ]
        },
        {
            "id": "f5bf5d77-1bb6-4e3f-8648-13fa9d5711dd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "51ae6d00-80c5-4777-8632-4e788d4e74f7",
            "compositeImage": {
                "id": "1b245909-86ec-4a2b-959c-fbe20f0f4808",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f5bf5d77-1bb6-4e3f-8648-13fa9d5711dd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "aa068e3b-13bb-4c16-ac18-a4acad4f929f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f5bf5d77-1bb6-4e3f-8648-13fa9d5711dd",
                    "LayerId": "23e6a526-8cec-4f6b-8991-e8aeb06cf784"
                }
            ]
        },
        {
            "id": "2e445edf-6a26-401e-b750-e3ee21af65fc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "51ae6d00-80c5-4777-8632-4e788d4e74f7",
            "compositeImage": {
                "id": "d8d490bf-1e67-4dfb-af8c-fc815549888b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2e445edf-6a26-401e-b750-e3ee21af65fc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "64c07a58-c540-412b-8ed5-247ee9049966",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2e445edf-6a26-401e-b750-e3ee21af65fc",
                    "LayerId": "23e6a526-8cec-4f6b-8991-e8aeb06cf784"
                }
            ]
        },
        {
            "id": "336cf576-5b7b-4db8-897a-070414e88529",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "51ae6d00-80c5-4777-8632-4e788d4e74f7",
            "compositeImage": {
                "id": "d37d81f3-bf0e-4aa8-b540-3a467565a502",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "336cf576-5b7b-4db8-897a-070414e88529",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "dbf321ae-4ec3-4230-a5e7-c558acc2bd12",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "336cf576-5b7b-4db8-897a-070414e88529",
                    "LayerId": "23e6a526-8cec-4f6b-8991-e8aeb06cf784"
                }
            ]
        },
        {
            "id": "9875a83e-b295-43f6-89fa-cf5a63993060",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "51ae6d00-80c5-4777-8632-4e788d4e74f7",
            "compositeImage": {
                "id": "ca4443eb-43ca-415c-a550-012fd7cd280c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9875a83e-b295-43f6-89fa-cf5a63993060",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "51b91b05-db4a-4edc-aeb8-909858515adc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9875a83e-b295-43f6-89fa-cf5a63993060",
                    "LayerId": "23e6a526-8cec-4f6b-8991-e8aeb06cf784"
                }
            ]
        },
        {
            "id": "43a3fed5-6399-465b-9a7e-6ccb3ebd6399",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "51ae6d00-80c5-4777-8632-4e788d4e74f7",
            "compositeImage": {
                "id": "fb18826f-ce89-4744-95ff-c37456957b0f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "43a3fed5-6399-465b-9a7e-6ccb3ebd6399",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8cbac0a3-5df2-447d-95f1-251878208de3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "43a3fed5-6399-465b-9a7e-6ccb3ebd6399",
                    "LayerId": "23e6a526-8cec-4f6b-8991-e8aeb06cf784"
                }
            ]
        },
        {
            "id": "4effa2c1-c95c-4c84-9dfc-007c83f4272b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "51ae6d00-80c5-4777-8632-4e788d4e74f7",
            "compositeImage": {
                "id": "0007c3e6-0b47-4213-a138-7626aacb205b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4effa2c1-c95c-4c84-9dfc-007c83f4272b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f2ece3de-e94b-4405-acea-3e1cc3ae13e0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4effa2c1-c95c-4c84-9dfc-007c83f4272b",
                    "LayerId": "23e6a526-8cec-4f6b-8991-e8aeb06cf784"
                }
            ]
        },
        {
            "id": "031c93bd-3706-45b3-a595-3345bee8b3d4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "51ae6d00-80c5-4777-8632-4e788d4e74f7",
            "compositeImage": {
                "id": "2e293201-481a-4c0b-a11c-685cf45ea197",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "031c93bd-3706-45b3-a595-3345bee8b3d4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fea11e81-d721-4782-92c5-1c99769f2926",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "031c93bd-3706-45b3-a595-3345bee8b3d4",
                    "LayerId": "23e6a526-8cec-4f6b-8991-e8aeb06cf784"
                }
            ]
        },
        {
            "id": "13994af1-898c-4008-be0a-09b588c645ea",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "51ae6d00-80c5-4777-8632-4e788d4e74f7",
            "compositeImage": {
                "id": "f297e3ac-705d-428a-a38f-cd0136c283cd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "13994af1-898c-4008-be0a-09b588c645ea",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4a7c5787-dfe9-45d4-9d29-61c322b9f572",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "13994af1-898c-4008-be0a-09b588c645ea",
                    "LayerId": "23e6a526-8cec-4f6b-8991-e8aeb06cf784"
                }
            ]
        },
        {
            "id": "e207885a-0323-4933-a059-fff40c4d8130",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "51ae6d00-80c5-4777-8632-4e788d4e74f7",
            "compositeImage": {
                "id": "8f96a9e6-82a6-4c37-a789-1aea99aa6ef7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e207885a-0323-4933-a059-fff40c4d8130",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c29a326b-ecc2-49c7-83c5-eaf4027ade92",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e207885a-0323-4933-a059-fff40c4d8130",
                    "LayerId": "23e6a526-8cec-4f6b-8991-e8aeb06cf784"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 35,
    "layers": [
        {
            "id": "23e6a526-8cec-4f6b-8991-e8aeb06cf784",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "51ae6d00-80c5-4777-8632-4e788d4e74f7",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 8,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 80,
    "xorig": -3,
    "yorig": 37
}