{
    "id": "935af09a-799d-44db-a5e1-a908769e3815",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_Bad",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 34,
    "bbox_left": 4,
    "bbox_right": 68,
    "bbox_top": 2,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "51ad6fff-802a-4a4a-9a64-79f06e8f2cea",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "935af09a-799d-44db-a5e1-a908769e3815",
            "compositeImage": {
                "id": "08cfbd96-b866-4da7-afb8-6119e4c17fb6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "51ad6fff-802a-4a4a-9a64-79f06e8f2cea",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "da3d2cc1-15c2-4283-8393-df79282247a3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "51ad6fff-802a-4a4a-9a64-79f06e8f2cea",
                    "LayerId": "8fe0e7f7-f4ae-4884-8d55-3894c19c320b"
                }
            ]
        },
        {
            "id": "af6162f3-6942-4aed-8b62-b10d015bd2dc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "935af09a-799d-44db-a5e1-a908769e3815",
            "compositeImage": {
                "id": "d49720dc-c512-4f35-89db-86de72f37edf",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "af6162f3-6942-4aed-8b62-b10d015bd2dc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f07416a4-5688-41af-b245-ffc2829d9592",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "af6162f3-6942-4aed-8b62-b10d015bd2dc",
                    "LayerId": "8fe0e7f7-f4ae-4884-8d55-3894c19c320b"
                }
            ]
        },
        {
            "id": "492029e2-90a5-4082-b9b2-8136f4096ade",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "935af09a-799d-44db-a5e1-a908769e3815",
            "compositeImage": {
                "id": "2e331e16-d977-4da1-b7b6-1556d6849f65",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "492029e2-90a5-4082-b9b2-8136f4096ade",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a27cd86d-4068-4393-ade0-8e19bb491732",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "492029e2-90a5-4082-b9b2-8136f4096ade",
                    "LayerId": "8fe0e7f7-f4ae-4884-8d55-3894c19c320b"
                }
            ]
        },
        {
            "id": "56e2210b-fd56-45c5-bd40-bf42735057d5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "935af09a-799d-44db-a5e1-a908769e3815",
            "compositeImage": {
                "id": "507eda5d-fb41-44a3-b390-27b3ba292b08",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "56e2210b-fd56-45c5-bd40-bf42735057d5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8a72c8d9-0240-4f05-b2b8-7d84f35fbb04",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "56e2210b-fd56-45c5-bd40-bf42735057d5",
                    "LayerId": "8fe0e7f7-f4ae-4884-8d55-3894c19c320b"
                }
            ]
        },
        {
            "id": "092a9e22-1d0f-4423-bd99-385e2af8cec5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "935af09a-799d-44db-a5e1-a908769e3815",
            "compositeImage": {
                "id": "56d5c7fc-8509-4275-8354-4cca6c0eeffe",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "092a9e22-1d0f-4423-bd99-385e2af8cec5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "413abd5b-d1a3-4a30-a6e3-ba8e8cfadf1a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "092a9e22-1d0f-4423-bd99-385e2af8cec5",
                    "LayerId": "8fe0e7f7-f4ae-4884-8d55-3894c19c320b"
                }
            ]
        },
        {
            "id": "fab832e1-4df4-4eb0-a808-45a6ff4a5362",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "935af09a-799d-44db-a5e1-a908769e3815",
            "compositeImage": {
                "id": "a1e1a1db-ca28-43f1-9175-d41a93fda752",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fab832e1-4df4-4eb0-a808-45a6ff4a5362",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3660ed64-866c-422b-af39-c393e1fbb192",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fab832e1-4df4-4eb0-a808-45a6ff4a5362",
                    "LayerId": "8fe0e7f7-f4ae-4884-8d55-3894c19c320b"
                }
            ]
        },
        {
            "id": "3e8a473a-be97-4005-b241-d6d7e35b03ea",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "935af09a-799d-44db-a5e1-a908769e3815",
            "compositeImage": {
                "id": "b9127dde-1344-4108-b0d2-52950688783e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3e8a473a-be97-4005-b241-d6d7e35b03ea",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ac004997-d0bf-40fc-9dbd-3a53a3c6f291",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3e8a473a-be97-4005-b241-d6d7e35b03ea",
                    "LayerId": "8fe0e7f7-f4ae-4884-8d55-3894c19c320b"
                }
            ]
        },
        {
            "id": "6983a97e-bc70-416b-a7de-dfe9d10ff351",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "935af09a-799d-44db-a5e1-a908769e3815",
            "compositeImage": {
                "id": "33e728a0-4014-4c3e-9cc8-8d1877e12d4f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6983a97e-bc70-416b-a7de-dfe9d10ff351",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "69c67023-c60e-4bf8-bc3f-24e3e0002acb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6983a97e-bc70-416b-a7de-dfe9d10ff351",
                    "LayerId": "8fe0e7f7-f4ae-4884-8d55-3894c19c320b"
                }
            ]
        },
        {
            "id": "d6caf44f-e8f0-420f-ad3d-9b764428e394",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "935af09a-799d-44db-a5e1-a908769e3815",
            "compositeImage": {
                "id": "17d61fa6-a044-41e0-8e1c-f237df2d415d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d6caf44f-e8f0-420f-ad3d-9b764428e394",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2dbde78f-61b6-4613-a790-2318996313b9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d6caf44f-e8f0-420f-ad3d-9b764428e394",
                    "LayerId": "8fe0e7f7-f4ae-4884-8d55-3894c19c320b"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 35,
    "layers": [
        {
            "id": "8fe0e7f7-f4ae-4884-8d55-3894c19c320b",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "935af09a-799d-44db-a5e1-a908769e3815",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 75,
    "xorig": -3,
    "yorig": 37
}