{
    "id": "7bee3777-cc65-4055-bba4-86957d4f2ecf",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "small_strike",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 18,
    "bbox_left": 0,
    "bbox_right": 27,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "877266bd-de8d-4df5-88b4-e3eec31d5202",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7bee3777-cc65-4055-bba4-86957d4f2ecf",
            "compositeImage": {
                "id": "70dfb71c-af44-4af8-a7ea-fd61cb21a45d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "877266bd-de8d-4df5-88b4-e3eec31d5202",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "97bdfde5-62cb-4e7c-9e67-d97a3f648e0c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "877266bd-de8d-4df5-88b4-e3eec31d5202",
                    "LayerId": "970f16d2-9033-4d45-9d63-57954cc7f859"
                }
            ]
        },
        {
            "id": "9844aa6a-25ee-4c0e-a5c0-9f04debefb12",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7bee3777-cc65-4055-bba4-86957d4f2ecf",
            "compositeImage": {
                "id": "2675b4fe-6312-428f-9ab5-005355789682",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9844aa6a-25ee-4c0e-a5c0-9f04debefb12",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d21c7f44-f688-4f1e-a819-51042a4c9b43",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9844aa6a-25ee-4c0e-a5c0-9f04debefb12",
                    "LayerId": "970f16d2-9033-4d45-9d63-57954cc7f859"
                }
            ]
        },
        {
            "id": "5df5cc02-7a30-4b20-9a9e-99bafec78b9b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7bee3777-cc65-4055-bba4-86957d4f2ecf",
            "compositeImage": {
                "id": "d42833f7-93c6-4a1d-a073-08410a13a527",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5df5cc02-7a30-4b20-9a9e-99bafec78b9b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "36aacbca-d305-41fe-9892-c542dfa3bf35",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5df5cc02-7a30-4b20-9a9e-99bafec78b9b",
                    "LayerId": "970f16d2-9033-4d45-9d63-57954cc7f859"
                }
            ]
        },
        {
            "id": "e4a1bf0c-d6b8-4f6f-87dc-862d0f05f8ba",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7bee3777-cc65-4055-bba4-86957d4f2ecf",
            "compositeImage": {
                "id": "96addbe4-0c50-4080-8ae4-cc5ffdcbaf9c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e4a1bf0c-d6b8-4f6f-87dc-862d0f05f8ba",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4b2926ee-bee4-48d9-b9f4-df46703750d2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e4a1bf0c-d6b8-4f6f-87dc-862d0f05f8ba",
                    "LayerId": "970f16d2-9033-4d45-9d63-57954cc7f859"
                }
            ]
        },
        {
            "id": "cb1b2f7c-750c-4d31-a8dd-2f15d322cbb3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7bee3777-cc65-4055-bba4-86957d4f2ecf",
            "compositeImage": {
                "id": "27fce749-8da1-4157-ba4e-0f72feaf378b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cb1b2f7c-750c-4d31-a8dd-2f15d322cbb3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f4895a70-8d93-4478-9157-571067d77fab",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cb1b2f7c-750c-4d31-a8dd-2f15d322cbb3",
                    "LayerId": "970f16d2-9033-4d45-9d63-57954cc7f859"
                }
            ]
        },
        {
            "id": "555de1ed-0b72-4a15-8092-9fa209bd921f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7bee3777-cc65-4055-bba4-86957d4f2ecf",
            "compositeImage": {
                "id": "64716137-9ba9-4e5c-95bf-a922d2b4e872",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "555de1ed-0b72-4a15-8092-9fa209bd921f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8d1c30b4-23bf-4aaa-8701-ae855c95111f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "555de1ed-0b72-4a15-8092-9fa209bd921f",
                    "LayerId": "970f16d2-9033-4d45-9d63-57954cc7f859"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "970f16d2-9033-4d45-9d63-57954cc7f859",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "7bee3777-cc65-4055-bba4-86957d4f2ecf",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 3,
    "originLocked": false,
    "playbackSpeed": 20,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 16
}