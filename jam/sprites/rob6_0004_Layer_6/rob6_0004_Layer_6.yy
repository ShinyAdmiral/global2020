{
    "id": "4689e760-df45-4199-a3d4-b2a577a2bcbd",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "rob6_0004_Layer_6",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 20,
    "bbox_left": 0,
    "bbox_right": 10,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 0,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "330e31e9-2a88-41e4-a135-daf9403345ee",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4689e760-df45-4199-a3d4-b2a577a2bcbd",
            "compositeImage": {
                "id": "286d93c5-1e40-460f-8063-b9f885b22635",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "330e31e9-2a88-41e4-a135-daf9403345ee",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d05e4ce6-f767-44d4-b374-5ffa0b44e807",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "330e31e9-2a88-41e4-a135-daf9403345ee",
                    "LayerId": "f2776457-6eb2-48b4-bcaa-f04494766d79"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 21,
    "layers": [
        {
            "id": "f2776457-6eb2-48b4-bcaa-f04494766d79",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "4689e760-df45-4199-a3d4-b2a577a2bcbd",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 11,
    "xorig": 6,
    "yorig": 2
}