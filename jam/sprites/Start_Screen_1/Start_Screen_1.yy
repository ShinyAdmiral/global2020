{
    "id": "77718bc0-8b04-4a20-934d-4947111d5abb",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "Start_Screen_1",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 113,
    "bbox_left": 132,
    "bbox_right": 186,
    "bbox_top": 84,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "9e779db9-17a1-4ef6-9b4f-5900c030859b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "77718bc0-8b04-4a20-934d-4947111d5abb",
            "compositeImage": {
                "id": "8562ba0f-7844-4870-b443-1092624faee4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9e779db9-17a1-4ef6-9b4f-5900c030859b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cf39aa06-4b62-4ed3-8fce-2f61d077ab11",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9e779db9-17a1-4ef6-9b4f-5900c030859b",
                    "LayerId": "5ccd14d5-2e5d-4650-a59c-25c3403aa727"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 180,
    "layers": [
        {
            "id": "5ccd14d5-2e5d-4650-a59c-25c3403aa727",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "77718bc0-8b04-4a20-934d-4947111d5abb",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 320,
    "xorig": 160,
    "yorig": 90
}