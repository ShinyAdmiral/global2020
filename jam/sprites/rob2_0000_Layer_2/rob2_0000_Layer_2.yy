{
    "id": "a7dbfc85-730f-445c-8d4b-0c24385c65f3",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "rob2_0000_Layer_2",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 39,
    "bbox_left": 0,
    "bbox_right": 24,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 0,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "1ee0985e-3683-4b1e-b83b-a35ccb4ae9d0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a7dbfc85-730f-445c-8d4b-0c24385c65f3",
            "compositeImage": {
                "id": "fc2e9194-20aa-4d0f-8908-d3c60b994311",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1ee0985e-3683-4b1e-b83b-a35ccb4ae9d0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "dedfa09d-fa44-4889-ae95-609a0c795ae3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1ee0985e-3683-4b1e-b83b-a35ccb4ae9d0",
                    "LayerId": "abbdcce7-93fe-4c38-83da-b29f6dae763f"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 40,
    "layers": [
        {
            "id": "abbdcce7-93fe-4c38-83da-b29f6dae763f",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "a7dbfc85-730f-445c-8d4b-0c24385c65f3",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 25,
    "xorig": 2,
    "yorig": 35
}