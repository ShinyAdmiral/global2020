{
    "id": "74a73b4e-8bb8-4b49-ba6f-d46681a0b845",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "rob6_0001_Layer_3",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 29,
    "bbox_left": 0,
    "bbox_right": 14,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 0,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "7aa7495f-b622-4c3b-bbbd-e643e9d2adc7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "74a73b4e-8bb8-4b49-ba6f-d46681a0b845",
            "compositeImage": {
                "id": "f0d4cea6-fcb8-46c2-bb3a-21c6ca1eeaa8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7aa7495f-b622-4c3b-bbbd-e643e9d2adc7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1333bd5a-613c-42dc-bac2-3dc58a17a663",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7aa7495f-b622-4c3b-bbbd-e643e9d2adc7",
                    "LayerId": "2552bcdb-d0be-4eb7-b0d9-1eafbd452229"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 30,
    "layers": [
        {
            "id": "2552bcdb-d0be-4eb7-b0d9-1eafbd452229",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "74a73b4e-8bb8-4b49-ba6f-d46681a0b845",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 15,
    "xorig": 2,
    "yorig": 2
}