{
    "id": "117b102d-3e6b-43ee-a2f0-295e0e958de9",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "rob7_0000_Layer_2",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 14,
    "bbox_left": 0,
    "bbox_right": 26,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 0,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "6c96e05e-c8d9-4956-8b28-fc31821f7371",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "117b102d-3e6b-43ee-a2f0-295e0e958de9",
            "compositeImage": {
                "id": "0162e0f8-6635-4157-8167-6194e14d16df",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6c96e05e-c8d9-4956-8b28-fc31821f7371",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "888165fa-290b-4db0-9d25-b761c2dc1fc8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6c96e05e-c8d9-4956-8b28-fc31821f7371",
                    "LayerId": "060f1b6a-1958-4f01-826d-4deed728aaea"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 15,
    "layers": [
        {
            "id": "060f1b6a-1958-4f01-826d-4deed728aaea",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "117b102d-3e6b-43ee-a2f0-295e0e958de9",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 5,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 27,
    "xorig": 26,
    "yorig": 7
}