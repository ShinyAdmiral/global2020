{
    "id": "4a152593-0928-4786-a3da-862161822fe0",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "big_laser",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "2c3dbda5-63b5-4e6f-8d0a-eff9c5647b90",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4a152593-0928-4786-a3da-862161822fe0",
            "compositeImage": {
                "id": "8a857173-5117-4042-abce-61da68a32865",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2c3dbda5-63b5-4e6f-8d0a-eff9c5647b90",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "50db2af1-bf5c-4393-a75f-2f426653655c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2c3dbda5-63b5-4e6f-8d0a-eff9c5647b90",
                    "LayerId": "bc9bf639-5e5f-47e3-ad0e-5018595f7987"
                }
            ]
        },
        {
            "id": "18644bd0-b6ce-4514-b4fa-28f0771f542d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4a152593-0928-4786-a3da-862161822fe0",
            "compositeImage": {
                "id": "a35a4d6c-a433-414b-a3e0-fc4d3bf66e06",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "18644bd0-b6ce-4514-b4fa-28f0771f542d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2d3a582f-5659-4264-9057-f58d05764e2b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "18644bd0-b6ce-4514-b4fa-28f0771f542d",
                    "LayerId": "bc9bf639-5e5f-47e3-ad0e-5018595f7987"
                }
            ]
        },
        {
            "id": "b84d7dd0-fb71-4436-8b4d-22379170b4b5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4a152593-0928-4786-a3da-862161822fe0",
            "compositeImage": {
                "id": "ca371df9-482e-4b0d-a969-afc15346d1e7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b84d7dd0-fb71-4436-8b4d-22379170b4b5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "063c3006-9ef2-47d4-a496-fd696babc0db",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b84d7dd0-fb71-4436-8b4d-22379170b4b5",
                    "LayerId": "bc9bf639-5e5f-47e3-ad0e-5018595f7987"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "bc9bf639-5e5f-47e3-ad0e-5018595f7987",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "4a152593-0928-4786-a3da-862161822fe0",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 0,
    "yorig": 0
}