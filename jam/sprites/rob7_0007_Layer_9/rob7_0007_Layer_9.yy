{
    "id": "b306bd2b-23e4-49ed-80b2-00340d14456b",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "rob7_0007_Layer_9",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 28,
    "bbox_left": 0,
    "bbox_right": 36,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 0,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "61c95cb3-c33c-4edf-8bc1-240b7b4563e1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b306bd2b-23e4-49ed-80b2-00340d14456b",
            "compositeImage": {
                "id": "57f4c588-d449-47cc-883b-57850dc255e1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "61c95cb3-c33c-4edf-8bc1-240b7b4563e1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "13b66392-14cd-4b59-b3bd-e1ce9adad7c1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "61c95cb3-c33c-4edf-8bc1-240b7b4563e1",
                    "LayerId": "20b53486-16b9-4641-88db-7273eb4973cf"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 29,
    "layers": [
        {
            "id": "20b53486-16b9-4641-88db-7273eb4973cf",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b306bd2b-23e4-49ed-80b2-00340d14456b",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 37,
    "xorig": 18,
    "yorig": 26
}