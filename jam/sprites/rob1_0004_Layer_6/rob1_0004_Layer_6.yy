{
    "id": "811cfb31-bcc8-40f2-bf84-fda7f7a5f601",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "rob1_0004_Layer_6",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 28,
    "bbox_left": 0,
    "bbox_right": 16,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 0,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "290831c0-7e35-46d6-bbbe-ae4ec509ad2e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "811cfb31-bcc8-40f2-bf84-fda7f7a5f601",
            "compositeImage": {
                "id": "daf0d80d-d63b-499a-a8ff-4e9762b5a4a2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "290831c0-7e35-46d6-bbbe-ae4ec509ad2e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b3aebccb-fe41-4bb3-be91-78fe83dc462d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "290831c0-7e35-46d6-bbbe-ae4ec509ad2e",
                    "LayerId": "1080a8fa-7be8-4e6a-b7b1-acd41a1a064d"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 29,
    "layers": [
        {
            "id": "1080a8fa-7be8-4e6a-b7b1-acd41a1a064d",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "811cfb31-bcc8-40f2-bf84-fda7f7a5f601",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 17,
    "xorig": 10,
    "yorig": 4
}