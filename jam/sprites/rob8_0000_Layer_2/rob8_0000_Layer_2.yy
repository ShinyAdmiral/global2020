{
    "id": "021c8cea-898c-4e2f-b972-0b8ec5524377",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "rob8_0000_Layer_2",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 22,
    "bbox_left": 0,
    "bbox_right": 28,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 0,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "6f03d346-c818-42d2-845e-70bd7e9c490d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "021c8cea-898c-4e2f-b972-0b8ec5524377",
            "compositeImage": {
                "id": "a84f1978-ded3-4185-8eb8-95bdb757d823",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6f03d346-c818-42d2-845e-70bd7e9c490d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a197e157-3228-4739-9573-38f867fbc687",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6f03d346-c818-42d2-845e-70bd7e9c490d",
                    "LayerId": "e029f73b-98e0-4ca9-b1c8-7d38411430a0"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 23,
    "layers": [
        {
            "id": "e029f73b-98e0-4ca9-b1c8-7d38411430a0",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "021c8cea-898c-4e2f-b972-0b8ec5524377",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 29,
    "xorig": 14,
    "yorig": 20
}