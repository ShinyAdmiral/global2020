{
    "id": "8eb56d7c-4cf2-4179-a2c5-b433a8760620",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "rob7_0005_Layer_7",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 0,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "bade1c36-0ba2-428d-8ebb-84458e0f77d7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8eb56d7c-4cf2-4179-a2c5-b433a8760620",
            "compositeImage": {
                "id": "9ec976f8-d25e-4058-8a49-d0cd3f7c1c2e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bade1c36-0ba2-428d-8ebb-84458e0f77d7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b66ecbcb-0f5b-4aff-9b33-304e6ce4de02",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bade1c36-0ba2-428d-8ebb-84458e0f77d7",
                    "LayerId": "0bf2b30d-d7f0-4294-ad46-2979831f203c"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "0bf2b30d-d7f0-4294-ad46-2979831f203c",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "8eb56d7c-4cf2-4179-a2c5-b433a8760620",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 11,
    "yorig": 0
}