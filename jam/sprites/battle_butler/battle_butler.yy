{
    "id": "1c0e8d77-8544-497b-af6b-6b91d5f29275",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "battle_butler",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 27,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "dd67b6c8-56cd-49d0-b669-d68ed9650df2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1c0e8d77-8544-497b-af6b-6b91d5f29275",
            "compositeImage": {
                "id": "00118f95-0bef-46f9-a809-27ecd2b0bac1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "dd67b6c8-56cd-49d0-b669-d68ed9650df2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ff102dd4-01b4-4e06-81a1-be507d8806f5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "dd67b6c8-56cd-49d0-b669-d68ed9650df2",
                    "LayerId": "334a15dd-38ef-4521-8bbf-5d412e7a0668"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 28,
    "layers": [
        {
            "id": "334a15dd-38ef-4521-8bbf-5d412e7a0668",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "1c0e8d77-8544-497b-af6b-6b91d5f29275",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 7,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 27
}