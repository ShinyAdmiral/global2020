{
    "id": "132e3d6f-bc3c-44c9-ba7a-6fbe946c5b5b",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "rob6_0003_Layer_5",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 40,
    "bbox_left": 0,
    "bbox_right": 30,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 0,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "98b29eaf-fb40-455e-bd94-99c9de7489e8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "132e3d6f-bc3c-44c9-ba7a-6fbe946c5b5b",
            "compositeImage": {
                "id": "ec215fcc-bb7c-4471-b773-7493fefcd1ce",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "98b29eaf-fb40-455e-bd94-99c9de7489e8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b630d634-84e4-4d5b-bc78-39c04974686e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "98b29eaf-fb40-455e-bd94-99c9de7489e8",
                    "LayerId": "02219685-d4f2-481f-8a81-1341f78a1ea8"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 41,
    "layers": [
        {
            "id": "02219685-d4f2-481f-8a81-1341f78a1ea8",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "132e3d6f-bc3c-44c9-ba7a-6fbe946c5b5b",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 31,
    "xorig": 15,
    "yorig": 38
}