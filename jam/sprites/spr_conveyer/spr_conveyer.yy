{
    "id": "c1a2f955-8265-495c-8b2c-e948f8ec9527",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_conveyer",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 34,
    "bbox_left": 0,
    "bbox_right": 86,
    "bbox_top": 0,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "ba3e37ec-6461-4812-b0bf-d647e17aa934",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c1a2f955-8265-495c-8b2c-e948f8ec9527",
            "compositeImage": {
                "id": "a3543721-de5f-4571-98a8-0315a0ffc35f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ba3e37ec-6461-4812-b0bf-d647e17aa934",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a9c5cf72-97f7-4450-b71e-9865b69c1a72",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ba3e37ec-6461-4812-b0bf-d647e17aa934",
                    "LayerId": "45b8e557-e245-49a9-b4b3-9d526778c432"
                }
            ]
        },
        {
            "id": "ab3fb606-e533-409f-bb20-cad296466e20",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c1a2f955-8265-495c-8b2c-e948f8ec9527",
            "compositeImage": {
                "id": "2bb70489-51ff-4931-94da-96c6d3a7edcb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ab3fb606-e533-409f-bb20-cad296466e20",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f4559f34-5c3c-4def-943d-56f3659df471",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ab3fb606-e533-409f-bb20-cad296466e20",
                    "LayerId": "45b8e557-e245-49a9-b4b3-9d526778c432"
                }
            ]
        },
        {
            "id": "995d7e27-614e-425f-ad82-fb22e0cabc1f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c1a2f955-8265-495c-8b2c-e948f8ec9527",
            "compositeImage": {
                "id": "5a69c113-5f79-4262-830f-3808ada971c7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "995d7e27-614e-425f-ad82-fb22e0cabc1f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b68f86b1-5a33-407e-9eae-79e49c014d81",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "995d7e27-614e-425f-ad82-fb22e0cabc1f",
                    "LayerId": "45b8e557-e245-49a9-b4b3-9d526778c432"
                }
            ]
        },
        {
            "id": "c0fa8e75-8d89-4965-a942-e97dd5eb7b50",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c1a2f955-8265-495c-8b2c-e948f8ec9527",
            "compositeImage": {
                "id": "8b47d7d4-df29-46b6-b245-dba4d2546216",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c0fa8e75-8d89-4965-a942-e97dd5eb7b50",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "69e0ffac-48d3-4668-8169-52d194d1acf4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c0fa8e75-8d89-4965-a942-e97dd5eb7b50",
                    "LayerId": "45b8e557-e245-49a9-b4b3-9d526778c432"
                }
            ]
        },
        {
            "id": "04aac7d7-0afa-481a-8385-8022dbb802f2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c1a2f955-8265-495c-8b2c-e948f8ec9527",
            "compositeImage": {
                "id": "48f64bdf-c56e-4ddb-9893-43ea573d45c8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "04aac7d7-0afa-481a-8385-8022dbb802f2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "daab448e-9f05-4564-bd55-7b71ec6798e2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "04aac7d7-0afa-481a-8385-8022dbb802f2",
                    "LayerId": "45b8e557-e245-49a9-b4b3-9d526778c432"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 40,
    "layers": [
        {
            "id": "45b8e557-e245-49a9-b4b3-9d526778c432",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "c1a2f955-8265-495c-8b2c-e948f8ec9527",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 13,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 100,
    "xorig": 28,
    "yorig": 3
}