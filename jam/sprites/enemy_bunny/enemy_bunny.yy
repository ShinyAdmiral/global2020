{
    "id": "e9405204-8ae5-473b-b13c-7d47e2e1f15d",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "enemy_bunny",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 23,
    "bbox_left": 0,
    "bbox_right": 13,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "e15cfb54-e12c-488b-9567-dab8c79066da",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e9405204-8ae5-473b-b13c-7d47e2e1f15d",
            "compositeImage": {
                "id": "c6c5a5fd-fc24-4d1a-907c-77b2548d08e0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e15cfb54-e12c-488b-9567-dab8c79066da",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9b33cceb-c72b-4237-87a6-7d91761f6015",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e15cfb54-e12c-488b-9567-dab8c79066da",
                    "LayerId": "577d4401-ba76-4fdf-b66c-462c4bda2391"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 24,
    "layers": [
        {
            "id": "577d4401-ba76-4fdf-b66c-462c4bda2391",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "e9405204-8ae5-473b-b13c-7d47e2e1f15d",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 7,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 14,
    "xorig": 7,
    "yorig": 23
}