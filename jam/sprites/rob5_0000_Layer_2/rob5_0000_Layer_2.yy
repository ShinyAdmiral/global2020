{
    "id": "c69106d7-04cf-4940-8037-640b890b0a10",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "rob5_0000_Layer_2",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 33,
    "bbox_left": 0,
    "bbox_right": 24,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 0,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "b9892f98-b7c9-452a-87a0-16f9eb458752",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c69106d7-04cf-4940-8037-640b890b0a10",
            "compositeImage": {
                "id": "f9a9935c-d5df-459c-9e68-20858d063eef",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b9892f98-b7c9-452a-87a0-16f9eb458752",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "522619f3-3955-41a4-8c1d-cc2af7f5dc22",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b9892f98-b7c9-452a-87a0-16f9eb458752",
                    "LayerId": "6c3e02cc-3b00-4229-b2b7-89d983f06c60"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 34,
    "layers": [
        {
            "id": "6c3e02cc-3b00-4229-b2b7-89d983f06c60",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "c69106d7-04cf-4940-8037-640b890b0a10",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 25,
    "xorig": 12,
    "yorig": 31
}