{
    "id": "99e68f67-7096-4e00-a5f9-533c44a69f13",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "rob5_0002_Layer_4",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 30,
    "bbox_left": 0,
    "bbox_right": 26,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 0,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "ca807d5f-358f-4547-b051-e4776a182a76",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "99e68f67-7096-4e00-a5f9-533c44a69f13",
            "compositeImage": {
                "id": "faaf5102-751f-47cc-88fc-54244646a667",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ca807d5f-358f-4547-b051-e4776a182a76",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a5e1376f-6973-4e46-b23a-05a271a578fe",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ca807d5f-358f-4547-b051-e4776a182a76",
                    "LayerId": "a0562e70-b1b4-4d44-bb46-1816a641a018"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 31,
    "layers": [
        {
            "id": "a0562e70-b1b4-4d44-bb46-1816a641a018",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "99e68f67-7096-4e00-a5f9-533c44a69f13",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 27,
    "xorig": 13,
    "yorig": 15
}