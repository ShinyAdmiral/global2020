{
    "id": "b3ddb6f7-cc72-4669-a8d3-cc8a6beabb3e",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "rob4_0000_Layer_2",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 32,
    "bbox_left": 0,
    "bbox_right": 42,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 0,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "ada1be37-2548-4b28-b27b-f960e69bcb03",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b3ddb6f7-cc72-4669-a8d3-cc8a6beabb3e",
            "compositeImage": {
                "id": "846a6694-ccf1-4799-ae87-7bddd6874c84",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ada1be37-2548-4b28-b27b-f960e69bcb03",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cbbecf71-62a4-4119-9a26-a272b692715e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ada1be37-2548-4b28-b27b-f960e69bcb03",
                    "LayerId": "78a97bf6-3583-43e7-bae3-c5c586cab7d2"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 33,
    "layers": [
        {
            "id": "78a97bf6-3583-43e7-bae3-c5c586cab7d2",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b3ddb6f7-cc72-4669-a8d3-cc8a6beabb3e",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 43,
    "xorig": 21,
    "yorig": 28
}