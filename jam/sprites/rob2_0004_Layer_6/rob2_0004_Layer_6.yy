{
    "id": "59ea9b1b-df96-4d25-baf3-3b28162290f3",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "rob2_0004_Layer_6",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 48,
    "bbox_left": 0,
    "bbox_right": 52,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 0,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "b35bd315-d950-4057-9917-5fbdce49be2b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "59ea9b1b-df96-4d25-baf3-3b28162290f3",
            "compositeImage": {
                "id": "a5806384-aa1b-4dc4-bdcc-f44e6a13a2ed",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b35bd315-d950-4057-9917-5fbdce49be2b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bf0dc5a5-c6af-407b-a16b-ae7852568e25",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b35bd315-d950-4057-9917-5fbdce49be2b",
                    "LayerId": "a1583398-0aab-400e-b5d3-06886d4eccd1"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 49,
    "layers": [
        {
            "id": "a1583398-0aab-400e-b5d3-06886d4eccd1",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "59ea9b1b-df96-4d25-baf3-3b28162290f3",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 53,
    "xorig": 26,
    "yorig": 24
}