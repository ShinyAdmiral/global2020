{
    "id": "418d9721-156e-4573-b8c5-decdaf77ab1a",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_test",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 28,
    "bbox_left": 0,
    "bbox_right": 23,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "c7fc70cc-ba96-442e-904c-df49991b4271",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "418d9721-156e-4573-b8c5-decdaf77ab1a",
            "compositeImage": {
                "id": "3c098903-b654-4ff9-b8e9-e84610f68bda",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c7fc70cc-ba96-442e-904c-df49991b4271",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "29e5f7ca-70df-49fe-9e17-925855939b1e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c7fc70cc-ba96-442e-904c-df49991b4271",
                    "LayerId": "6655c2e8-bb5b-49a7-95cf-91a831a2b486"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 29,
    "layers": [
        {
            "id": "6655c2e8-bb5b-49a7-95cf-91a831a2b486",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "418d9721-156e-4573-b8c5-decdaf77ab1a",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 7,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 24,
    "xorig": 12,
    "yorig": 28
}