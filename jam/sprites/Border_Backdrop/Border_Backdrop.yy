{
    "id": "db4834aa-a9d5-44b8-b6e8-39de859147e6",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "Border_Backdrop",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 203,
    "bbox_left": 10,
    "bbox_right": 138,
    "bbox_top": 25,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "619cb049-4db1-4c22-8dba-e575bccd6c35",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "db4834aa-a9d5-44b8-b6e8-39de859147e6",
            "compositeImage": {
                "id": "ec215014-c61b-470d-a623-9b401f72d6b8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "619cb049-4db1-4c22-8dba-e575bccd6c35",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7ef21e34-f189-4759-94c6-7e1f882a794f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "619cb049-4db1-4c22-8dba-e575bccd6c35",
                    "LayerId": "a26ada0a-6031-4d6c-94c1-2fcd68966ebc"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 220,
    "layers": [
        {
            "id": "a26ada0a-6031-4d6c-94c1-2fcd68966ebc",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "db4834aa-a9d5-44b8-b6e8-39de859147e6",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 400,
    "xorig": 0,
    "yorig": 0
}