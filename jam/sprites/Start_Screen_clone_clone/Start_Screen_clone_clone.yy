{
    "id": "cbf8d5fe-e9e7-44c4-9db1-9c900fd3cf0d",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "Start_Screen_clone_clone",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 113,
    "bbox_left": 111,
    "bbox_right": 186,
    "bbox_top": 84,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "06884696-f009-4017-bff1-23bf83c2b5f8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cbf8d5fe-e9e7-44c4-9db1-9c900fd3cf0d",
            "compositeImage": {
                "id": "5d50bd10-8e5d-4519-a1a9-cb893b5a5621",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "06884696-f009-4017-bff1-23bf83c2b5f8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0a365f59-e4c4-4afc-b056-2f63556dfd22",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "06884696-f009-4017-bff1-23bf83c2b5f8",
                    "LayerId": "5aa4fe88-33d1-4214-925c-bf38b48ab77d"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 180,
    "layers": [
        {
            "id": "5aa4fe88-33d1-4214-925c-bf38b48ab77d",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "cbf8d5fe-e9e7-44c4-9db1-9c900fd3cf0d",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 320,
    "xorig": 150,
    "yorig": 90
}