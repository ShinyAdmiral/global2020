{
    "id": "a04fd79e-0a7f-4a98-a456-b4c990151937",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "rob1_0000_Layer_2",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 14,
    "bbox_left": 0,
    "bbox_right": 27,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 0,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "b0bdd0d0-e1a1-45db-b33f-6d69618d91a0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a04fd79e-0a7f-4a98-a456-b4c990151937",
            "compositeImage": {
                "id": "3b917b65-1d40-494f-9ec0-e9d2cd793cc6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b0bdd0d0-e1a1-45db-b33f-6d69618d91a0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1761812f-3284-4d93-861a-0b5259700741",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b0bdd0d0-e1a1-45db-b33f-6d69618d91a0",
                    "LayerId": "68d49bca-0113-4960-9982-a3001bf6e65f"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 15,
    "layers": [
        {
            "id": "68d49bca-0113-4960-9982-a3001bf6e65f",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "a04fd79e-0a7f-4a98-a456-b4c990151937",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 28,
    "xorig": 23,
    "yorig": 7
}