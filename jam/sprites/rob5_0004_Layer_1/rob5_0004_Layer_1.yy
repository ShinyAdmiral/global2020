{
    "id": "bbffe98d-dc61-4037-b78a-25cf4fe14ffa",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "rob5_0004_Layer_1",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 28,
    "bbox_left": 0,
    "bbox_right": 18,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 0,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "a7843646-72b3-469c-8597-418a25d4f24e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bbffe98d-dc61-4037-b78a-25cf4fe14ffa",
            "compositeImage": {
                "id": "49464eba-f563-45f2-8147-d8675a15baf3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a7843646-72b3-469c-8597-418a25d4f24e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "464dc03b-a399-43df-9bd0-66cbc549fe55",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a7843646-72b3-469c-8597-418a25d4f24e",
                    "LayerId": "5b006796-7d39-44a0-84f7-d9de824bbcfb"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 29,
    "layers": [
        {
            "id": "5b006796-7d39-44a0-84f7-d9de824bbcfb",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "bbffe98d-dc61-4037-b78a-25cf4fe14ffa",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 19,
    "xorig": 9,
    "yorig": 2
}