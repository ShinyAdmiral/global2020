{
    "id": "48c98ed0-a296-431a-beab-e2873dca9f15",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "rob1_0005_Layer_7",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 28,
    "bbox_left": 0,
    "bbox_right": 16,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 0,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "8df02a0d-5cd1-4ca6-b529-6bcead4a4411",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "48c98ed0-a296-431a-beab-e2873dca9f15",
            "compositeImage": {
                "id": "c704e69f-210e-4748-9b87-4cc886eaf7ee",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8df02a0d-5cd1-4ca6-b529-6bcead4a4411",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "23144d06-3589-420b-ad12-f94c94b28dd7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8df02a0d-5cd1-4ca6-b529-6bcead4a4411",
                    "LayerId": "aed049ea-649e-42b3-bfff-445f2e3fe866"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 29,
    "layers": [
        {
            "id": "aed049ea-649e-42b3-bfff-445f2e3fe866",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "48c98ed0-a296-431a-beab-e2873dca9f15",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 17,
    "xorig": 6,
    "yorig": 4
}