{
    "id": "f619c0b8-316e-4571-9cb8-a5c4e9b50d1e",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "rob3_0000_Layer_2",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 28,
    "bbox_left": 0,
    "bbox_right": 42,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 0,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "263a4f34-03c8-42b5-b662-b7272a8c8633",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f619c0b8-316e-4571-9cb8-a5c4e9b50d1e",
            "compositeImage": {
                "id": "16b62822-7818-4202-9e77-f9b497d4d37b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "263a4f34-03c8-42b5-b662-b7272a8c8633",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "dedfb207-a374-4273-806c-3c1ac83585fe",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "263a4f34-03c8-42b5-b662-b7272a8c8633",
                    "LayerId": "a3acefd8-f315-4fd5-922a-5f007597970e"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 29,
    "layers": [
        {
            "id": "a3acefd8-f315-4fd5-922a-5f007597970e",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "f619c0b8-316e-4571-9cb8-a5c4e9b50d1e",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 43,
    "xorig": 21,
    "yorig": 24
}