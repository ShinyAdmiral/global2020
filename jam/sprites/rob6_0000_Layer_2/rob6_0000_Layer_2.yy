{
    "id": "7be6292f-8974-47ce-861f-669864888417",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "rob6_0000_Layer_2",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 29,
    "bbox_left": 0,
    "bbox_right": 14,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 0,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "bdae8502-91ef-4118-91b2-44e32ce3abf7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7be6292f-8974-47ce-861f-669864888417",
            "compositeImage": {
                "id": "0e7a7283-bab6-45b9-b023-b80cc9ecf7c0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bdae8502-91ef-4118-91b2-44e32ce3abf7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0490e929-f111-4539-8b5d-23a2de20ed93",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bdae8502-91ef-4118-91b2-44e32ce3abf7",
                    "LayerId": "b7fb3afa-bce7-450f-b065-93f3aaac8135"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 30,
    "layers": [
        {
            "id": "b7fb3afa-bce7-450f-b065-93f3aaac8135",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "7be6292f-8974-47ce-861f-669864888417",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 15,
    "xorig": 12,
    "yorig": 2
}