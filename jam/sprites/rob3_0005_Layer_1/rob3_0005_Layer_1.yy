{
    "id": "8732327b-e798-46cf-8ca7-0179322e0bbc",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "rob3_0005_Layer_1",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 28,
    "bbox_left": 0,
    "bbox_right": 16,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 0,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "058166e2-d19f-4f3f-bd2c-7ca58954c770",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8732327b-e798-46cf-8ca7-0179322e0bbc",
            "compositeImage": {
                "id": "02cb0b28-b15c-4442-9470-fa75ea1a986d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "058166e2-d19f-4f3f-bd2c-7ca58954c770",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "57215ec8-73ef-43a9-819e-33a9f866a5f6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "058166e2-d19f-4f3f-bd2c-7ca58954c770",
                    "LayerId": "8203e85b-006c-4861-9b8a-1447c681b8ff"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 29,
    "layers": [
        {
            "id": "8203e85b-006c-4861-9b8a-1447c681b8ff",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "8732327b-e798-46cf-8ca7-0179322e0bbc",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 17,
    "xorig": 6,
    "yorig": 4
}