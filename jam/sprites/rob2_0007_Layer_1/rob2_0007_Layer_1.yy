{
    "id": "a71ed4ce-8803-437d-963b-fbaafb20c034",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "rob2_0007_Layer_1",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 24,
    "bbox_left": 0,
    "bbox_right": 18,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 0,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "452fc930-ee58-4bc6-9127-e34ceeb1ebd2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a71ed4ce-8803-437d-963b-fbaafb20c034",
            "compositeImage": {
                "id": "63f3184d-e737-4bd3-8949-7232c8f39311",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "452fc930-ee58-4bc6-9127-e34ceeb1ebd2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8b85a036-30dd-4c90-937c-e96c4efaddd6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "452fc930-ee58-4bc6-9127-e34ceeb1ebd2",
                    "LayerId": "c7aad357-b578-4bd3-bea8-12433f834a09"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 25,
    "layers": [
        {
            "id": "c7aad357-b578-4bd3-bea8-12433f834a09",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "a71ed4ce-8803-437d-963b-fbaafb20c034",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 19,
    "xorig": 9,
    "yorig": 22
}