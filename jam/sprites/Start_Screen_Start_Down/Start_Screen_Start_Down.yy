{
    "id": "421bc795-27e4-4811-8655-90c4bb6071a5",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "Start_Screen_Start_Down",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 115,
    "bbox_left": 132,
    "bbox_right": 186,
    "bbox_top": 86,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "8997f8f9-311a-464c-82e2-2b2fbf22f471",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "421bc795-27e4-4811-8655-90c4bb6071a5",
            "compositeImage": {
                "id": "ea7525d0-020c-4b39-a18e-0366f06dcbc2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8997f8f9-311a-464c-82e2-2b2fbf22f471",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "18164d34-7df4-4df5-8139-ba94ce3acdd5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8997f8f9-311a-464c-82e2-2b2fbf22f471",
                    "LayerId": "a52be706-efcc-4410-a567-f8d452bb91c3"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 180,
    "layers": [
        {
            "id": "a52be706-efcc-4410-a567-f8d452bb91c3",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "421bc795-27e4-4811-8655-90c4bb6071a5",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 320,
    "xorig": 160,
    "yorig": 90
}