{
    "id": "0ff21ed1-53fe-46cf-bff4-2b8d2ecaf278",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "rob7_0001_Layer_3",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 14,
    "bbox_left": 0,
    "bbox_right": 26,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 0,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "97b2fdf8-66aa-4411-bfd2-f87c17e8e2d5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0ff21ed1-53fe-46cf-bff4-2b8d2ecaf278",
            "compositeImage": {
                "id": "b3cf8f97-9b22-4f39-bc1f-95b97af1114a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "97b2fdf8-66aa-4411-bfd2-f87c17e8e2d5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "aeb12a36-9850-4300-b73b-85e98115d261",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "97b2fdf8-66aa-4411-bfd2-f87c17e8e2d5",
                    "LayerId": "c8917bf0-28af-48f2-aa5c-50393cb01e94"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 15,
    "layers": [
        {
            "id": "c8917bf0-28af-48f2-aa5c-50393cb01e94",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "0ff21ed1-53fe-46cf-bff4-2b8d2ecaf278",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 3,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 27,
    "xorig": 0,
    "yorig": 7
}