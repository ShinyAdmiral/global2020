{
    "id": "82c33977-7f37-441b-ae6d-bd8fbef96c4d",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_battlefield",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 187,
    "bbox_left": 0,
    "bbox_right": 236,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "3c4da02f-ca70-4d67-9640-fb101ee88454",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "82c33977-7f37-441b-ae6d-bd8fbef96c4d",
            "compositeImage": {
                "id": "e663d2e4-fe62-461d-8dfd-8fbbbd52e668",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3c4da02f-ca70-4d67-9640-fb101ee88454",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5d463f90-12d3-4af8-bf06-7ce36abd1ccd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3c4da02f-ca70-4d67-9640-fb101ee88454",
                    "LayerId": "3f6b3be8-4d64-4496-a683-052d1570c334"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 188,
    "layers": [
        {
            "id": "3f6b3be8-4d64-4496-a683-052d1570c334",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "82c33977-7f37-441b-ae6d-bd8fbef96c4d",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 237,
    "xorig": 0,
    "yorig": 0
}