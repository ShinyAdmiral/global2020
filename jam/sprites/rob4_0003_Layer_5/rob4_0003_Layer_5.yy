{
    "id": "9d55e109-9275-46e6-979e-349c79682995",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "rob4_0003_Layer_5",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 51,
    "bbox_left": 0,
    "bbox_right": 74,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 0,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "a406905e-9dad-49f2-9395-93b0105067d0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9d55e109-9275-46e6-979e-349c79682995",
            "compositeImage": {
                "id": "e587b755-153c-44c8-a139-08557c2995b1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a406905e-9dad-49f2-9395-93b0105067d0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "895c5052-8a78-461f-a99b-8711a7b72b42",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a406905e-9dad-49f2-9395-93b0105067d0",
                    "LayerId": "405be12a-49f9-47f1-afd2-3318e08a8cbb"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 52,
    "layers": [
        {
            "id": "405be12a-49f9-47f1-afd2-3318e08a8cbb",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "9d55e109-9275-46e6-979e-349c79682995",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 75,
    "xorig": 37,
    "yorig": 26
}