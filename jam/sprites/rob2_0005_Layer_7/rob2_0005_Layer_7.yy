{
    "id": "14b1e33e-a11f-41d8-a1cf-4e856554582e",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "rob2_0005_Layer_7",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 27,
    "bbox_left": 0,
    "bbox_right": 10,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 0,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "f3fe5659-57b6-413d-96e1-a1971689d507",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "14b1e33e-a11f-41d8-a1cf-4e856554582e",
            "compositeImage": {
                "id": "45e5a1f3-be36-4300-a4c8-061be138361e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f3fe5659-57b6-413d-96e1-a1971689d507",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ef19f1e3-6dee-4cc2-9d23-9678c0c9070a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f3fe5659-57b6-413d-96e1-a1971689d507",
                    "LayerId": "5e9dfcc1-6105-42d3-abd4-3f0b1ab2b9fe"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 28,
    "layers": [
        {
            "id": "5e9dfcc1-6105-42d3-abd4-3f0b1ab2b9fe",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "14b1e33e-a11f-41d8-a1cf-4e856554582e",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 11,
    "xorig": 7,
    "yorig": 2
}