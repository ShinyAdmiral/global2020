{
    "id": "50d7122f-196a-4077-a6c9-77d8d84effc8",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "rob2_0001_Layer_3",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 39,
    "bbox_left": 0,
    "bbox_right": 24,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 0,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "6fbf9a75-81fe-428e-9eef-b048d2762bf6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "50d7122f-196a-4077-a6c9-77d8d84effc8",
            "compositeImage": {
                "id": "87fca3d2-3ac3-4bab-b9fe-8a0293081f2e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6fbf9a75-81fe-428e-9eef-b048d2762bf6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "838d229d-2c3f-4593-9ae3-1417f50c54fd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6fbf9a75-81fe-428e-9eef-b048d2762bf6",
                    "LayerId": "14ac7bfa-26a7-46ac-ac84-572669a11002"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 40,
    "layers": [
        {
            "id": "14ac7bfa-26a7-46ac-ac84-572669a11002",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "50d7122f-196a-4077-a6c9-77d8d84effc8",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 25,
    "xorig": 2,
    "yorig": 4
}