{
    "id": "a1f1c320-0449-4437-8916-1c49da08edaa",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "laser_particle",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 9,
    "bbox_left": 0,
    "bbox_right": 9,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "1cfcf4c5-06ed-468a-9195-dd9ca6592dac",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a1f1c320-0449-4437-8916-1c49da08edaa",
            "compositeImage": {
                "id": "778d3cfa-4da3-41f7-8c54-0587dac0950e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1cfcf4c5-06ed-468a-9195-dd9ca6592dac",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9f7b2c4c-ec35-4480-9f71-86c90b726187",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1cfcf4c5-06ed-468a-9195-dd9ca6592dac",
                    "LayerId": "b24c87b6-8900-4aa3-a000-baccaf7ae069"
                }
            ]
        },
        {
            "id": "ec6a4a12-2001-4d9d-913a-cd3ec9cf2ae0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a1f1c320-0449-4437-8916-1c49da08edaa",
            "compositeImage": {
                "id": "54de5c7e-c66d-4d98-b4cf-8507c60be581",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ec6a4a12-2001-4d9d-913a-cd3ec9cf2ae0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d16077fb-0c73-4511-9083-42e37bb7bd0a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ec6a4a12-2001-4d9d-913a-cd3ec9cf2ae0",
                    "LayerId": "b24c87b6-8900-4aa3-a000-baccaf7ae069"
                }
            ]
        },
        {
            "id": "fe6bb3d0-ac85-4178-8e96-2fcd79e2bc88",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a1f1c320-0449-4437-8916-1c49da08edaa",
            "compositeImage": {
                "id": "1f3974c4-ebb5-4c8d-a451-8cef6b16e72d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fe6bb3d0-ac85-4178-8e96-2fcd79e2bc88",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0787a157-a661-48a0-87df-d5b67ab34e43",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fe6bb3d0-ac85-4178-8e96-2fcd79e2bc88",
                    "LayerId": "b24c87b6-8900-4aa3-a000-baccaf7ae069"
                }
            ]
        },
        {
            "id": "2c2aaae7-c09f-4ed0-8b54-2e424eafe977",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a1f1c320-0449-4437-8916-1c49da08edaa",
            "compositeImage": {
                "id": "5ca5d8f4-917d-4c0e-bf0d-18e75369cba4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2c2aaae7-c09f-4ed0-8b54-2e424eafe977",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d4ca13a5-8f78-450b-adc7-f9e9facb8d6e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2c2aaae7-c09f-4ed0-8b54-2e424eafe977",
                    "LayerId": "b24c87b6-8900-4aa3-a000-baccaf7ae069"
                }
            ]
        },
        {
            "id": "103c7817-6017-45bb-955a-b34834bbd635",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a1f1c320-0449-4437-8916-1c49da08edaa",
            "compositeImage": {
                "id": "ef1b51da-430a-4043-a456-747360c1d6c6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "103c7817-6017-45bb-955a-b34834bbd635",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fbda61b8-839c-402d-9007-08bcb2aa4251",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "103c7817-6017-45bb-955a-b34834bbd635",
                    "LayerId": "b24c87b6-8900-4aa3-a000-baccaf7ae069"
                }
            ]
        },
        {
            "id": "fcbd4386-97f6-4e1d-b0f8-769dad932521",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a1f1c320-0449-4437-8916-1c49da08edaa",
            "compositeImage": {
                "id": "aaaed85a-23dc-49dd-a805-01eb42ca78ad",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fcbd4386-97f6-4e1d-b0f8-769dad932521",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "64ad7e95-cc78-4eac-9ab9-7d8d5d809400",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fcbd4386-97f6-4e1d-b0f8-769dad932521",
                    "LayerId": "b24c87b6-8900-4aa3-a000-baccaf7ae069"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "b24c87b6-8900-4aa3-a000-baccaf7ae069",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "a1f1c320-0449-4437-8916-1c49da08edaa",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 0,
    "yorig": 0
}