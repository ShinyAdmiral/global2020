{
    "id": "9f833efb-e358-4705-b4c7-4bc235ed825f",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_bulb",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 27,
    "bbox_left": 0,
    "bbox_right": 14,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "7b3debd2-357f-4d84-9b7d-6042fb7ac9e3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9f833efb-e358-4705-b4c7-4bc235ed825f",
            "compositeImage": {
                "id": "571fa30e-fc87-472f-a8e4-f2a690dcd053",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7b3debd2-357f-4d84-9b7d-6042fb7ac9e3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e231b05e-c12c-446c-846a-5e5cdd47f2b0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7b3debd2-357f-4d84-9b7d-6042fb7ac9e3",
                    "LayerId": "b0069cf4-bf95-4a33-882d-5b29e1629d8f"
                }
            ]
        },
        {
            "id": "18cbc100-cd91-49b3-9cb7-66e062b68b7f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9f833efb-e358-4705-b4c7-4bc235ed825f",
            "compositeImage": {
                "id": "001fef4a-6f9f-4f9c-ad4a-136c462c7493",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "18cbc100-cd91-49b3-9cb7-66e062b68b7f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "569515cb-5d4f-4451-8218-cb138a897ce0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "18cbc100-cd91-49b3-9cb7-66e062b68b7f",
                    "LayerId": "b0069cf4-bf95-4a33-882d-5b29e1629d8f"
                }
            ]
        },
        {
            "id": "d0810fac-a343-4792-b80d-7e83326b815a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9f833efb-e358-4705-b4c7-4bc235ed825f",
            "compositeImage": {
                "id": "aed1cb58-b159-4f17-a491-762cc999e776",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d0810fac-a343-4792-b80d-7e83326b815a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "73b9e866-13e6-42a0-b201-7f8c8b1e2604",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d0810fac-a343-4792-b80d-7e83326b815a",
                    "LayerId": "b0069cf4-bf95-4a33-882d-5b29e1629d8f"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 28,
    "layers": [
        {
            "id": "b0069cf4-bf95-4a33-882d-5b29e1629d8f",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "9f833efb-e358-4705-b4c7-4bc235ed825f",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 3,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 15,
    "xorig": 0,
    "yorig": 0
}