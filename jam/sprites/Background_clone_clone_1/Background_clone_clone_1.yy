{
    "id": "8bb2c4c1-2daa-4ea2-8af1-987902063a5c",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "Background_clone_clone_1",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 32,
    "bbox_left": 12,
    "bbox_right": 24,
    "bbox_top": 27,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "385065c1-b0c6-4a29-9bb7-3786d102ed95",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8bb2c4c1-2daa-4ea2-8af1-987902063a5c",
            "compositeImage": {
                "id": "ff7a1aca-5fdf-414f-9ed3-f999a7926a81",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "385065c1-b0c6-4a29-9bb7-3786d102ed95",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e950a273-8e18-4478-b4cd-10dcc3dfa33c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "385065c1-b0c6-4a29-9bb7-3786d102ed95",
                    "LayerId": "0aa61d32-a843-452f-8f48-a99d8fa59a61"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 220,
    "layers": [
        {
            "id": "0aa61d32-a843-452f-8f48-a99d8fa59a61",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "8bb2c4c1-2daa-4ea2-8af1-987902063a5c",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 400,
    "xorig": 0,
    "yorig": 0
}