{
    "id": "78bee023-96f9-4976-a6b9-45212500e753",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_test2",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 28,
    "bbox_left": 0,
    "bbox_right": 23,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "c59f1bde-5ecf-4f16-baaa-449dc1a53c0f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "78bee023-96f9-4976-a6b9-45212500e753",
            "compositeImage": {
                "id": "041b8eda-533e-4154-ab3d-fb6276326f66",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c59f1bde-5ecf-4f16-baaa-449dc1a53c0f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "12b5b652-8960-4ab3-9ed4-11bb3b177604",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c59f1bde-5ecf-4f16-baaa-449dc1a53c0f",
                    "LayerId": "83be7eb6-9f44-4fdc-8a74-9fd896ba886e"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 29,
    "layers": [
        {
            "id": "83be7eb6-9f44-4fdc-8a74-9fd896ba886e",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "78bee023-96f9-4976-a6b9-45212500e753",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 7,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 24,
    "xorig": 12,
    "yorig": 28
}