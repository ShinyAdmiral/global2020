{
    "id": "54afa862-f8af-4f16-bab3-7719d59fd1ad",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "jab",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 18,
    "bbox_left": 0,
    "bbox_right": 20,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "d229648c-fa8a-406c-9afe-f68394de7c44",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "54afa862-f8af-4f16-bab3-7719d59fd1ad",
            "compositeImage": {
                "id": "5f2a7766-d3c2-4d96-971c-f878735ee4e9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d229648c-fa8a-406c-9afe-f68394de7c44",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8f36c4fd-cff4-4471-ad2e-c1dc6d8c9efe",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d229648c-fa8a-406c-9afe-f68394de7c44",
                    "LayerId": "7502e00e-f802-4ea7-9bca-e108bf2788c1"
                }
            ]
        },
        {
            "id": "ec7202ec-075a-4d16-860f-70ca33617b7d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "54afa862-f8af-4f16-bab3-7719d59fd1ad",
            "compositeImage": {
                "id": "39e3aab2-c416-4f11-8edc-2c695b2e775d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ec7202ec-075a-4d16-860f-70ca33617b7d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8eb27489-aa1c-4836-b8fe-cb98f4835021",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ec7202ec-075a-4d16-860f-70ca33617b7d",
                    "LayerId": "7502e00e-f802-4ea7-9bca-e108bf2788c1"
                }
            ]
        },
        {
            "id": "3de4e7b3-a7dc-42a5-bbf1-e22c10af354b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "54afa862-f8af-4f16-bab3-7719d59fd1ad",
            "compositeImage": {
                "id": "e3f5d8f7-cb5d-4018-bccb-c11cdbffffc2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3de4e7b3-a7dc-42a5-bbf1-e22c10af354b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "90dfa766-3519-4488-a84a-420d625fd491",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3de4e7b3-a7dc-42a5-bbf1-e22c10af354b",
                    "LayerId": "7502e00e-f802-4ea7-9bca-e108bf2788c1"
                }
            ]
        },
        {
            "id": "6ba377ed-a11a-4f8a-adae-8b379a7bf104",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "54afa862-f8af-4f16-bab3-7719d59fd1ad",
            "compositeImage": {
                "id": "49abce26-e1e9-4405-9f33-79751b15a129",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6ba377ed-a11a-4f8a-adae-8b379a7bf104",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "00e6c446-a6f5-4244-ac1f-67a034038aef",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6ba377ed-a11a-4f8a-adae-8b379a7bf104",
                    "LayerId": "7502e00e-f802-4ea7-9bca-e108bf2788c1"
                }
            ]
        },
        {
            "id": "bf337713-0952-486d-a18f-450c085e5f53",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "54afa862-f8af-4f16-bab3-7719d59fd1ad",
            "compositeImage": {
                "id": "8a2c78e4-f22d-48e7-adff-a7e7198fe9a2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bf337713-0952-486d-a18f-450c085e5f53",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5828a52d-d256-4ac7-bc92-ab4639424dc4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bf337713-0952-486d-a18f-450c085e5f53",
                    "LayerId": "7502e00e-f802-4ea7-9bca-e108bf2788c1"
                }
            ]
        },
        {
            "id": "aa10bbb6-4d23-478a-81f3-c51cf2fe09da",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "54afa862-f8af-4f16-bab3-7719d59fd1ad",
            "compositeImage": {
                "id": "b3052357-fb7f-4d25-8ec4-b80a60b3de52",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "aa10bbb6-4d23-478a-81f3-c51cf2fe09da",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ed121bb8-7ac7-4246-a1a9-30311b3b997f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "aa10bbb6-4d23-478a-81f3-c51cf2fe09da",
                    "LayerId": "7502e00e-f802-4ea7-9bca-e108bf2788c1"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "7502e00e-f802-4ea7-9bca-e108bf2788c1",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "54afa862-f8af-4f16-bab3-7719d59fd1ad",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 3,
    "originLocked": false,
    "playbackSpeed": 20,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 24,
    "xorig": 0,
    "yorig": 16
}