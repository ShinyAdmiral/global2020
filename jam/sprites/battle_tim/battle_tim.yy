{
    "id": "eaecb8c0-1330-412e-b7c5-8c66a8591104",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "battle_tim",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 21,
    "bbox_left": 0,
    "bbox_right": 19,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "0ea58356-f479-4222-84c9-fa10d1bb0194",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "eaecb8c0-1330-412e-b7c5-8c66a8591104",
            "compositeImage": {
                "id": "51825ccc-2068-4fef-a6fb-4bfee1ac68e2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0ea58356-f479-4222-84c9-fa10d1bb0194",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3dd050d6-bc04-44b8-9ba3-5ca7782a0d27",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0ea58356-f479-4222-84c9-fa10d1bb0194",
                    "LayerId": "1c9d624b-1ae8-483a-a0e1-4522adec8bf6"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 22,
    "layers": [
        {
            "id": "1c9d624b-1ae8-483a-a0e1-4522adec8bf6",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "eaecb8c0-1330-412e-b7c5-8c66a8591104",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 7,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 20,
    "xorig": 10,
    "yorig": 21
}