{
    "id": "e9c661ae-f3f7-47cf-b02a-2a9d7fd0e8db",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprite4",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "0a91530c-4e45-45aa-b2be-264fd9cb3526",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e9c661ae-f3f7-47cf-b02a-2a9d7fd0e8db",
            "compositeImage": {
                "id": "f8d7c66c-8a0c-44ca-bc64-17bea284b735",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0a91530c-4e45-45aa-b2be-264fd9cb3526",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2601aadc-67fd-4108-b150-97495ebed9a0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0a91530c-4e45-45aa-b2be-264fd9cb3526",
                    "LayerId": "358d2874-55e9-4971-8620-09a545f5dbc6"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "358d2874-55e9-4971-8620-09a545f5dbc6",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "e9c661ae-f3f7-47cf-b02a-2a9d7fd0e8db",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 0,
    "yorig": 0
}