{
    "id": "085713ca-6042-463b-8853-89bcc7893e12",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "rob4_0005_Layer_1",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 18,
    "bbox_left": 0,
    "bbox_right": 10,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 0,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "64a5b4e5-a09d-4afa-9ec2-d33e9058f170",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "085713ca-6042-463b-8853-89bcc7893e12",
            "compositeImage": {
                "id": "42fb605b-bc51-434c-80b1-45a44f0a445c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "64a5b4e5-a09d-4afa-9ec2-d33e9058f170",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "76bf4d15-3c71-4b4e-9438-99708ddaa42f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "64a5b4e5-a09d-4afa-9ec2-d33e9058f170",
                    "LayerId": "c3f5ebe8-64f5-4edd-a6ae-7870ff5e7875"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 19,
    "layers": [
        {
            "id": "c3f5ebe8-64f5-4edd-a6ae-7870ff5e7875",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "085713ca-6042-463b-8853-89bcc7893e12",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 11,
    "xorig": 3,
    "yorig": 2
}