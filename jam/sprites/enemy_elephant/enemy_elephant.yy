{
    "id": "252ae9d0-db8f-4552-9df6-9e02d15d9cd9",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "enemy_elephant",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 29,
    "bbox_left": 0,
    "bbox_right": 43,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "e8ad55e8-9c43-4256-8f1c-991a352348ec",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "252ae9d0-db8f-4552-9df6-9e02d15d9cd9",
            "compositeImage": {
                "id": "72b1565a-f709-4bc1-9817-3455dcd24a57",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e8ad55e8-9c43-4256-8f1c-991a352348ec",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "23aac341-f423-48bf-985d-ad74498ebabc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e8ad55e8-9c43-4256-8f1c-991a352348ec",
                    "LayerId": "14b24979-0618-4d0c-8481-ee8e09c68127"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 30,
    "layers": [
        {
            "id": "14b24979-0618-4d0c-8481-ee8e09c68127",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "252ae9d0-db8f-4552-9df6-9e02d15d9cd9",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 7,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 44,
    "xorig": 22,
    "yorig": 29
}