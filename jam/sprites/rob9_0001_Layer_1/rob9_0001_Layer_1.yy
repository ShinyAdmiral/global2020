{
    "id": "cb2645cb-d7ba-4a83-97bd-207cf82515a9",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "rob9_0001_Layer_1",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 10,
    "bbox_left": 0,
    "bbox_right": 18,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 0,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "f929833c-842f-4c9a-b0a7-17ce3a370334",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cb2645cb-d7ba-4a83-97bd-207cf82515a9",
            "compositeImage": {
                "id": "81bbf48c-072e-43eb-a863-719990bb4a0a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f929833c-842f-4c9a-b0a7-17ce3a370334",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ef1d112a-fdc2-4354-99f1-5362893dad88",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f929833c-842f-4c9a-b0a7-17ce3a370334",
                    "LayerId": "f186f076-bbfa-4ad4-a00c-64e0ea5fc75f"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 11,
    "layers": [
        {
            "id": "f186f076-bbfa-4ad4-a00c-64e0ea5fc75f",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "cb2645cb-d7ba-4a83-97bd-207cf82515a9",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 19,
    "xorig": 9,
    "yorig": 2
}