{
    "id": "124cc849-f81e-42ba-9324-84e235120de5",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "rob4_0001_Layer_3",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 37,
    "bbox_left": 0,
    "bbox_right": 27,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 0,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "25589707-f877-487d-aec4-6eb8e4175483",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "124cc849-f81e-42ba-9324-84e235120de5",
            "compositeImage": {
                "id": "0856bcdb-5261-41db-8ee3-66f97b867620",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "25589707-f877-487d-aec4-6eb8e4175483",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8530ccf7-af19-453f-832c-964d39dd23f1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "25589707-f877-487d-aec4-6eb8e4175483",
                    "LayerId": "f05963d4-3c23-4380-90b0-be584cf08c56"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 38,
    "layers": [
        {
            "id": "f05963d4-3c23-4380-90b0-be584cf08c56",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "124cc849-f81e-42ba-9324-84e235120de5",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 28,
    "xorig": 13,
    "yorig": 2
}