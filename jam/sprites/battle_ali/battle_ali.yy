{
    "id": "681d5c67-13b2-4788-aa91-ab360fe6da87",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "battle_ali",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 33,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "9f9d5024-2695-4867-9165-f81686f46a66",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "681d5c67-13b2-4788-aa91-ab360fe6da87",
            "compositeImage": {
                "id": "e0911b2c-2e4e-49cb-91a4-a43ee53dee1c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9f9d5024-2695-4867-9165-f81686f46a66",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3e20e94b-acb8-4b9d-874c-2711789db6e2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9f9d5024-2695-4867-9165-f81686f46a66",
                    "LayerId": "fa6ae99c-b462-46d9-96fe-0dc95b67f603"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "fa6ae99c-b462-46d9-96fe-0dc95b67f603",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "681d5c67-13b2-4788-aa91-ab360fe6da87",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 7,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 34,
    "xorig": 17,
    "yorig": 31
}