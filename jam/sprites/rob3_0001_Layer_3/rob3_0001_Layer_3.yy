{
    "id": "0408f3ba-eb7d-457f-b372-c5d69f2936a3",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "rob3_0001_Layer_3",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 8,
    "bbox_left": 0,
    "bbox_right": 25,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 0,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "7f410962-cee4-48b4-9ce4-75ea8b9b8734",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0408f3ba-eb7d-457f-b372-c5d69f2936a3",
            "compositeImage": {
                "id": "e5834264-448c-447a-b719-e3c1eb18e28f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7f410962-cee4-48b4-9ce4-75ea8b9b8734",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "da666151-a755-4179-a7a0-ddfe7b0a1636",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7f410962-cee4-48b4-9ce4-75ea8b9b8734",
                    "LayerId": "271a5f81-d8cb-422f-abe6-6b127c16ffcd"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 9,
    "layers": [
        {
            "id": "271a5f81-d8cb-422f-abe6-6b127c16ffcd",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "0408f3ba-eb7d-457f-b372-c5d69f2936a3",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 26,
    "xorig": 4,
    "yorig": 4
}