{
    "id": "78016081-3531-4435-b71a-8f5dbbc15e71",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "battle_deathro",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 37,
    "bbox_left": 0,
    "bbox_right": 11,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "75c1d28b-54d9-4c4a-856d-b4c46c0507dc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "78016081-3531-4435-b71a-8f5dbbc15e71",
            "compositeImage": {
                "id": "a3b0553f-af70-497a-a052-3b515b849b06",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "75c1d28b-54d9-4c4a-856d-b4c46c0507dc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9b679409-cba0-4f4b-a91f-407ff0faf7e9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "75c1d28b-54d9-4c4a-856d-b4c46c0507dc",
                    "LayerId": "15328a2b-a6e1-4285-9d44-c04e1dcb6154"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 38,
    "layers": [
        {
            "id": "15328a2b-a6e1-4285-9d44-c04e1dcb6154",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "78016081-3531-4435-b71a-8f5dbbc15e71",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 7,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 12,
    "xorig": 6,
    "yorig": 37
}