{
    "id": "726fb39a-54d0-4ff0-a4dc-9f3a7fe4fabd",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "rob6_0005_Layer_1",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 20,
    "bbox_left": 0,
    "bbox_right": 10,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 0,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "28751cf2-9b88-434f-91ce-d7d080d520df",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "726fb39a-54d0-4ff0-a4dc-9f3a7fe4fabd",
            "compositeImage": {
                "id": "d252026c-87df-4bf6-b883-4065b9af8881",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "28751cf2-9b88-434f-91ce-d7d080d520df",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5fbccf3c-e3af-4489-aa72-d65ec5af4bfd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "28751cf2-9b88-434f-91ce-d7d080d520df",
                    "LayerId": "afde8974-4611-4507-81cb-ee229b99c452"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 21,
    "layers": [
        {
            "id": "afde8974-4611-4507-81cb-ee229b99c452",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "726fb39a-54d0-4ff0-a4dc-9f3a7fe4fabd",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 11,
    "xorig": 4,
    "yorig": 2
}