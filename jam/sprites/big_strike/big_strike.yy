{
    "id": "cbfb92cb-849a-4c94-83d1-37db7e618842",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "big_strike",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 18,
    "bbox_left": 0,
    "bbox_right": 55,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "1d989296-55d7-4333-92ea-a20ae52cbe5b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cbfb92cb-849a-4c94-83d1-37db7e618842",
            "compositeImage": {
                "id": "4a5fe9d9-5405-4c77-8c05-4a769ba84fa6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1d989296-55d7-4333-92ea-a20ae52cbe5b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1945cabb-7d30-4051-a381-3e74d4354d60",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1d989296-55d7-4333-92ea-a20ae52cbe5b",
                    "LayerId": "cc0352f4-29f4-4067-8236-ed5d21ae1872"
                }
            ]
        },
        {
            "id": "46da1908-308d-4e79-b1a1-9d84713ecdca",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cbfb92cb-849a-4c94-83d1-37db7e618842",
            "compositeImage": {
                "id": "fd4dd698-2bfb-42d5-ae7a-bb35ac70bdd7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "46da1908-308d-4e79-b1a1-9d84713ecdca",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4aee283a-59f6-4b83-8084-cf95a7ae0bea",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "46da1908-308d-4e79-b1a1-9d84713ecdca",
                    "LayerId": "cc0352f4-29f4-4067-8236-ed5d21ae1872"
                }
            ]
        },
        {
            "id": "39a0a3c8-5123-43a6-8d99-56d78038b113",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cbfb92cb-849a-4c94-83d1-37db7e618842",
            "compositeImage": {
                "id": "cf428cea-d2df-4dc6-b69d-2a4c096f251d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "39a0a3c8-5123-43a6-8d99-56d78038b113",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "75d9ce44-e333-4262-b9bf-ae6824f0a145",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "39a0a3c8-5123-43a6-8d99-56d78038b113",
                    "LayerId": "cc0352f4-29f4-4067-8236-ed5d21ae1872"
                }
            ]
        },
        {
            "id": "aa837ae4-081b-4549-8726-2739a99e2387",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cbfb92cb-849a-4c94-83d1-37db7e618842",
            "compositeImage": {
                "id": "89dca111-1493-4f16-8c02-01025b0f1822",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "aa837ae4-081b-4549-8726-2739a99e2387",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1e0c795f-4f0a-495a-8e5d-7dbb6849da2a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "aa837ae4-081b-4549-8726-2739a99e2387",
                    "LayerId": "cc0352f4-29f4-4067-8236-ed5d21ae1872"
                }
            ]
        },
        {
            "id": "935adee1-8970-4359-8911-ef7be8ee4c89",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cbfb92cb-849a-4c94-83d1-37db7e618842",
            "compositeImage": {
                "id": "e4bd2922-26c8-45ce-b446-da6dca201200",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "935adee1-8970-4359-8911-ef7be8ee4c89",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a31b8fc2-51a6-483c-bf6a-b62b8e6bc66c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "935adee1-8970-4359-8911-ef7be8ee4c89",
                    "LayerId": "cc0352f4-29f4-4067-8236-ed5d21ae1872"
                }
            ]
        },
        {
            "id": "4c88bb70-f130-45d7-90a2-7f8a5dda8706",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cbfb92cb-849a-4c94-83d1-37db7e618842",
            "compositeImage": {
                "id": "62d7f5ba-a6da-4c7f-b2dc-0f3cfa5cc30b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4c88bb70-f130-45d7-90a2-7f8a5dda8706",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b8622c74-10db-4bf7-a7b1-28160f1b2609",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4c88bb70-f130-45d7-90a2-7f8a5dda8706",
                    "LayerId": "cc0352f4-29f4-4067-8236-ed5d21ae1872"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "cc0352f4-29f4-4067-8236-ed5d21ae1872",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "cbfb92cb-849a-4c94-83d1-37db7e618842",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 20,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 0,
    "yorig": 0
}