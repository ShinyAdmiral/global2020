{
    "id": "e671db2e-8611-43f6-9206-d5383f456ea9",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_Great",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 34,
    "bbox_left": 2,
    "bbox_right": 78,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "7251f1af-ad4f-484b-8e9d-7db0752c702f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e671db2e-8611-43f6-9206-d5383f456ea9",
            "compositeImage": {
                "id": "164aeba6-fece-4039-baa3-db83f14aa2ad",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7251f1af-ad4f-484b-8e9d-7db0752c702f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4d19b623-c126-4561-92dd-96484cf61ecd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7251f1af-ad4f-484b-8e9d-7db0752c702f",
                    "LayerId": "bdf575df-b4ef-4411-8b3d-a99c9a683e59"
                }
            ]
        },
        {
            "id": "63b3bf1c-055b-438f-b71b-2052728f9ca4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e671db2e-8611-43f6-9206-d5383f456ea9",
            "compositeImage": {
                "id": "e761a976-86de-46fe-b3d0-37648e70d3b1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "63b3bf1c-055b-438f-b71b-2052728f9ca4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c3e8a6d7-d3d6-4ee4-919f-1a593baede8d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "63b3bf1c-055b-438f-b71b-2052728f9ca4",
                    "LayerId": "bdf575df-b4ef-4411-8b3d-a99c9a683e59"
                }
            ]
        },
        {
            "id": "70e87705-1889-49a1-8e0c-961336e1402f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e671db2e-8611-43f6-9206-d5383f456ea9",
            "compositeImage": {
                "id": "8b6aa692-68e7-47d0-ac97-058a3f1083c8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "70e87705-1889-49a1-8e0c-961336e1402f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6df36fb3-0589-4214-938d-1c54aaa3b6db",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "70e87705-1889-49a1-8e0c-961336e1402f",
                    "LayerId": "bdf575df-b4ef-4411-8b3d-a99c9a683e59"
                }
            ]
        },
        {
            "id": "9e8a2dc3-1171-4c39-ab44-d00b2c70808a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e671db2e-8611-43f6-9206-d5383f456ea9",
            "compositeImage": {
                "id": "7452a691-29e6-471f-b269-a3801205abee",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9e8a2dc3-1171-4c39-ab44-d00b2c70808a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2718d792-d2fc-4691-884b-e8bca860f088",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9e8a2dc3-1171-4c39-ab44-d00b2c70808a",
                    "LayerId": "bdf575df-b4ef-4411-8b3d-a99c9a683e59"
                }
            ]
        },
        {
            "id": "ebba7c75-fb9e-43e5-bb51-16a27975d850",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e671db2e-8611-43f6-9206-d5383f456ea9",
            "compositeImage": {
                "id": "8123f359-b700-43b3-bcdf-8c3efa70cad2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ebba7c75-fb9e-43e5-bb51-16a27975d850",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "59b3efd8-1f30-4346-8f4f-6c2729a64ae1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ebba7c75-fb9e-43e5-bb51-16a27975d850",
                    "LayerId": "bdf575df-b4ef-4411-8b3d-a99c9a683e59"
                }
            ]
        },
        {
            "id": "9d20315f-ac01-4a1e-ae4a-bcdf8b9ffda0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e671db2e-8611-43f6-9206-d5383f456ea9",
            "compositeImage": {
                "id": "316439b5-2d8c-4db5-bd5d-51e90d0f0b26",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9d20315f-ac01-4a1e-ae4a-bcdf8b9ffda0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9e7b3215-056a-430a-85c0-97ebb27b3114",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9d20315f-ac01-4a1e-ae4a-bcdf8b9ffda0",
                    "LayerId": "bdf575df-b4ef-4411-8b3d-a99c9a683e59"
                }
            ]
        },
        {
            "id": "9bd547bd-de37-4a50-8159-419f9f0c7a51",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e671db2e-8611-43f6-9206-d5383f456ea9",
            "compositeImage": {
                "id": "940b4e95-35a5-4b72-a393-177190b64695",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9bd547bd-de37-4a50-8159-419f9f0c7a51",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6ba5fc44-6a3d-4628-8e89-358db78ed67f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9bd547bd-de37-4a50-8159-419f9f0c7a51",
                    "LayerId": "bdf575df-b4ef-4411-8b3d-a99c9a683e59"
                }
            ]
        },
        {
            "id": "7b927583-6f96-42e6-9764-a90ffe35bb33",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e671db2e-8611-43f6-9206-d5383f456ea9",
            "compositeImage": {
                "id": "d3c2a6ab-0940-4119-80b6-7368b674f96a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7b927583-6f96-42e6-9764-a90ffe35bb33",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6e6a4c12-286a-4325-81ed-82a0fbef9894",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7b927583-6f96-42e6-9764-a90ffe35bb33",
                    "LayerId": "bdf575df-b4ef-4411-8b3d-a99c9a683e59"
                }
            ]
        },
        {
            "id": "809c9dfd-cdfd-4cd1-b6ec-1198a0462e2f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e671db2e-8611-43f6-9206-d5383f456ea9",
            "compositeImage": {
                "id": "5dd9d1d7-a23c-4c25-9264-cb17d29b6dc4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "809c9dfd-cdfd-4cd1-b6ec-1198a0462e2f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "46660851-9eb7-4c14-8a64-4256078c13e8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "809c9dfd-cdfd-4cd1-b6ec-1198a0462e2f",
                    "LayerId": "bdf575df-b4ef-4411-8b3d-a99c9a683e59"
                }
            ]
        },
        {
            "id": "889ee91a-0af4-4a7d-bcc2-ce03cd62b23c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e671db2e-8611-43f6-9206-d5383f456ea9",
            "compositeImage": {
                "id": "c61b8ab2-859c-4f1a-b803-149600e9efe8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "889ee91a-0af4-4a7d-bcc2-ce03cd62b23c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6fe88c57-1f5f-41b9-aa57-dfed97e0793c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "889ee91a-0af4-4a7d-bcc2-ce03cd62b23c",
                    "LayerId": "bdf575df-b4ef-4411-8b3d-a99c9a683e59"
                }
            ]
        },
        {
            "id": "ab2584b6-b9e3-4f47-b17e-549e558b0958",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e671db2e-8611-43f6-9206-d5383f456ea9",
            "compositeImage": {
                "id": "61c9ec25-f63e-4e79-892f-3c6a10f315b0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ab2584b6-b9e3-4f47-b17e-549e558b0958",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2b5111bc-e1b6-46df-97b3-1d317ec34bd0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ab2584b6-b9e3-4f47-b17e-549e558b0958",
                    "LayerId": "bdf575df-b4ef-4411-8b3d-a99c9a683e59"
                }
            ]
        },
        {
            "id": "5e231ecc-5774-4673-8f1f-badf1fab843c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e671db2e-8611-43f6-9206-d5383f456ea9",
            "compositeImage": {
                "id": "e5e26633-9245-46fe-a5a8-b4772675af25",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5e231ecc-5774-4673-8f1f-badf1fab843c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "036c3e06-659d-4ccb-8139-b4bd58784392",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5e231ecc-5774-4673-8f1f-badf1fab843c",
                    "LayerId": "bdf575df-b4ef-4411-8b3d-a99c9a683e59"
                }
            ]
        },
        {
            "id": "1e9f7e5b-314f-4f8b-b4d4-ddcb292deffc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e671db2e-8611-43f6-9206-d5383f456ea9",
            "compositeImage": {
                "id": "7ef1f8ea-9e01-4357-a48a-f5110e686cf7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1e9f7e5b-314f-4f8b-b4d4-ddcb292deffc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ff463c81-5d5d-44cc-94cd-81f7b96b02e4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1e9f7e5b-314f-4f8b-b4d4-ddcb292deffc",
                    "LayerId": "bdf575df-b4ef-4411-8b3d-a99c9a683e59"
                }
            ]
        },
        {
            "id": "29dc80d1-aa37-4ae4-af27-c95afc8d449e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e671db2e-8611-43f6-9206-d5383f456ea9",
            "compositeImage": {
                "id": "8ea22596-9440-462d-bdf1-cfc05101f6b9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "29dc80d1-aa37-4ae4-af27-c95afc8d449e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6fc081c5-3430-4fb9-b359-ecdb3b390a0e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "29dc80d1-aa37-4ae4-af27-c95afc8d449e",
                    "LayerId": "bdf575df-b4ef-4411-8b3d-a99c9a683e59"
                }
            ]
        },
        {
            "id": "212821a5-88be-4d5d-9a07-6789b5695f8e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e671db2e-8611-43f6-9206-d5383f456ea9",
            "compositeImage": {
                "id": "48fdcd41-cee9-4fdc-99d9-5311ba8f6e6a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "212821a5-88be-4d5d-9a07-6789b5695f8e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f14258ec-839a-488a-8214-57c8cf6913bf",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "212821a5-88be-4d5d-9a07-6789b5695f8e",
                    "LayerId": "bdf575df-b4ef-4411-8b3d-a99c9a683e59"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 35,
    "layers": [
        {
            "id": "bdf575df-b4ef-4411-8b3d-a99c9a683e59",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "e671db2e-8611-43f6-9206-d5383f456ea9",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 80,
    "xorig": -3,
    "yorig": 37
}