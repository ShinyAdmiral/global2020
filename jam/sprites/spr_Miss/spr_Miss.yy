{
    "id": "6b5c3d9f-6fd7-458d-a670-e6a55644a043",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_Miss",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 29,
    "bbox_left": 2,
    "bbox_right": 77,
    "bbox_top": 6,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "bf3f5452-52ae-4f70-96d6-ed980c84420b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6b5c3d9f-6fd7-458d-a670-e6a55644a043",
            "compositeImage": {
                "id": "6ea35489-898d-4f27-8256-67491f4de4e5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bf3f5452-52ae-4f70-96d6-ed980c84420b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "67ab36c6-f13d-42c1-a9ca-1f10f6a2480e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bf3f5452-52ae-4f70-96d6-ed980c84420b",
                    "LayerId": "2a914c93-4233-4c7a-a0cb-3d9f938a3bb6"
                }
            ]
        },
        {
            "id": "021ed96d-3d75-4dea-8481-53090d3e8d8b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6b5c3d9f-6fd7-458d-a670-e6a55644a043",
            "compositeImage": {
                "id": "d3b8f8b7-babc-487e-9657-2e375882fad3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "021ed96d-3d75-4dea-8481-53090d3e8d8b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "321a3f69-ef2d-4a85-bcd4-9b48d65b6dfd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "021ed96d-3d75-4dea-8481-53090d3e8d8b",
                    "LayerId": "2a914c93-4233-4c7a-a0cb-3d9f938a3bb6"
                }
            ]
        },
        {
            "id": "0a02f367-9372-47f0-a2f5-aa250daa35b6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6b5c3d9f-6fd7-458d-a670-e6a55644a043",
            "compositeImage": {
                "id": "cabd2311-6027-4b3e-94b0-b7034d5e4ac2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0a02f367-9372-47f0-a2f5-aa250daa35b6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c8ba948a-6e01-4e16-9897-501534819c8d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0a02f367-9372-47f0-a2f5-aa250daa35b6",
                    "LayerId": "2a914c93-4233-4c7a-a0cb-3d9f938a3bb6"
                }
            ]
        },
        {
            "id": "196b80b0-0ee4-445d-a5a3-22da04d9e86c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6b5c3d9f-6fd7-458d-a670-e6a55644a043",
            "compositeImage": {
                "id": "41413f9d-537f-4687-a1da-d7a2ab5fd346",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "196b80b0-0ee4-445d-a5a3-22da04d9e86c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "63be787c-64f7-40a5-b358-9a8a06b363b5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "196b80b0-0ee4-445d-a5a3-22da04d9e86c",
                    "LayerId": "2a914c93-4233-4c7a-a0cb-3d9f938a3bb6"
                }
            ]
        },
        {
            "id": "7cef9927-e847-4a47-9b16-297ddb87ed68",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6b5c3d9f-6fd7-458d-a670-e6a55644a043",
            "compositeImage": {
                "id": "f71717fb-2d16-4b94-ae9b-a147903acfae",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7cef9927-e847-4a47-9b16-297ddb87ed68",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1e057d61-8755-45d2-83ec-10aba1855d2e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7cef9927-e847-4a47-9b16-297ddb87ed68",
                    "LayerId": "2a914c93-4233-4c7a-a0cb-3d9f938a3bb6"
                }
            ]
        },
        {
            "id": "4f51dd26-852e-4329-97fa-7e4df3a1891e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6b5c3d9f-6fd7-458d-a670-e6a55644a043",
            "compositeImage": {
                "id": "bd4a131c-00c3-4c9e-8e9e-a1207b79ba52",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4f51dd26-852e-4329-97fa-7e4df3a1891e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7cd970d0-b329-4cb4-878a-a5a38e25e50b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4f51dd26-852e-4329-97fa-7e4df3a1891e",
                    "LayerId": "2a914c93-4233-4c7a-a0cb-3d9f938a3bb6"
                }
            ]
        },
        {
            "id": "e1141f13-d603-4d7c-b6f1-8d65735e5dbc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6b5c3d9f-6fd7-458d-a670-e6a55644a043",
            "compositeImage": {
                "id": "3b4d8687-9d60-4de4-9c9d-34ae057dc953",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e1141f13-d603-4d7c-b6f1-8d65735e5dbc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "53f92a29-e72d-4b07-b0c4-e0927a731c70",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e1141f13-d603-4d7c-b6f1-8d65735e5dbc",
                    "LayerId": "2a914c93-4233-4c7a-a0cb-3d9f938a3bb6"
                }
            ]
        },
        {
            "id": "081d45e0-b39c-4119-bd86-473cbc83141a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6b5c3d9f-6fd7-458d-a670-e6a55644a043",
            "compositeImage": {
                "id": "89a0f6ed-f588-4a51-b2ab-6b51ca3ffcf8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "081d45e0-b39c-4119-bd86-473cbc83141a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "65b5724b-8983-48e3-9808-6c41b9a00a94",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "081d45e0-b39c-4119-bd86-473cbc83141a",
                    "LayerId": "2a914c93-4233-4c7a-a0cb-3d9f938a3bb6"
                }
            ]
        },
        {
            "id": "29d64ee8-e154-4d89-81a6-2fb2413402e8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6b5c3d9f-6fd7-458d-a670-e6a55644a043",
            "compositeImage": {
                "id": "81b30b27-fde3-4d02-b22c-6b3a2821a18b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "29d64ee8-e154-4d89-81a6-2fb2413402e8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a20f3868-0717-4b72-9a38-4405eb9db94e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "29d64ee8-e154-4d89-81a6-2fb2413402e8",
                    "LayerId": "2a914c93-4233-4c7a-a0cb-3d9f938a3bb6"
                }
            ]
        },
        {
            "id": "48715811-8fac-407b-a829-edd713d38d61",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6b5c3d9f-6fd7-458d-a670-e6a55644a043",
            "compositeImage": {
                "id": "83eaa526-0772-41fe-8dfe-182ca5be0139",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "48715811-8fac-407b-a829-edd713d38d61",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3e4a18a4-6dc3-4ff1-b2dc-1d6cb4e10f2c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "48715811-8fac-407b-a829-edd713d38d61",
                    "LayerId": "2a914c93-4233-4c7a-a0cb-3d9f938a3bb6"
                }
            ]
        },
        {
            "id": "ed1d73f6-0406-4970-9f07-5027d07fab1f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6b5c3d9f-6fd7-458d-a670-e6a55644a043",
            "compositeImage": {
                "id": "30f43094-de48-4f96-82dc-1e7041d733cb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ed1d73f6-0406-4970-9f07-5027d07fab1f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f08c02c1-7f22-46b2-84d6-b27bd5a4b3d8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ed1d73f6-0406-4970-9f07-5027d07fab1f",
                    "LayerId": "2a914c93-4233-4c7a-a0cb-3d9f938a3bb6"
                }
            ]
        },
        {
            "id": "b8718e13-307e-435b-b15b-070ec5f71ed8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6b5c3d9f-6fd7-458d-a670-e6a55644a043",
            "compositeImage": {
                "id": "acd0b8ff-bdf1-4e37-9ee3-8a5f6e52e06e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b8718e13-307e-435b-b15b-070ec5f71ed8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0101391b-1b06-43af-a4e2-19dba542e211",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b8718e13-307e-435b-b15b-070ec5f71ed8",
                    "LayerId": "2a914c93-4233-4c7a-a0cb-3d9f938a3bb6"
                }
            ]
        },
        {
            "id": "21415990-14cd-4806-9bae-acfcfdd25226",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6b5c3d9f-6fd7-458d-a670-e6a55644a043",
            "compositeImage": {
                "id": "5fbba87f-71b4-4c6e-9854-48dd45c7a363",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "21415990-14cd-4806-9bae-acfcfdd25226",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f49ce039-a189-410b-88f3-c07e9fda5e35",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "21415990-14cd-4806-9bae-acfcfdd25226",
                    "LayerId": "2a914c93-4233-4c7a-a0cb-3d9f938a3bb6"
                }
            ]
        },
        {
            "id": "e8b85e1b-0e16-453e-ad3a-3e625b45c363",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6b5c3d9f-6fd7-458d-a670-e6a55644a043",
            "compositeImage": {
                "id": "c9110174-9b86-433d-b6b6-b5e9277d732f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e8b85e1b-0e16-453e-ad3a-3e625b45c363",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a95d27e5-b92f-4f27-bf18-4f3118824b45",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e8b85e1b-0e16-453e-ad3a-3e625b45c363",
                    "LayerId": "2a914c93-4233-4c7a-a0cb-3d9f938a3bb6"
                }
            ]
        },
        {
            "id": "fceeabee-32a7-4240-a01a-94022294e2a6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6b5c3d9f-6fd7-458d-a670-e6a55644a043",
            "compositeImage": {
                "id": "27f5907c-eafd-441f-abe7-f3e15bb1f4db",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fceeabee-32a7-4240-a01a-94022294e2a6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7fce28b0-ffe9-43e7-82f1-99a2a52146a6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fceeabee-32a7-4240-a01a-94022294e2a6",
                    "LayerId": "2a914c93-4233-4c7a-a0cb-3d9f938a3bb6"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 35,
    "layers": [
        {
            "id": "2a914c93-4233-4c7a-a0cb-3d9f938a3bb6",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "6b5c3d9f-6fd7-458d-a670-e6a55644a043",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 14,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 80,
    "xorig": -3,
    "yorig": 37
}