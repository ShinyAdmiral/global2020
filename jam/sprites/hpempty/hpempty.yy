{
    "id": "ccd03ea8-ccc6-42a7-b864-21c00e7e02cb",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "hpempty",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 9,
    "bbox_left": 4,
    "bbox_right": 36,
    "bbox_top": 3,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "d9dc4778-b2fb-4e19-9487-62049292177f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ccd03ea8-ccc6-42a7-b864-21c00e7e02cb",
            "compositeImage": {
                "id": "efa433bd-143a-4174-bd8d-20e0680743d3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d9dc4778-b2fb-4e19-9487-62049292177f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7c5287da-71ff-4913-8d65-ad5ce6e1db91",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d9dc4778-b2fb-4e19-9487-62049292177f",
                    "LayerId": "3d3d876b-1555-4ce8-bf13-35c0f811d0a0"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 13,
    "layers": [
        {
            "id": "3d3d876b-1555-4ce8-bf13-35c0f811d0a0",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "ccd03ea8-ccc6-42a7-b864-21c00e7e02cb",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 40,
    "xorig": 4,
    "yorig": 3
}