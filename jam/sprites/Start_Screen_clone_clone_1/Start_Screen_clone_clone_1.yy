{
    "id": "916b5611-8c19-4962-b07f-3a2c0b4b59cc",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "Start_Screen_clone_clone_1",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 113,
    "bbox_left": 111,
    "bbox_right": 186,
    "bbox_top": 84,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "80874591-be72-46d5-9ee5-5a37452d2dd8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "916b5611-8c19-4962-b07f-3a2c0b4b59cc",
            "compositeImage": {
                "id": "499e47b7-f1af-4ba8-8238-df85ff01c442",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "80874591-be72-46d5-9ee5-5a37452d2dd8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "edc1de54-e483-422c-b3c2-ad6c02f3b763",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "80874591-be72-46d5-9ee5-5a37452d2dd8",
                    "LayerId": "ea95fca2-7434-48ec-b212-c38d9b448d1a"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 180,
    "layers": [
        {
            "id": "ea95fca2-7434-48ec-b212-c38d9b448d1a",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "916b5611-8c19-4962-b07f-3a2c0b4b59cc",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 320,
    "xorig": 151,
    "yorig": 90
}