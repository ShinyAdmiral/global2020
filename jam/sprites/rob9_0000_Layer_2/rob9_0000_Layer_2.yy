{
    "id": "4cb2a9d5-a837-4c95-bc52-e46784fea1f7",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "rob9_0000_Layer_2",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 110,
    "bbox_left": 0,
    "bbox_right": 14,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 0,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "aebe6cc8-6c8e-42d1-a85e-8fbfdb871288",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4cb2a9d5-a837-4c95-bc52-e46784fea1f7",
            "compositeImage": {
                "id": "d456919d-09fa-443f-b847-c340aa5055d8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "aebe6cc8-6c8e-42d1-a85e-8fbfdb871288",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "33e6563b-9e8b-4657-bccc-c00b3c2a63c1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "aebe6cc8-6c8e-42d1-a85e-8fbfdb871288",
                    "LayerId": "27fcadc7-e596-4325-aa09-36e2394b38f4"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 111,
    "layers": [
        {
            "id": "27fcadc7-e596-4325-aa09-36e2394b38f4",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "4cb2a9d5-a837-4c95-bc52-e46784fea1f7",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 15,
    "xorig": 7,
    "yorig": 55
}