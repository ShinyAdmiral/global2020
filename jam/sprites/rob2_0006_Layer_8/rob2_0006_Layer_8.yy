{
    "id": "ec3a2fb6-9323-49e6-854e-1e7bad30d9d9",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "rob2_0006_Layer_8",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 27,
    "bbox_left": 0,
    "bbox_right": 10,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 0,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "299180c6-87d1-4c15-aad5-7cc3e808cdb9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ec3a2fb6-9323-49e6-854e-1e7bad30d9d9",
            "compositeImage": {
                "id": "86c1e0b7-a27c-4120-a766-6c6892cade79",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "299180c6-87d1-4c15-aad5-7cc3e808cdb9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "030dc2c2-42d1-4c9a-a666-a6d89ee57c19",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "299180c6-87d1-4c15-aad5-7cc3e808cdb9",
                    "LayerId": "c556edc6-53c1-4035-854a-67cd5735263c"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 28,
    "layers": [
        {
            "id": "c556edc6-53c1-4035-854a-67cd5735263c",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "ec3a2fb6-9323-49e6-854e-1e7bad30d9d9",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 11,
    "xorig": 3,
    "yorig": 2
}