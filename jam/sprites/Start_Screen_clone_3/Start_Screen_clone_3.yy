{
    "id": "2e411613-edcd-41cc-b1de-72c343401e13",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "Start_Screen_clone_3",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 76,
    "bbox_left": 111,
    "bbox_right": 208,
    "bbox_top": 7,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "cff1b9d7-cb86-4335-bde9-b228520459c8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2e411613-edcd-41cc-b1de-72c343401e13",
            "compositeImage": {
                "id": "9b347d2c-386d-43c1-a085-8e6ed28a7f83",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cff1b9d7-cb86-4335-bde9-b228520459c8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "354f88a7-7ddc-4961-b5f4-d0d09faf2529",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cff1b9d7-cb86-4335-bde9-b228520459c8",
                    "LayerId": "0dcff4f4-23c8-4ad8-b205-8870b89a03a6"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 180,
    "layers": [
        {
            "id": "0dcff4f4-23c8-4ad8-b205-8870b89a03a6",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "2e411613-edcd-41cc-b1de-72c343401e13",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 320,
    "xorig": 0,
    "yorig": 0
}