{
    "id": "ac030762-c6a7-4990-9448-d714deff2aed",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_Perfect",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 2,
    "bbox_right": 78,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "9403a30b-247f-4967-8a20-29c562cbf0e8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ac030762-c6a7-4990-9448-d714deff2aed",
            "compositeImage": {
                "id": "d6a4a1b7-5852-4f6e-a862-360cf61bd096",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9403a30b-247f-4967-8a20-29c562cbf0e8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "73a3a86f-5acb-4b2d-9eb4-b9f8c5246705",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9403a30b-247f-4967-8a20-29c562cbf0e8",
                    "LayerId": "fcdc5b31-7362-4816-914b-a6b469116ebf"
                }
            ]
        },
        {
            "id": "db4a8531-cf35-409a-a2a2-006168e9bb66",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ac030762-c6a7-4990-9448-d714deff2aed",
            "compositeImage": {
                "id": "254df9ca-fdc7-43f9-9569-ca337e358728",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "db4a8531-cf35-409a-a2a2-006168e9bb66",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "52899b4a-0627-48a3-8289-45a3bf5f4777",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "db4a8531-cf35-409a-a2a2-006168e9bb66",
                    "LayerId": "fcdc5b31-7362-4816-914b-a6b469116ebf"
                }
            ]
        },
        {
            "id": "f8bf6842-bb40-4526-a429-125607413817",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ac030762-c6a7-4990-9448-d714deff2aed",
            "compositeImage": {
                "id": "197aafd9-866e-40f8-bf6f-c7005c85940b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f8bf6842-bb40-4526-a429-125607413817",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3287565a-88da-4e1f-b48d-b2023efa0db7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f8bf6842-bb40-4526-a429-125607413817",
                    "LayerId": "fcdc5b31-7362-4816-914b-a6b469116ebf"
                }
            ]
        },
        {
            "id": "a45cb0c6-2ec5-4ea9-a5ad-a59a804801ca",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ac030762-c6a7-4990-9448-d714deff2aed",
            "compositeImage": {
                "id": "66743426-1f31-44a3-8938-66ff8903fb21",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a45cb0c6-2ec5-4ea9-a5ad-a59a804801ca",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "23834af7-c975-4e8e-967c-45cd59200061",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a45cb0c6-2ec5-4ea9-a5ad-a59a804801ca",
                    "LayerId": "fcdc5b31-7362-4816-914b-a6b469116ebf"
                }
            ]
        },
        {
            "id": "1c933613-88ae-4031-8558-37ce51515ea0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ac030762-c6a7-4990-9448-d714deff2aed",
            "compositeImage": {
                "id": "6a426730-fcce-4fa3-be6f-dfc0f0fa8497",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1c933613-88ae-4031-8558-37ce51515ea0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "edf2b172-332c-4cf7-8a2d-5399b82269f5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1c933613-88ae-4031-8558-37ce51515ea0",
                    "LayerId": "fcdc5b31-7362-4816-914b-a6b469116ebf"
                }
            ]
        },
        {
            "id": "e19462f2-f9b4-42f7-b3f5-c93ad18db288",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ac030762-c6a7-4990-9448-d714deff2aed",
            "compositeImage": {
                "id": "13937ec7-dd6d-4a09-8931-1e132af7618c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e19462f2-f9b4-42f7-b3f5-c93ad18db288",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e42e891d-7737-43f6-8f90-b6c758d44940",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e19462f2-f9b4-42f7-b3f5-c93ad18db288",
                    "LayerId": "fcdc5b31-7362-4816-914b-a6b469116ebf"
                }
            ]
        },
        {
            "id": "c64193d9-4600-4416-b475-4272c8e0d02f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ac030762-c6a7-4990-9448-d714deff2aed",
            "compositeImage": {
                "id": "ad8b7649-5385-4f32-99b8-368f9a8d90df",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c64193d9-4600-4416-b475-4272c8e0d02f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8e63d24b-eea3-41ab-91a6-73158ea77085",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c64193d9-4600-4416-b475-4272c8e0d02f",
                    "LayerId": "fcdc5b31-7362-4816-914b-a6b469116ebf"
                }
            ]
        },
        {
            "id": "aa72f9d4-68ca-4a30-b4ab-a88677010651",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ac030762-c6a7-4990-9448-d714deff2aed",
            "compositeImage": {
                "id": "cd6a15d3-0c60-4c99-9f83-33aeaee36578",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "aa72f9d4-68ca-4a30-b4ab-a88677010651",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "71658e12-1e35-4c1f-91e1-6b486c69fcee",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "aa72f9d4-68ca-4a30-b4ab-a88677010651",
                    "LayerId": "fcdc5b31-7362-4816-914b-a6b469116ebf"
                }
            ]
        },
        {
            "id": "21b8925b-2d96-48d9-8d5e-dd28bf2f998f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ac030762-c6a7-4990-9448-d714deff2aed",
            "compositeImage": {
                "id": "595f6bf0-2524-4d8f-83de-b5fdf9b4efd1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "21b8925b-2d96-48d9-8d5e-dd28bf2f998f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e9c45025-a224-4e99-9953-7ff6f8d8f07f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "21b8925b-2d96-48d9-8d5e-dd28bf2f998f",
                    "LayerId": "fcdc5b31-7362-4816-914b-a6b469116ebf"
                }
            ]
        },
        {
            "id": "09fc5a18-fb21-4c30-9e55-52d0259a4e04",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ac030762-c6a7-4990-9448-d714deff2aed",
            "compositeImage": {
                "id": "59db4c8d-f39d-436d-9391-7984f2065e8d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "09fc5a18-fb21-4c30-9e55-52d0259a4e04",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6ada1b78-1ce6-4514-8a30-3975bf68db4e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "09fc5a18-fb21-4c30-9e55-52d0259a4e04",
                    "LayerId": "fcdc5b31-7362-4816-914b-a6b469116ebf"
                }
            ]
        },
        {
            "id": "d2ba9d27-90f1-4beb-bfa9-72d4e4eefe35",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ac030762-c6a7-4990-9448-d714deff2aed",
            "compositeImage": {
                "id": "ee72addd-204b-44f9-95e8-071031b7069e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d2ba9d27-90f1-4beb-bfa9-72d4e4eefe35",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3202f22e-f25c-4cbf-bd2f-488cb7fe672b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d2ba9d27-90f1-4beb-bfa9-72d4e4eefe35",
                    "LayerId": "fcdc5b31-7362-4816-914b-a6b469116ebf"
                }
            ]
        },
        {
            "id": "79b4d57d-04f2-4464-9beb-b2b6990b4992",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ac030762-c6a7-4990-9448-d714deff2aed",
            "compositeImage": {
                "id": "aa934aa5-ca19-48b1-a784-0251cffaa9c8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "79b4d57d-04f2-4464-9beb-b2b6990b4992",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3afd45a9-95a3-4044-aedb-0a0c256c9be5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "79b4d57d-04f2-4464-9beb-b2b6990b4992",
                    "LayerId": "fcdc5b31-7362-4816-914b-a6b469116ebf"
                }
            ]
        },
        {
            "id": "9a9ceb26-53cf-4f79-82ad-cc0590e48af9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ac030762-c6a7-4990-9448-d714deff2aed",
            "compositeImage": {
                "id": "5dfbe431-782b-4e74-ba0d-f9e7e813bdfd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9a9ceb26-53cf-4f79-82ad-cc0590e48af9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "49e89128-b124-4b18-8029-24510ec94f83",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9a9ceb26-53cf-4f79-82ad-cc0590e48af9",
                    "LayerId": "fcdc5b31-7362-4816-914b-a6b469116ebf"
                }
            ]
        },
        {
            "id": "48d9bf4b-8c9c-4237-b066-ab977748e3e1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ac030762-c6a7-4990-9448-d714deff2aed",
            "compositeImage": {
                "id": "c0a0659e-f327-47cc-ade7-a1752cad55e1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "48d9bf4b-8c9c-4237-b066-ab977748e3e1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c1b58db9-e288-4097-baab-83a7ab6ad1df",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "48d9bf4b-8c9c-4237-b066-ab977748e3e1",
                    "LayerId": "fcdc5b31-7362-4816-914b-a6b469116ebf"
                }
            ]
        },
        {
            "id": "984f0a6f-37a9-4d8a-a06a-9628a3dd8c6a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ac030762-c6a7-4990-9448-d714deff2aed",
            "compositeImage": {
                "id": "0c136daa-b563-433a-ae97-027c6d512342",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "984f0a6f-37a9-4d8a-a06a-9628a3dd8c6a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5b8a4cbf-f685-4dc0-9e1e-6d766d866e3c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "984f0a6f-37a9-4d8a-a06a-9628a3dd8c6a",
                    "LayerId": "fcdc5b31-7362-4816-914b-a6b469116ebf"
                }
            ]
        },
        {
            "id": "5c6492fe-24dc-4823-87ae-9e654b90e08d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ac030762-c6a7-4990-9448-d714deff2aed",
            "compositeImage": {
                "id": "ee952b05-255c-45c9-ab7f-e5d70b0625fd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5c6492fe-24dc-4823-87ae-9e654b90e08d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e1c0cafb-cc4a-4499-8c8a-a6f14f558079",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5c6492fe-24dc-4823-87ae-9e654b90e08d",
                    "LayerId": "fcdc5b31-7362-4816-914b-a6b469116ebf"
                }
            ]
        },
        {
            "id": "c009bff4-b833-4a0d-8fd7-f679641ebbd6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ac030762-c6a7-4990-9448-d714deff2aed",
            "compositeImage": {
                "id": "79ee288e-44d0-41dc-916a-f328fe30a154",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c009bff4-b833-4a0d-8fd7-f679641ebbd6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ba2d6c99-f210-4890-aef2-ad588004f1de",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c009bff4-b833-4a0d-8fd7-f679641ebbd6",
                    "LayerId": "fcdc5b31-7362-4816-914b-a6b469116ebf"
                }
            ]
        },
        {
            "id": "38de0747-7ee3-4142-a8ae-fc21e9e82317",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ac030762-c6a7-4990-9448-d714deff2aed",
            "compositeImage": {
                "id": "10c28833-f303-4bfb-ae01-30259ab37fcc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "38de0747-7ee3-4142-a8ae-fc21e9e82317",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6fe2345e-e5f6-4cf0-bca3-20734d20e2a2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "38de0747-7ee3-4142-a8ae-fc21e9e82317",
                    "LayerId": "fcdc5b31-7362-4816-914b-a6b469116ebf"
                }
            ]
        },
        {
            "id": "23c5eb63-19a8-4203-9f1b-c4e8d66d93d8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ac030762-c6a7-4990-9448-d714deff2aed",
            "compositeImage": {
                "id": "42f454ff-e475-4373-9a4b-108e1e765429",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "23c5eb63-19a8-4203-9f1b-c4e8d66d93d8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8b20f3d2-6874-447a-82ba-7a903ede7b63",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "23c5eb63-19a8-4203-9f1b-c4e8d66d93d8",
                    "LayerId": "fcdc5b31-7362-4816-914b-a6b469116ebf"
                }
            ]
        },
        {
            "id": "0ba7e61b-a47e-44c8-8de8-55c399d0ef99",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ac030762-c6a7-4990-9448-d714deff2aed",
            "compositeImage": {
                "id": "7288b1d0-c372-4b9b-b91b-4e78239d9911",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0ba7e61b-a47e-44c8-8de8-55c399d0ef99",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c6648959-4eb3-47ee-85cf-0f0e9d8388ff",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0ba7e61b-a47e-44c8-8de8-55c399d0ef99",
                    "LayerId": "fcdc5b31-7362-4816-914b-a6b469116ebf"
                }
            ]
        },
        {
            "id": "00de29a8-2e9e-4445-bff1-4785d6fc91ca",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ac030762-c6a7-4990-9448-d714deff2aed",
            "compositeImage": {
                "id": "94295173-174f-4f2a-9488-f856b93f6a06",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "00de29a8-2e9e-4445-bff1-4785d6fc91ca",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8c79435c-3d7e-40df-bd42-e40cf1546c77",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "00de29a8-2e9e-4445-bff1-4785d6fc91ca",
                    "LayerId": "fcdc5b31-7362-4816-914b-a6b469116ebf"
                }
            ]
        },
        {
            "id": "502f2d62-8a0e-4411-bd3e-d9210689a3ec",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ac030762-c6a7-4990-9448-d714deff2aed",
            "compositeImage": {
                "id": "afc2be82-822a-4592-a76e-b33122326e68",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "502f2d62-8a0e-4411-bd3e-d9210689a3ec",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4e2bf3d0-5c73-4d3f-a4c5-e5ed92fabc93",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "502f2d62-8a0e-4411-bd3e-d9210689a3ec",
                    "LayerId": "fcdc5b31-7362-4816-914b-a6b469116ebf"
                }
            ]
        },
        {
            "id": "bac84906-6237-4dcd-9751-59c62181bbb9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ac030762-c6a7-4990-9448-d714deff2aed",
            "compositeImage": {
                "id": "e7cde30b-f25b-4474-a620-605bd62ec0d4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bac84906-6237-4dcd-9751-59c62181bbb9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5f4ce90b-7054-4611-8efd-5cf9af46fc73",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bac84906-6237-4dcd-9751-59c62181bbb9",
                    "LayerId": "fcdc5b31-7362-4816-914b-a6b469116ebf"
                }
            ]
        },
        {
            "id": "8b7ed6bd-d61b-4211-bc83-5ac487afdae8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ac030762-c6a7-4990-9448-d714deff2aed",
            "compositeImage": {
                "id": "7658c999-ec1c-496d-a921-ea4261ac37b1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8b7ed6bd-d61b-4211-bc83-5ac487afdae8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bdbef668-e8a6-415b-b251-b429087416ba",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8b7ed6bd-d61b-4211-bc83-5ac487afdae8",
                    "LayerId": "fcdc5b31-7362-4816-914b-a6b469116ebf"
                }
            ]
        },
        {
            "id": "6a04cb16-35ca-4cbe-b98e-e263d7b77ace",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ac030762-c6a7-4990-9448-d714deff2aed",
            "compositeImage": {
                "id": "c2493d95-80ba-440b-a831-1bac5c1f2f57",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6a04cb16-35ca-4cbe-b98e-e263d7b77ace",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "33024f18-67d1-4f48-90db-842790267c0f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6a04cb16-35ca-4cbe-b98e-e263d7b77ace",
                    "LayerId": "fcdc5b31-7362-4816-914b-a6b469116ebf"
                }
            ]
        },
        {
            "id": "be21c39b-4d13-4f49-96e1-0e9975356e4e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ac030762-c6a7-4990-9448-d714deff2aed",
            "compositeImage": {
                "id": "5687e10e-e226-407b-ab2f-2192d89b40d1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "be21c39b-4d13-4f49-96e1-0e9975356e4e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "36a0ce35-701f-4608-a6bb-16ce19569150",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "be21c39b-4d13-4f49-96e1-0e9975356e4e",
                    "LayerId": "fcdc5b31-7362-4816-914b-a6b469116ebf"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 35,
    "layers": [
        {
            "id": "fcdc5b31-7362-4816-914b-a6b469116ebf",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "ac030762-c6a7-4990-9448-d714deff2aed",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 25,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 80,
    "xorig": -3,
    "yorig": 37
}