{
    "id": "934c5aed-6bb6-476a-9a41-db2187a5051f",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "End_Screen",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 111,
    "bbox_left": 3,
    "bbox_right": 312,
    "bbox_top": 13,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "bdd53543-1463-40e8-833c-908c35e0efca",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "934c5aed-6bb6-476a-9a41-db2187a5051f",
            "compositeImage": {
                "id": "a169e30f-244b-49c7-9583-3adf7c0cc510",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bdd53543-1463-40e8-833c-908c35e0efca",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d4c7312c-6c2a-4dd3-a6c9-f384554d8960",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bdd53543-1463-40e8-833c-908c35e0efca",
                    "LayerId": "376d2859-d029-4b90-9f19-dc97a1ec6ac8"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 180,
    "layers": [
        {
            "id": "376d2859-d029-4b90-9f19-dc97a1ec6ac8",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "934c5aed-6bb6-476a-9a41-db2187a5051f",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 320,
    "xorig": 160,
    "yorig": 90
}