{
    "id": "8a87d65e-76c4-4412-aa37-ea091c6c12d3",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "rob1_0003_Layer_5",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 30,
    "bbox_left": 0,
    "bbox_right": 32,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 0,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "19978922-ffdc-422c-b324-665e247dec6a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8a87d65e-76c4-4412-aa37-ea091c6c12d3",
            "compositeImage": {
                "id": "8bbb3e42-608e-4bb5-a4f3-513bf1a79def",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "19978922-ffdc-422c-b324-665e247dec6a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c0a3f0c5-3f01-4afc-8a3b-5c5bbb46caed",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "19978922-ffdc-422c-b324-665e247dec6a",
                    "LayerId": "7ede08c0-26e6-4bd6-a965-d1cee590832d"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 31,
    "layers": [
        {
            "id": "7ede08c0-26e6-4bd6-a965-d1cee590832d",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "8a87d65e-76c4-4412-aa37-ea091c6c12d3",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 33,
    "xorig": 16,
    "yorig": 26
}