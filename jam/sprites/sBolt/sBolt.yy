{
    "id": "7b465159-394b-407e-84c4-0410e0852075",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sBolt",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 4,
    "bbox_left": 0,
    "bbox_right": 4,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "8ce623dc-0673-40bc-81f6-f7551bfdf2d7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7b465159-394b-407e-84c4-0410e0852075",
            "compositeImage": {
                "id": "c5192c86-6def-4eb7-a5fd-1251ca59780c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8ce623dc-0673-40bc-81f6-f7551bfdf2d7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3a3055f3-14ba-4b45-8c5b-effce26aa7d1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8ce623dc-0673-40bc-81f6-f7551bfdf2d7",
                    "LayerId": "607d39fd-dfba-4583-b3ae-23dcbb3f0175"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 5,
    "layers": [
        {
            "id": "607d39fd-dfba-4583-b3ae-23dcbb3f0175",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "7b465159-394b-407e-84c4-0410e0852075",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 5,
    "xorig": 2,
    "yorig": 2
}