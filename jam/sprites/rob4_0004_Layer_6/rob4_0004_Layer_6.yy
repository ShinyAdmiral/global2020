{
    "id": "567b3977-7784-4817-8735-b27415fa59b5",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "rob4_0004_Layer_6",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 18,
    "bbox_left": 0,
    "bbox_right": 10,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 0,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "79d35aaf-fa91-490f-b73a-d86d03009a79",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "567b3977-7784-4817-8735-b27415fa59b5",
            "compositeImage": {
                "id": "0884b36c-f8d8-45cb-8695-d2efab7e3dc3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "79d35aaf-fa91-490f-b73a-d86d03009a79",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cd6f1d2a-d0c2-4ed0-9b5c-efb819a60152",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "79d35aaf-fa91-490f-b73a-d86d03009a79",
                    "LayerId": "0fe4988b-1c9c-4b79-a8c6-c3fc726ecf3f"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 19,
    "layers": [
        {
            "id": "0fe4988b-1c9c-4b79-a8c6-c3fc726ecf3f",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "567b3977-7784-4817-8735-b27415fa59b5",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 11,
    "xorig": 7,
    "yorig": 2
}