{
    "id": "3a57ef93-f3fd-4f55-a2be-4a7fe4af1196",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "rob3_0003_Layer_5",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 43,
    "bbox_left": 0,
    "bbox_right": 36,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 0,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "a6f007b1-2c1a-4fed-aa1e-e8f5298cb66d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3a57ef93-f3fd-4f55-a2be-4a7fe4af1196",
            "compositeImage": {
                "id": "13675964-a41f-40e0-8608-fbb829818c8b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a6f007b1-2c1a-4fed-aa1e-e8f5298cb66d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3f5c7a83-edd4-4c8e-9864-a6b2f1556785",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a6f007b1-2c1a-4fed-aa1e-e8f5298cb66d",
                    "LayerId": "cef8b907-1e0f-4370-a3d8-98f0ece1637a"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 44,
    "layers": [
        {
            "id": "cef8b907-1e0f-4370-a3d8-98f0ece1637a",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "3a57ef93-f3fd-4f55-a2be-4a7fe4af1196",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 37,
    "xorig": 18,
    "yorig": 22
}