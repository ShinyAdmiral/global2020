{
    "id": "6d3993e1-e70a-4c12-939b-de81cb02dc5a",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "battle_default",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 28,
    "bbox_left": 0,
    "bbox_right": 23,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "eb1659e5-c513-4452-923f-c101fce51646",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6d3993e1-e70a-4c12-939b-de81cb02dc5a",
            "compositeImage": {
                "id": "a0d399ad-6d60-421a-a196-d58e89d2cf83",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "eb1659e5-c513-4452-923f-c101fce51646",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ce413aba-786e-4ca2-acde-f89e07d0dadc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "eb1659e5-c513-4452-923f-c101fce51646",
                    "LayerId": "1e39cee9-b3a6-493b-a788-e0ea4da25666"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 29,
    "layers": [
        {
            "id": "1e39cee9-b3a6-493b-a788-e0ea4da25666",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "6d3993e1-e70a-4c12-939b-de81cb02dc5a",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 7,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 24,
    "xorig": 12,
    "yorig": 28
}