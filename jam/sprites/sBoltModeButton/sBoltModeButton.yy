{
    "id": "642b6a61-2497-4f4e-9c49-e542fde750d5",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sBoltModeButton",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 18,
    "bbox_left": 0,
    "bbox_right": 28,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "f1f4822c-5d53-499e-8384-60ecb2b905ed",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "642b6a61-2497-4f4e-9c49-e542fde750d5",
            "compositeImage": {
                "id": "75299398-94bd-47d6-82a5-e0afc643e69f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f1f4822c-5d53-499e-8384-60ecb2b905ed",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "dc158127-79dc-46f7-87ad-ca4e942db221",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f1f4822c-5d53-499e-8384-60ecb2b905ed",
                    "LayerId": "546aae90-be02-42f1-9d8d-d0d258e99026"
                }
            ]
        },
        {
            "id": "45ddf791-4af8-449a-a678-42fa9a654e9e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "642b6a61-2497-4f4e-9c49-e542fde750d5",
            "compositeImage": {
                "id": "77f0638c-d582-4d66-9f90-3115b830eaba",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "45ddf791-4af8-449a-a678-42fa9a654e9e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "abc9a99c-3c5d-4dfd-b70e-2483102a3d70",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "45ddf791-4af8-449a-a678-42fa9a654e9e",
                    "LayerId": "546aae90-be02-42f1-9d8d-d0d258e99026"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 19,
    "layers": [
        {
            "id": "546aae90-be02-42f1-9d8d-d0d258e99026",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "642b6a61-2497-4f4e-9c49-e542fde750d5",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 29,
    "xorig": 0,
    "yorig": 0
}