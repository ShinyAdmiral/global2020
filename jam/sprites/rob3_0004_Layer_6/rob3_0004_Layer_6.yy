{
    "id": "40e8d21a-7d6d-45b8-99ed-34978b8325cd",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "rob3_0004_Layer_6",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 28,
    "bbox_left": 0,
    "bbox_right": 16,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 0,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "9d51652e-3b38-4ddc-a55a-58746a329a61",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "40e8d21a-7d6d-45b8-99ed-34978b8325cd",
            "compositeImage": {
                "id": "285cceff-5091-42a0-b3b0-f59fc09a6d88",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9d51652e-3b38-4ddc-a55a-58746a329a61",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6eee6e25-514f-4569-9ee9-3d86ea332f87",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9d51652e-3b38-4ddc-a55a-58746a329a61",
                    "LayerId": "3f218043-26da-4a0f-8d3a-98b533652b40"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 29,
    "layers": [
        {
            "id": "3f218043-26da-4a0f-8d3a-98b533652b40",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "40e8d21a-7d6d-45b8-99ed-34978b8325cd",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 17,
    "xorig": 10,
    "yorig": 4
}