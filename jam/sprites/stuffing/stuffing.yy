{
    "id": "b6e06657-756f-4178-8c53-760aae9aee48",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "stuffing",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 11,
    "bbox_left": 4,
    "bbox_right": 11,
    "bbox_top": 4,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "832ee14b-65fe-423e-b410-57a7a20d044d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b6e06657-756f-4178-8c53-760aae9aee48",
            "compositeImage": {
                "id": "3ae94b68-bc4c-4189-9910-2992754c7fa1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "832ee14b-65fe-423e-b410-57a7a20d044d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5381b386-e59e-443f-bd9c-ff3837b41c59",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "832ee14b-65fe-423e-b410-57a7a20d044d",
                    "LayerId": "d89eadd4-8802-4618-8308-224a87e44e20"
                }
            ]
        },
        {
            "id": "5cf73f94-78de-460f-8e3e-b33907dc76f4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b6e06657-756f-4178-8c53-760aae9aee48",
            "compositeImage": {
                "id": "021179e5-e479-44e7-a500-3d4f845371ec",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5cf73f94-78de-460f-8e3e-b33907dc76f4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "536ffaa7-7db4-4f75-9a51-5257ecafe846",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5cf73f94-78de-460f-8e3e-b33907dc76f4",
                    "LayerId": "d89eadd4-8802-4618-8308-224a87e44e20"
                }
            ]
        },
        {
            "id": "46c4ce72-a9f9-471c-8014-53493ad8faf7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b6e06657-756f-4178-8c53-760aae9aee48",
            "compositeImage": {
                "id": "38fab916-b10e-46f6-b2d6-be55526e613e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "46c4ce72-a9f9-471c-8014-53493ad8faf7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9ab9f1ea-d27d-4ea2-9557-e2d7b34368ed",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "46c4ce72-a9f9-471c-8014-53493ad8faf7",
                    "LayerId": "d89eadd4-8802-4618-8308-224a87e44e20"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "d89eadd4-8802-4618-8308-224a87e44e20",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b6e06657-756f-4178-8c53-760aae9aee48",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}