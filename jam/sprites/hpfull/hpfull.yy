{
    "id": "3404f96e-5ffd-4a1a-8534-0e49887a9369",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "hpfull",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 8,
    "bbox_left": 6,
    "bbox_right": 34,
    "bbox_top": 4,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "0dad7a76-bdea-4b92-916e-ec7d74e8c823",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3404f96e-5ffd-4a1a-8534-0e49887a9369",
            "compositeImage": {
                "id": "2b2db0cc-31d3-4b7c-a828-b6eb3653dd5e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0dad7a76-bdea-4b92-916e-ec7d74e8c823",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0f70e546-7cf2-4df3-8e8f-a7aee35c263c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0dad7a76-bdea-4b92-916e-ec7d74e8c823",
                    "LayerId": "ed787618-6186-44c6-ab29-59ffb81084dd"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 13,
    "layers": [
        {
            "id": "ed787618-6186-44c6-ab29-59ffb81084dd",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "3404f96e-5ffd-4a1a-8534-0e49887a9369",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 40,
    "xorig": 4,
    "yorig": 3
}