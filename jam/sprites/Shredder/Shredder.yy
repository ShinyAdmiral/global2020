{
    "id": "80b59b4e-ed5e-4856-ba8c-11fa4b0b00df",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "Shredder",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "acc46fb8-d268-4a15-b1b9-d2e5e578a1a1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "80b59b4e-ed5e-4856-ba8c-11fa4b0b00df",
            "compositeImage": {
                "id": "b054b15c-40a9-44c2-ba8e-3baccdf0d73d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "acc46fb8-d268-4a15-b1b9-d2e5e578a1a1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b350c50b-00e0-4789-bed3-d6e08cba682f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "acc46fb8-d268-4a15-b1b9-d2e5e578a1a1",
                    "LayerId": "a97af38d-a26b-4df4-a5d0-db57b45ed7a2"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "a97af38d-a26b-4df4-a5d0-db57b45ed7a2",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "80b59b4e-ed5e-4856-ba8c-11fa4b0b00df",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 0,
    "yorig": 0
}