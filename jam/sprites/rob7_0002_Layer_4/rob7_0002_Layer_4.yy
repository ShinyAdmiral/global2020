{
    "id": "ee8c1671-c492-43be-a843-07a58491b041",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "rob7_0002_Layer_4",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 17,
    "bbox_left": 0,
    "bbox_right": 16,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 0,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "cef759f1-cf47-444a-9dae-efe666aa9f06",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ee8c1671-c492-43be-a843-07a58491b041",
            "compositeImage": {
                "id": "9256a6f3-c86c-4593-9e83-ddf37b6518c3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cef759f1-cf47-444a-9dae-efe666aa9f06",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "04b79d5b-5a7a-4124-b7e9-a5349a4e7c0a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cef759f1-cf47-444a-9dae-efe666aa9f06",
                    "LayerId": "fbfe9e70-8b4b-48d6-85c3-6a9e37f9a9fa"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 18,
    "layers": [
        {
            "id": "fbfe9e70-8b4b-48d6-85c3-6a9e37f9a9fa",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "ee8c1671-c492-43be-a843-07a58491b041",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 17,
    "xorig": 13,
    "yorig": 3
}