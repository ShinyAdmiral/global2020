{
    "id": "d39cc17f-4bc9-47c4-a86c-aa4ff08aa985",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sweep",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "41437614-e8ba-4236-ab07-74ff7a5ab9c8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d39cc17f-4bc9-47c4-a86c-aa4ff08aa985",
            "compositeImage": {
                "id": "dbf0b760-5320-4e16-9014-012d2e0c92c3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "41437614-e8ba-4236-ab07-74ff7a5ab9c8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1ce728a4-3156-48a5-a76f-ef840299deeb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "41437614-e8ba-4236-ab07-74ff7a5ab9c8",
                    "LayerId": "3db58654-75e5-44a3-b587-c869c88815f6"
                }
            ]
        },
        {
            "id": "b098719c-8e79-4297-929c-9ac774fcfdfb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d39cc17f-4bc9-47c4-a86c-aa4ff08aa985",
            "compositeImage": {
                "id": "2701d2fa-a6cb-4851-944b-cf233b070251",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b098719c-8e79-4297-929c-9ac774fcfdfb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c9a33730-84e7-4eb1-b1bc-e74eda4988ee",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b098719c-8e79-4297-929c-9ac774fcfdfb",
                    "LayerId": "3db58654-75e5-44a3-b587-c869c88815f6"
                }
            ]
        },
        {
            "id": "6c7d1023-4ac2-4d48-ab49-858927452351",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d39cc17f-4bc9-47c4-a86c-aa4ff08aa985",
            "compositeImage": {
                "id": "205bd7a4-7047-4620-a7da-2899cebd5f2d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6c7d1023-4ac2-4d48-ab49-858927452351",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "dc8dc855-1b95-47cc-8030-83cf549401b7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6c7d1023-4ac2-4d48-ab49-858927452351",
                    "LayerId": "3db58654-75e5-44a3-b587-c869c88815f6"
                }
            ]
        },
        {
            "id": "aad0b8cf-e956-4fbd-9e5e-72c6b389632c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d39cc17f-4bc9-47c4-a86c-aa4ff08aa985",
            "compositeImage": {
                "id": "1962663f-7ee1-40e7-bf0e-1591ba523f9a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "aad0b8cf-e956-4fbd-9e5e-72c6b389632c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "dac4f2cb-8756-43ca-bbc9-0216557e0031",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "aad0b8cf-e956-4fbd-9e5e-72c6b389632c",
                    "LayerId": "3db58654-75e5-44a3-b587-c869c88815f6"
                }
            ]
        },
        {
            "id": "a19b2399-edda-4b71-b8e7-8ab3733db1b3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d39cc17f-4bc9-47c4-a86c-aa4ff08aa985",
            "compositeImage": {
                "id": "582c25fd-5bcf-4b23-aa44-bca41c19f7a5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a19b2399-edda-4b71-b8e7-8ab3733db1b3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bdf23c0c-3b71-4b71-8501-d0d0a23191c1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a19b2399-edda-4b71-b8e7-8ab3733db1b3",
                    "LayerId": "3db58654-75e5-44a3-b587-c869c88815f6"
                }
            ]
        },
        {
            "id": "be7726c7-6fdd-4063-a669-185b571b139e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d39cc17f-4bc9-47c4-a86c-aa4ff08aa985",
            "compositeImage": {
                "id": "757192db-c09b-4f5a-a8c1-f236bb3a4c8a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "be7726c7-6fdd-4063-a669-185b571b139e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "de1900e3-c74b-4701-808c-338b0661b0cf",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "be7726c7-6fdd-4063-a669-185b571b139e",
                    "LayerId": "3db58654-75e5-44a3-b587-c869c88815f6"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "3db58654-75e5-44a3-b587-c869c88815f6",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "d39cc17f-4bc9-47c4-a86c-aa4ff08aa985",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 3,
    "originLocked": false,
    "playbackSpeed": 20,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 32
}