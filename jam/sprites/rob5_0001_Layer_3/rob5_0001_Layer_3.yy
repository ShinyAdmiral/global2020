{
    "id": "c7c21cd3-ab59-4f54-b5a9-cad8cdc15515",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "rob5_0001_Layer_3",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 32,
    "bbox_left": 0,
    "bbox_right": 25,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 0,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "b4419cf5-aef1-4be3-9b3f-28b6c8952001",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c7c21cd3-ab59-4f54-b5a9-cad8cdc15515",
            "compositeImage": {
                "id": "15c3fa12-d647-40e8-98dc-ea3c085667ee",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b4419cf5-aef1-4be3-9b3f-28b6c8952001",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a78ef63c-92c1-43f8-bf8a-a76d57bbf54f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b4419cf5-aef1-4be3-9b3f-28b6c8952001",
                    "LayerId": "e7e9bec7-0bd4-4789-b752-be71d28df78c"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 33,
    "layers": [
        {
            "id": "e7e9bec7-0bd4-4789-b752-be71d28df78c",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "c7c21cd3-ab59-4f54-b5a9-cad8cdc15515",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 26,
    "xorig": 0,
    "yorig": 7
}